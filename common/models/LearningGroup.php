<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "learning_group".
 *
 * @property int $id
 * @property string $name
 * @property int $teacher_id
 * @property int $auto_class_id
 *
 * @property AutoClass $autoClass
 * @property Staff $teacher
 * @property UserLearningGroup[] $userLearningGroups
 * @property Staff[] $staff
 */
class LearningGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'learning_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacher_id', 'auto_class_id'], 'integer'],
            [['auto_class_id'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['auto_class_id'], 'exist', 'skipOnError' => true, 'targetClass' => AutoClass::className(), 'targetAttribute' => ['auto_class_id' => 'id']],
            [['teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Staff::className(), 'targetAttribute' => ['teacher_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'teacher_id' => 'Teacher ID',
            'auto_class_id' => 'Auto Class ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoClass()
    {
        return $this->hasOne(AutoClass::className(), ['id' => 'auto_class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Staff::className(), ['id' => 'teacher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLearningGroups()
    {
        return $this->hasMany(UserLearningGroup::className(), ['learning_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasMany(Staff::className(), ['id' => 'staff_id'])->viaTable('user_learning_group', ['learning_group_id' => 'id']);
    }

    public static function getLearningGroupNames()
    {
        /** @var LearningGroup[] $result */
        $result = self::find()->all();
        $names = [];
        foreach ($result as $item) {
            $names[$item->id] = $item->name;
        }
        return $names;
    }

    public static function getTeacherLearningGroupIds($teacherId)
    {
        /** @var LearningGroup[] $result */
        $result = self::find()->where(['teacher_id' => $teacherId])->all();
        $groupIds = [];
        foreach ($result as $item){
            $groupIds[] = $item->id;
        }
        return $groupIds;
    }
}
