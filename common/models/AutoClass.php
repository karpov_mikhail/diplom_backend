<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "auto_class".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $active
 *
 * @property TheoryCardAutoClass[] $theoryCardAutoClasses
 * @property TheoryCard[] $cards
 */
class AutoClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auto_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['active'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheoryCardAutoClasses()
    {
        return $this->hasMany(TheoryCardAutoClass::className(), ['auto_class_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCards()
    {
        return $this->hasMany(TheoryCard::className(), ['id' => 'card_id'])->viaTable('theory_card_auto_class', ['auto_class_id' => 'id']);
    }

    /**
     * Метод для получения имен категорий авто с ключами в виде id
     * @return array
     */
    public static function getAutoClassNames()
    {
        $names = [];
        /** @var AutoClass[] $result */
        $result = self::find()->all();
        foreach ($result as $item) {
            $names[$item->id] = $item->name;
        }
        return $names;
    }
}
