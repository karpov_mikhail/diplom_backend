<?php

namespace common\models;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "theory_answer_log".
 *
 * @property int $id
 * @property int $user_id
 * @property int $question_id
 * @property int $answer_id
 * @property string $answer_time
 * @property int $correctly
 *
 * @property TheoryAnswer $answer
 * @property TheoryQuestion $question
 * @property User $user
 */
class TheoryAnswerLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'theory_answer_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'question_id', 'answer_id', 'answer_time', 'correctly'], 'required'],
            [['user_id', 'question_id', 'answer_id', 'correctly'], 'integer'],
            [['answer_time'], 'safe'],
            [['answer_id'], 'exist', 'skipOnError' => true, 'targetClass' => TheoryAnswer::className(), 'targetAttribute' => ['answer_id' => 'id']],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => TheoryQuestion::className(), 'targetAttribute' => ['question_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'question_id' => 'Question ID',
            'answer_id' => 'Answer ID',
            'answer_time' => 'Answer Time',
            'correctly' => 'Correctly',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswer()
    {
        return $this->hasOne(TheoryAnswer::className(), ['id' => 'answer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(TheoryQuestion::className(), ['id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Функция сохранения логов
     * @param $questionId
     * @param $answerId
     * @param $isRightAnswer
     */
    public static function addToLog($questionId, $answerId, $isRightAnswer)
    {
        if(!Yii::$app->user->isGuest) {
            $log = self::findOne(['question_id' => $questionId, 'user_id' => Yii::$app->user->getId()]);
            if(!empty($log)) {
                $log->answer_id = $answerId;
                $log->correctly = $isRightAnswer;
                $log->answer_time = date('Y-m-d H:i:s');
                $log->save();
            }
            else {
                $newLog = new self();
                $newLog->user_id = Yii::$app->user->getId();
                $newLog->question_id = $questionId;
                $newLog->answer_id = $answerId;
                $newLog->answer_time = date('Y-m-d H:i:s');
                $newLog->correctly = $isRightAnswer;
                $newLog->save();
            }
        }
    }
}
