<?php

namespace common\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "file".
 *
 * @property int $id
 * @property string $path
 * @property string $name
 * @property string $extension
 * @property string $size
 * @property string $mime
 *
 * @property User[] $users
 */
class File extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path', 'name', 'extension', 'size', 'mime'], 'required'],
            [['path', 'name', 'size', 'mime'], 'string', 'max' => 255],
            [['extension'], 'string', 'max' => 10],
            [['path'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'name' => 'Name',
            'extension' => 'Extension',
            'size' => 'Size',
            'mime' => 'Mime',
        ];
    }

    /**
     * @param $file UploadedFile
     * @param $folder string
     * @return array|int
     * @description return id or array of errors
     */
    public static function addFile($file, $folder = "")
    {
        $newName = File::generateUniqueFileName(16);
        $path =  '/uploads/';
        if (!empty($folder)) {
            $path .= $folder . "/";
        }
        if (file_exists(realpath(\Yii::getAlias('@app/..')) . $path) === false) {
            mkdir(realpath(\Yii::getAlias('@app/..')) . $path, 0777, true);
        }
        $path .= $newName . '.' . $file->extension;
        $file->saveAs(realpath(\Yii::getAlias('@app/..')) . $path);
        $newFile = new File();
        $newFile->path = (!empty($_SERVER['HTTPS']) ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . $path;
        $newFile->name = $file->baseName;
        $newFile->extension = $file->extension;
        $newFile->size = (string)$file->size;
        $newFile->mime = FileHelper::getMimeType(realpath(\Yii::getAlias('@app/..')) . $path);
        if($newFile->save() === true) {
            $newFile->load(["id"]);
            return $newFile->id;
        } else {
            return $newFile->getErrors();
        }
    }

    public static function generateUniqueFileName($length = 32)
    {
        $randomString = \Yii::$app->getSecurity()->generateRandomString($length);

        if(self::find()->where(['like', 'path', $randomString])->count() == 0)
            return $randomString;
        else
            return self::generateUniqueFileName( $length);
    }

    /**
     * @param $id int
     */
    public static function getPath($id)
    {
        $file = self::findOne($id);
        return $file->path;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['avatar_id' => 'id']);
    }
}
