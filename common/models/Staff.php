<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "staff".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $second_name
 * @property int $group_id
 * @property int $user_id
 * @property int $salary_per_hour
 *
 * @property GroupOneTimeCode $groupOneTimeCode
 * @property Group $group
 * @property User $user
 * @property UserLearningGroup[] $userLearningGroups
 * @property LearningGroup[] $learningGroups
 * @property UserPracticeCar[] $userPracticeCars
 * @property PracticeCar[] $practiceCars
 */
class Staff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'staff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'group_id'], 'required'],
            [['group_id', 'user_id', 'salary_per_hour'], 'integer'],
            [['first_name', 'last_name', 'second_name'], 'string', 'max' => 255],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Group::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'second_name' => 'Second Name',
            'group_id' => 'Group ID',
            'user_id' => 'User ID',
            'salary_per_hour' => 'Salary Per Hour',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupOneTimeCode()
    {
        return $this->hasOne(GroupOneTimeCode::className(), ['staff_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Group::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLearningGroups()
    {
        return $this->hasMany(UserLearningGroup::className(), ['staff_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLearningGroups()
    {
        return $this->hasMany(LearningGroup::className(), ['id' => 'learning_group_id'])->viaTable('user_learning_group', ['staff_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPracticeCars()
    {
        return $this->hasMany(UserPracticeCar::className(), ['learner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPracticeCars()
    {
        return $this->hasMany(PracticeCar::className(), ['id' => 'practice_car_id'])->viaTable('user_practice_car', ['learner_id' => 'id']);
    }

    public static function getStaffGroup($staffIds)
    {
        /** @var Staff[] $result */
        $result = self::find()->where(['id' => $staffIds])->all();
        $staff = [];
        foreach ($result as $item) {
            $staff[$item->id]['job'] = $item->group->name;
            $staff[$item->id]['code'] = $item->groupOneTimeCode->unique_code;
        }

        return $staff;
    }

    public function getCardStatistic()
    {
        $allAnswersCount = 800;
        if(!empty($this->user_id)) {
            $successAnswersCount = TheoryAnswerLog::find()->where(['user_id' => $this->user_id, 'correctly' => true])->count();
            $failureAnswersCount = TheoryAnswerLog::find()->where(['user_id' => $this->user_id, 'correctly' => false])->count();
            $otherAnswersCount = $allAnswersCount - $successAnswersCount - $failureAnswersCount;
            return [
                'success' => $successAnswersCount,
                'failure' => $failureAnswersCount,
                'other' => $otherAnswersCount,
                'all' => $allAnswersCount
            ];
        } else {
            return [
                'success' => 0,
                'failure' => 0,
                'other' => $allAnswersCount,
                'all' => $allAnswersCount
            ];
        }
    }

    public static function getInstructorNames()
    {
        $instructorId = Group::findOne(['code' => 'instructor'])->id;
        /** @var Staff[] $result */
        $result = Staff::find()->where(['group_id' => $instructorId])->all();
        $instructorNames = [];
        foreach ($result as $item) {
            $instructorNames[$item->id] = $item->last_name . " " . $item->first_name;
        }
        return $instructorNames;
    }
}
