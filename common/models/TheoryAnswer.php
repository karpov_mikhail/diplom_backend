<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "theory_answer".
 *
 * @property int $id
 * @property string $answer_text
 * @property int $question_id
 * @property bool $right_answer
 *
 * @property TheoryQuestion $question
 * @property TheoryAnswerLog[] $theoryAnswerLogs
 */
class TheoryAnswer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'theory_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['answer_text', 'question_id'], 'required'],
            [['answer_text'], 'string'],
            ['question_id', 'integer'],
            ['right_answer', 'boolean'],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => TheoryQuestion::className(), 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'answer_text' => 'Answer Text',
            'question_id' => 'Question ID',
            'right_answer' => 'Right Answer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(TheoryQuestion::className(), ['id' => 'question_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheoryAnswerLogs()
    {
        return $this->hasMany(TheoryAnswerLog::className(), ['answer_id' => 'id']);
    }
}
