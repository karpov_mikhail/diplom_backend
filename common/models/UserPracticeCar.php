<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_practice_car".
 *
 * @property int $learner_id
 * @property int $practice_car_id
 *
 * @property Staff $learner
 * @property PracticeCar $practiceCar
 */
class UserPracticeCar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_practice_car';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['learner_id', 'practice_car_id'], 'required'],
            [['learner_id', 'practice_car_id'], 'integer'],
            [['learner_id', 'practice_car_id'], 'unique', 'targetAttribute' => ['learner_id', 'practice_car_id']],
            [['learner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Staff::className(), 'targetAttribute' => ['learner_id' => 'id']],
            [['practice_car_id'], 'exist', 'skipOnError' => true, 'targetClass' => PracticeCar::className(), 'targetAttribute' => ['practice_car_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'learner_id' => 'Learner ID',
            'practice_car_id' => 'Practice Car ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLearner()
    {
        return $this->hasOne(Staff::className(), ['id' => 'learner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPracticeCar()
    {
        return $this->hasOne(PracticeCar::className(), ['id' => 'practice_car_id']);
    }
}
