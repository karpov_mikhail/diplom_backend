<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "theory_card_auto_class".
 *
 * @property int $auto_class_id
 * @property int $card_id
 *
 * @property AutoClass $autoClass
 * @property TheoryCard $card
 */
class TheoryCardAutoClass extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'theory_card_auto_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auto_class_id', 'card_id'], 'required'],
            [['auto_class_id', 'card_id'], 'integer'],
            [['auto_class_id', 'card_id'], 'unique', 'targetAttribute' => ['auto_class_id', 'card_id']],
            [['auto_class_id'], 'exist', 'skipOnError' => true, 'targetClass' => AutoClass::className(), 'targetAttribute' => ['auto_class_id' => 'id']],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => TheoryCard::className(), 'targetAttribute' => ['card_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'auto_class_id' => 'Auto Class ID',
            'card_id' => 'Card ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoClass()
    {
        return $this->hasOne(AutoClass::className(), ['id' => 'auto_class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCard()
    {
        return $this->hasOne(TheoryCard::className(), ['id' => 'card_id']);
    }
}
