<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "theory_card".
 *
 * @property int $id
 * @property int $card_number
 *
 * @property TheoryCardAutoClass[] $theoryCardAutoClasses
 * @property AutoClass[] $autoClasses
 * @property TheoryCardLog[] $theoryCardLogs
 * @property TheoryQuestion[] $theoryQuestions
 */
class TheoryCard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'theory_card';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['card_number'], 'required'],
            [['card_number'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'card_number' => 'Card Number',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheoryCardAutoClasses()
    {
        return $this->hasMany(TheoryCardAutoClass::className(), ['card_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoClasses()
    {
        return $this->hasMany(AutoClass::className(), ['id' => 'auto_class_id'])->viaTable('theory_card_auto_class', ['card_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheoryCardLogs()
    {
        return $this->hasMany(TheoryCardLog::className(), ['card_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheoryQuestions()
    {
        return $this->hasMany(TheoryQuestion::className(), ['card_id' => 'id']);
    }

    /**
     * Метод поиска id категории авто, к которым принадлежит билет
     * @return array
     */
    public function getCardAutoClassIds()
    {
        $result = TheoryCardAutoClass::find()->where(["card_id" => $this->id])->select("auto_class_id")->asArray()->all();
        return array_column($result, "auto_class_id");
    }

    /**
     * Метод получения строки с категориями авто, к которым принадлежат билеты
     * @param $cardIds int|array
     * @return array
     */
    public static function getCardsAutoClassString($cardIds)
    {
        $cardsAutoClass = TheoryCardAutoClass::find()->where(['card_id' => $cardIds])->asArray()->all();
        $autoClasses = AutoClass::getAutoClassNames();
        $cardsAutoClassString = [];
        foreach ($cardsAutoClass as $item) {
            if(empty($cardsAutoClassString[$item['card_id']])) {
                $cardsAutoClassString[$item['card_id']] = $autoClasses[$item['auto_class_id']];
            } else {
                $cardsAutoClassString[$item['card_id']] .= ', ' . $autoClasses[$item['auto_class_id']];
            }
        }

        return $cardsAutoClassString;
    }

    /**
     * Метод получения номеров билета по id
     * @param $cardIds
     * @return array
     */
    public static function getCardNumbers($cardIds)
    {
        /** @var TheoryCard[] $cards */
        $cards = TheoryCard::find()->where(['id' => $cardIds])->all();
        $cardNumbers = [];
        foreach ($cards as $card) {
            $cardNumbers[$card->id] = $card->card_number;
        }
        return $cardNumbers;
    }
}
