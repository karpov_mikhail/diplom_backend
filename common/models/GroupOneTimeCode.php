<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "group_one_time_code".
 *
 * @property int $id
 * @property string $unique_code
 * @property string $create_date
 * @property boolean $activated
 * @property int $staff_id
 *
 * @property Staff $staff
 */
class GroupOneTimeCode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group_one_time_code';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['unique_code', 'create_date', 'activated', 'staff_id'], 'required'],
            [['create_date'], 'safe'],
            [['staff_id'], 'integer'],
            [['activated'], 'boolean'],
            [['unique_code'], 'string', 'max' => 10],
            [['unique_code'], 'unique'],
            [['staff_id'], 'unique'],
            [['staff_id'], 'exist', 'skipOnError' => true, 'targetClass' => Staff::className(), 'targetAttribute' => ['staff_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'unique_code' => 'Unique Code',
            'create_date' => 'Create Date',
            'activated' => 'Activated',
            'staff_id' => 'Staff ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasOne(Staff::className(), ['id' => 'staff_id']);
    }

    public static function generateUniqueCode($lenght = 10)
    {
        $randomString = \Yii::$app->getSecurity()->generateRandomString($lenght);

        if(self::find()->where(['unique_code' => $randomString])->count() == 0)
            return $randomString;
        else
            return self::generateUniqueCode($lenght);
    }
}
