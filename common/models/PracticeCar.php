<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "practice_car".
 *
 * @property int $id
 * @property string $mark
 * @property string $model
 * @property double $car_power
 * @property int $max_learners
 * @property int $teacher_id
 * @property int $auto_class_id
 * @property int $car_photo_id
 *
 * @property AutoClass $autoClass
 * @property File $carPhoto
 * @property Staff $teacher
 * @property PracticeSchedule[] $practiceSchedules
 * @property UserPracticeCar[] $userPracticeCars
 * @property Staff[] $learners
 */
class PracticeCar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'practice_car';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mark', 'model', 'car_power', 'auto_class_id'], 'required'],
            [['car_power'], 'number'],
            [['max_learners', 'teacher_id', 'auto_class_id', 'car_photo_id'], 'integer'],
            [['mark', 'model'], 'string', 'max' => 255],
            [['teacher_id'], 'unique'],
            [['auto_class_id'], 'exist', 'skipOnError' => true, 'targetClass' => AutoClass::className(), 'targetAttribute' => ['auto_class_id' => 'id']],
            [['car_photo_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['car_photo_id' => 'id']],
            [['teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => Staff::className(), 'targetAttribute' => ['teacher_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mark' => 'Mark',
            'model' => 'Model',
            'car_power' => 'Car Power',
            'max_learners' => 'Max Learners',
            'teacher_id' => 'Teacher ID',
            'auto_class_id' => 'Auto Class ID',
            'car_photo_id' => 'Car Photo ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoClass()
    {
        return $this->hasOne(AutoClass::className(), ['id' => 'auto_class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarPhoto()
    {
        return $this->hasOne(File::className(), ['id' => 'car_photo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(Staff::className(), ['id' => 'teacher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPracticeSchedules()
    {
        return $this->hasMany(PracticeSchedule::className(), ['practice_car_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPracticeCars()
    {
        return $this->hasMany(UserPracticeCar::className(), ['practice_car_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLearners()
    {
        return $this->hasMany(Staff::className(), ['id' => 'learner_id'])->viaTable('user_practice_car', ['practice_car_id' => 'id']);
    }

    public static function getCarIdsByAutoClassId($autoClassId)
    {
        $result = self::find()->where(['auto_class_id' => $autoClassId])->asArray()->all();
        return array_column($result, 'id');
    }
}
