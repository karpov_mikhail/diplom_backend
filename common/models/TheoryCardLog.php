<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "theory_card_log".
 *
 * @property int $id
 * @property int $card_id
 * @property int $user_id
 * @property string $create_time
 * @property int $passed
 * @property int $right_answer_count
 *
 * @property TheoryCard $card
 * @property User $user
 */
class TheoryCardLog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'theory_card_log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['card_id', 'user_id', 'create_time', 'passed', 'right_answer_count'], 'required'],
            [['card_id', 'user_id',  'right_answer_count'], 'integer'],
            [['passed'], 'boolean'],
            [['create_time'], 'safe'],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => TheoryCard::className(), 'targetAttribute' => ['card_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'card_id' => 'Card ID',
            'user_id' => 'User ID',
            'create_time' => 'Create Time',
            'passed' => 'Passed',
            'right_answer_count' => 'Right Answer Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCard()
    {
        return $this->hasOne(TheoryCard::className(), ['id' => 'card_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function addToLog($cardId, $rightAnswerCount)
    {
        if(!Yii::$app->user->isGuest) {
            $log = self::findOne(['user_id' => Yii::$app->user->getId(), 'card_id' => $cardId]);
            if(!empty($log)) {
                $log->right_answer_count = $rightAnswerCount;
                $log->passed = $rightAnswerCount < Yii::$app->params['rightAnswerCountToPassedCard'] ? false : true ;
                $log->create_time = date('Y-m-d H:i:s');
                $log->save();
            }
            else {
                $newLog = new self();
                $newLog->user_id = Yii::$app->user->getId();
                $newLog->card_id = $cardId;
                $newLog->right_answer_count = $rightAnswerCount;
                $newLog->passed = $rightAnswerCount < Yii::$app->params['rightAnswerCountToPassedCard'] ? false : true ;
                $newLog->create_time = date('Y-m-d H:i:s');
                $newLog->save();
            }
        }
    }
}
