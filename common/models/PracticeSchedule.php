<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "practice_schedule".
 *
 * @property int $id
 * @property string $datetime
 * @property int $learner_id
 * @property int $practice_car_id
 *
 * @property Staff $learner
 * @property PracticeCar $practiceCar
 */
class PracticeSchedule extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'practice_schedule';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['datetime', 'learner_id'], 'required'],
            [['datetime'], 'safe'],
            [['learner_id', 'practice_car_id'], 'integer'],
            [['learner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Staff::className(), 'targetAttribute' => ['learner_id' => 'id']],
            [['practice_car_id'], 'exist', 'skipOnError' => true, 'targetClass' => PracticeCar::className(), 'targetAttribute' => ['practice_car_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datetime' => 'Datetime',
            'learner_id' => 'Learner ID',
            'practice_car_id' => 'Practice Car ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLearner()
    {
        return $this->hasOne(Staff::className(), ['id' => 'learner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPracticeCar()
    {
        return $this->hasOne(PracticeCar::className(), ['id' => 'practice_car_id']);
    }
}
