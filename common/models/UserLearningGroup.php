<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_learning_group".
 *
 * @property int $learning_group_id
 * @property int $staff_id
 *
 * @property LearningGroup $learningGroup
 * @property Staff $staff
 */
class UserLearningGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_learning_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['learning_group_id', 'staff_id'], 'required'],
            [['learning_group_id', 'staff_id'], 'integer'],
            [['learning_group_id', 'staff_id'], 'unique', 'targetAttribute' => ['learning_group_id', 'staff_id']],
            [['learning_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => LearningGroup::className(), 'targetAttribute' => ['learning_group_id' => 'id']],
            [['staff_id'], 'exist', 'skipOnError' => true, 'targetClass' => Staff::className(), 'targetAttribute' => ['staff_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'learning_group_id' => 'Learning Group ID',
            'staff_id' => 'Staff ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLearningGroup()
    {
        return $this->hasOne(LearningGroup::className(), ['id' => 'learning_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasOne(Staff::className(), ['id' => 'staff_id']);
    }

    public static function getUserLearningGroupIds($staffId)
    {
        /** @var UserLearningGroup[] $result */
        $result = self::find()->where(['staff_id' => $staffId])->all();
        $groupIds = [];
        foreach ($result as $item) {
            $groupIds[] = $item->learning_group_id;
        }
        return $groupIds;
    }
}
