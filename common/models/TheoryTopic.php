<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "theory_topic".
 *
 * @property int $id
 * @property string $topic_number
 * @property string $topic_text
 *
 * @property TheoryQuestion[] $theoryQuestions
 */
class TheoryTopic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'theory_topic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['topic_number', 'topic_text'], 'required'],
            [['topic_number', 'topic_text'], 'string', 'max' => 255],
            [['topic_number'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'topic_number' => 'Topic Number',
            'topic_text' => 'Topic Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheoryQuestions()
    {
        return $this->hasMany(TheoryQuestion::className(), ['topic_id' => 'id']);
    }
}
