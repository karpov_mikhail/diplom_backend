<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "theory_question".
 *
 * @property int $id
 * @property integer $question_number
 * @property string $question_text
 * @property int $card_id
 * @property int $topic_id
 * @property int $image_id
 * @property string $update_status
 * @property string $last_update
 * @property string $note
 *
 * @property TheoryAnswer[] $theoryAnswers
 * @property TheoryAnswerLog[] $theoryAnswerLogs
 * @property TheoryCard $card
 * @property File $image
 * @property TheoryTopic $topic
 */
class TheoryQuestion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'theory_question';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question_text', 'card_id', 'topic_id', 'note'], 'required'],
            [['question_text', 'note'], 'string'],
            [['card_id', 'topic_id', 'image_id'], 'integer'],
            [['last_update'], 'safe'],
            [['update_status'], 'string', 'max' => 1],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => TheoryCard::className(), 'targetAttribute' => ['card_id' => 'id']],
            [['image_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['image_id' => 'id']],
            [['topic_id'], 'exist', 'skipOnError' => true, 'targetClass' => TheoryTopic::className(), 'targetAttribute' => ['topic_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question_text' => 'Question Text',
            'card_id' => 'Card ID',
            'topic_id' => 'Topic ID',
            'image_id' => 'Image ID',
            'update_status' => 'Update Status',
            'last_update' => 'Last Update',
            'note' => 'Note',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheoryAnswers()
    {
        return $this->hasMany(TheoryAnswer::className(), ['question_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheoryAnswerLogs()
    {
        return $this->hasMany(TheoryAnswerLog::className(), ['question_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCard()
    {
        return $this->hasOne(TheoryCard::className(), ['id' => 'card_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::className(), ['id' => 'image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTopic()
    {
        return $this->hasOne(TheoryTopic::className(), ['id' => 'topic_id']);
    }

    public static function getCardNumbers($questionIds)
    {
        /** @var TheoryQuestion[] $questions */
        $questions = TheoryQuestion::find()->where(['id' => $questionIds])->select(['id', 'card_id'])->asArray()->all();
        $cardIds = array_column($questions, 'card_id');
        $cards = TheoryCard::getCardNumbers($cardIds);
        $cardAutoClassString = TheoryCard::getCardsAutoClassString($cardIds);
        $questionCardNumbers = [];
        foreach ($questions as $question) {
            $questionCardNumbers[$question['id']] = 'Билет ' . $cards[$question['card_id']] . ' : ' . $cardAutoClassString[$question['card_id']];
        }
        return $questionCardNumbers;
    }
}
