<?php

namespace common\widgets;

use common\widgets\assets\MyDataTableAsset;
use yii\base\Widget;
use yii\helpers\Url;

class MyDataTable extends Widget
{
    public $data;
    public $columns;
    public $detailUrl;
    public $deleteUrl;
    public $urls;
    public $delUrls;

    public function init()
    {
        parent::init();

        if(empty($this->data) || empty($this->columns) || empty($this->detailUrl)) {
            return "";
        }

        // Создание ссылок для детальных страниц
        $parseUrl = explode("#", $this->detailUrl);
        if(count($parseUrl) < 2) {
            return "";
        }
        $needColumn = $parseUrl[1];
        if(!in_array($needColumn, array_keys($this->columns))) {
            return "";
        }
        foreach ($this->data as $key=>$datum) {
            $this->urls[$key] = $parseUrl[0] . $datum[$needColumn] . $parseUrl[2];
        }

        if(!empty($this->deleteUrl)) {
            $parseUrl = explode("#", $this->deleteUrl);
            if(count($parseUrl) < 2) {
                return "";
            }
            $needColumn = $parseUrl[1];
            if(!in_array($needColumn, array_keys($this->columns))) {
                return "";
            }
            foreach ($this->data as $key=>$datum) {
                $url = $parseUrl[0] . $datum[$needColumn] . $parseUrl[2];
                $this->delUrls[$key] = Url::toRoute([$url], true);
            }
        }

        return true;
    }

    public function run()
    {
        if(empty($this->data) || empty($this->columns) || empty($this->detailUrl) || empty($this->urls)) {
            return "";
        }
        return $this->render('myDataTable',
            [
                "data" => $this->data,
                "columns" => $this->columns,
                "detailUrl" => $this->detailUrl,
                "urls" => $this->urls,
                "delUrls" => $this->delUrls
            ]);
    }

    /**
     * Registers the needed assets
     */
    public function registerAssets()
    {
        $view = $this->getView();
        MyDataTableAsset::register($view);
    }
}