<?php

namespace common\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * Class FormWidget
 * @package common\widgets
 * @property $form yii\bootstrap\ActiveForm
 */
class FormWidget extends Widget
{
    public $model;
    public $input;
    public $class;
    public $id;
    private $form;
    private $htmlInputs;
    private $formOptions;

    public function init()
    {
        parent::init();

        if(empty($this->model) || empty($this->id) || empty($this->input) || !is_array($this->input)) {
            return false;
        }

        $this->formOptions = $this->getFormOptions();

        $this->form = ActiveForm::begin();
        $this->htmlInputs = [];
        $this->getHtmlString($this->input, $this->model);

        return true;
    }

    public function run()
    {
        return $this->render('formWidget', [
            'form' => $this->form, 'id' => $this->id, 'inputs' => $this->htmlInputs, 'formOptions' => $this->formOptions
        ]);
    }

    /**
     * Метод для получения HTML строки с формой
     * @param $inputs
     * @param $model
     */
    private function getHtmlString($inputs, $model)
    {
        foreach ($inputs as $item) {
            $class = $item['class'];
            switch ($item['type']) {
                case 'text': case 'email': case 'password':
                    $class = implode(' ', ['form-control', $class]);
                    $this->htmlInputs[] = $this->form->field($model, $item['attr'])->input($item['type'], ['class' => trim($class)])->label($item['label']);
                    break;
                case 'textarea':
                    $class = implode(' ', ['form-control form-widget-textarea', $class]);
                    $this->htmlInputs[] = $this->form->field($model, $item['attr'])->textarea(['class' => trim($class)])->label($item['label']);
                    break;
                case 'hidden':
                    $class = implode(' ', ['form-control', $class]);
                    $field = $this->form->field($model, $item['attr'], ['options' => ['class' => 'form-widget-hidden']])->input($item['type'], ['class' => trim($class)])->label($item['label']);
                    $field->template = '{input}{error}';
                    $field->errorOptions = ['class' => 'form-widget-hidden'];
                    $this->htmlInputs[] = $field;
                    break;
                case 'phone':
                    $class = implode(' ', ['js-phone-mask form-control', $class]);
                    $this->htmlInputs[] = $this->form->field($model, $item['attr'])->input('text', ['class' => trim($class)])->label($item['label']);
                    break;
                case 'checkboxList':
                    $class = implode(' ', ['', $class]);
                    $this->htmlInputs[] = $this->form->field($model, $item['attr'])->checkboxList($item['items'], ['class' => trim($class)])->label($item['label']);
                    break;
                case 'radioList':
                    $class = implode(' ', ['', $class]);
                    $this->htmlInputs[] = $this->form->field($model, $item['attr'])->radioList($item['items'], ['class' => trim($class)])->label($item['label']);
                    break;
                case 'checkbox':
                    $class = implode(' ', ['', $class]);
                    $this->htmlInputs[] = $this->form->field($model, $item['attr'])->checkbox(['class' => trim($class)])->label($item['label']);
                    break;
                case 'select':
                    $class = implode(' ', ['form-control', $class]);
                    $options = ['class' => trim($class)];
                    if($item['disabled']){
                        $options['disabled'] = 'disabled';
                    }
                    $this->htmlInputs[] = $this->form->field($model, $item['attr'])->dropDownList($item['items'], $options)->label($item['label']);
                    break;
                case 'file':
                    $class = implode(' ', ['', $class]);
                    $this->htmlInputs[] = $this->form->field($model, $item['attr'])->fileInput(['class' => trim($class)])->label($item['label']);
                    break;
                case 'image':
                    $class = implode(' ', ['form-widget-img control-form', $class]);
                    $this->htmlInputs[] =
                        '<div class="form-widget-div"><label for="' . $item['attr'] . '" class="control-label">' . $item['label'] . '</label><img src="' . $item['src'] . '" class="' . $class . '" id="' . $item['attr'] . '"/></div>';
                    break;
                case 'form':
                    $this->htmlInputs[] = "<h2>" . $item['label'] . "</h2>";
                    $this->getHtmlString($item['inputs'], $item['model']);
                    break;
                case 'submit':
                    $class = implode(' ', ['btn form-widget-btn', $class]);
                    $this->htmlInputs[] = Html::submitButton($item['label'], ['class' => trim($class), 'name' => 'submit-button']);
                    break;
                case 'reset':
                    $class = implode(' ', ['btn form-widget-btn js-form-widget-reset', $class]);
                    $this->htmlInputs[] = Html::resetButton($item['label'], ['class' => trim($class), 'name' => 'reset-button']);
                    break;
                case 'button':
                    $class = implode(' ', ['btn form-widget-btn', $class]);
                    $this->htmlInputs[] = Html::button($item['label'], ['class' => trim($class), 'name' => 'button']);
                    break;
            }
        }
    }

    /**
     * Метод для получения опций для формы
     * @return array
     */
    private function getFormOptions()
    {
        $options = [];
        $options['class'] = 'form-widget';
        foreach ($this->input as $input) {
            if($input['type'] == 'file' && empty($options['enctype'])) {
                $options['enctype'] = 'multipart/form-data';
            }
        }
        return $options;
    }
}