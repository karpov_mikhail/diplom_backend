<?php

namespace common\widgets;

use yii\base\Widget;

/**
 * Class FrontNavBar
 * @package common\widgets
 *
 * @property array $menuItems
 */
class FrontNavBar extends Widget
{
    public $menuItems;

    public function init()
    {
        parent::init();

        if(empty($this->menuItems)) {
            return false;
        }

        return true;
    }

    public function run()
    {
        return $this->render('frontNavBar', [
            'menuItems' => $this->menuItems
        ]);
    }
}