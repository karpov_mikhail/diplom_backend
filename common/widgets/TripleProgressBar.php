<?php

namespace common\widgets;

use yii\base\Widget;

class TripleProgressBar extends Widget
{
    public $progressData;

    private $successProgress;
    private $failureProgress;
    private $otherProgress;
    private $successText;
    private $failureText;
    private $otherText;

    public function init()
    {
        parent::init();

        if(empty($this->progressData)) {
            return "";
        }
        $this->successProgress = $this->progressData['success'] / $this->progressData['all'] * 100;
        $this->failureProgress = $this->progressData['failure'] / $this->progressData['all'] * 100;
        $this->otherProgress = 100 - $this->successProgress - $this->failureProgress;
        $successAnswers = $this->progressData['success'];
        $failureAnswers = $this->progressData['failure'];
        $otherAnswers = 800 - $this->progressData['success'] - $this->progressData['failure'];

        $this->successText = $this->getRightText('success', $successAnswers);
        $this->failureText = $this->getRightText('failure', $failureAnswers);
        $this->otherText = $this->getRightText('other', $otherAnswers);

        return true;
    }

    public function run()
    {
        return $this->render('tripleProgressBar',
            [
                "progressData" => $this->progressData,
                "successProgress" => $this->successProgress,
                "failureProgress" => $this->failureProgress,
                "otherProgress" => $this->otherProgress,
                "successText" => $this->successText,
                "failureText" => $this->failureText,
                "otherText" => $this->otherText,
            ]);
    }

    /**
     * Функция генерации текста в правильном падеже
     * @param $regime
     * @param $count
     */
    private function getRightText($regime, $count)
    {
        $string = $count;
        switch ($regime) {
            case 'success':
                $string .= ' ' . $this->true_wordform($count, 'Правильный', 'Правильных', 'Правильных');
                break;
            case 'failure':
                $string .= ' ' . $this->true_wordform($count, 'Неправильный', 'Неправильных', 'Неправильных');
                break;
            case 'other':
                $string .= ' ' . $this->true_wordform($count, 'Оставшийся', 'Оставшихся', 'Оставшихся');
                break;
        }
        $string .= ' ' . $this->true_wordform($count, 'ответ', 'ответа', 'ответов');
        return $string;
    }

    /*
    * $num число, от которого будет зависеть форма слова
    * $form_for_1 первая форма слова, например Товар
    * $form_for_2 вторая форма слова - Товара
    * $form_for_5 третья форма множественного числа слова - Товаров
    */
    private function true_wordform($num, $form_for_1, $form_for_2, $form_for_5){
        $num = abs($num) % 100; // берем число по модулю и сбрасываем сотни (делим на 100, а остаток присваиваем переменной $num)
        $num_x = $num % 10; // сбрасываем десятки и записываем в новую переменную
        if ($num > 10 && $num < 20) // если число принадлежит отрезку [11;19]
            return $form_for_5;
        if ($num_x > 1 && $num_x < 5) // иначе если число оканчивается на 2,3,4
            return $form_for_2;
        if ($num_x == 1) // иначе если оканчивается на 1
            return $form_for_1;
        return $form_for_5;
    }
}