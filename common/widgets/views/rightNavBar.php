<?php
use yii\helpers\Html;
use common\widgets\assets\RightNavBarAsset;
/** @var $menuItems array */
/** @var $this \yii\web\View */
RightNavBarAsset::register($this);
?>

<nav id="right-nav-bar">
    <? foreach ($menuItems as $item) { ?>
        <?= Html::a($item['label'], $item['url'], $item['options'])?>
    <? } ?>
</nav>
