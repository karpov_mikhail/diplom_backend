<?php
use yii\helpers\Html;
use common\widgets\assets\FrontNavBarAsset;
/** @var $menuItems array */
/** @var $this \yii\web\View */
FrontNavBarAsset::register($this);
?>

<nav id="front-nav-bar">
    <? foreach ($menuItems as $item) { ?>
        <?= Html::a($item['label'], $item['url'], $item['options'])?>
    <? } ?>
</nav>
