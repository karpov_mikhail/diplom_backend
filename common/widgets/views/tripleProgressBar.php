<?php
/** @var $this \yii\web\View */
/** @var $progressData string[] */
/** @var $successProgress float */
/** @var $failureProgress float */
/** @var $otherProgress float */
/** @var $successText string */
/** @var $failureText string */
/** @var $otherText string */
\common\widgets\assets\TripleProgressBarAsset::register($this);
?>
<div id="line-progress-bar">
    <?= \yii\bootstrap\Progress::widget([
            'bars' => [
            ['percent' => $successProgress, 'options' => ['class' => 'progress-bar-success']],
            ['percent' => $failureProgress, 'options' => ['class' => 'progress-bar-danger']],
            ['percent' => $otherProgress, 'options' => ['class' => 'progress-bar-primary']],
        ]
    ]);?>
    <div class="widget-progress-bar-info">
        <div class="info-div">
            <div class="info-span success-info-span"></div><p> - <?= $successText . ' ( ' . $successProgress . '% )'?></p>
        </div>
        <div class="info-div">
            <div class="info-span failure-info-span"></div><p> - <?= $failureText . ' ( ' . $failureProgress . '% )'?></p>
        </div>
        <div class="info-div">
            <div class="info-span other-info-span"></div><p> - <?= $otherText . ' ( ' . $otherProgress . '% )'?></p>
        </div>
    </div>
</div>
