<?php
/** @var $this \yii\web\View */
/** @var $id string */
/** @var $progressData array */
\common\widgets\assets\RadialProgressBarAsset::register($this);
?>
<div id="<?= $id?>">

</div>
<script>
    function drawChart<?=$id?>() {
        // Define the chart to be drawn.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Ответы');
        data.addColumn('number', 'Процент');
        data.addRows([
            ['Правильные', <?= $progressData['success']?>],
            ['Неправильные', <?= $progressData['failure']?>],
            ['Осталось', <?= $progressData['other']?>]
        ]);

        // Set chart options
        var options = {
            'width':$('#<?=$id?>').parent().width(),
            'height':$('#<?=$id?>').parent().width(),
            'pieHole': 0.4,
            'slices': {
                0: { 'color': '#5cb85c' },
                1: { 'color': '#d9534f' },
                2: { 'color': '#337ab7' }
            },
            'legend':{
                'position': 'bottom',
                'alignment': 'start'
            }

        };

        // Instantiate and draw the chart.
        var chart = new google.visualization.PieChart(document.getElementById('<?=$id?>'));
        chart.draw(data, options);
    }
    google.charts.setOnLoadCallback(drawChart<?=$id?>);
    //var pie = new d3pie("<?//= $id?>//", {
    //    data: {
    //        content: [
    //            { label: "Правильные", value: 100, color: '#5cb85c' },
    //            { label: "Неправильные", value: 50, color: '#d9534f' },
    //            { label: "Осталось", value: 650, color: '#337ab7' }
    //        ]
    //
    //    },
    //    size: {
    //        canvasWidth: $('#<?//=$id?>//').parent().width(),
    //        canvasHeight: $('#<?//=$id?>//').parent().width(),
    //        pieInnerRadius: "60%",
    //        pieOuterRadius: "90%"
    //    },
    //    labels: {
    //        outer: {
    //            format: "none"
    //        },
    //        inner: {
    //            format: "none"
    //        }
    //    }
    //});
</script>
