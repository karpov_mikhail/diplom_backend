<?php
/** @var $this \yii\web\View */
/** @var $data array */
/** @var $columns array */
/** @var $detailUrl string */
/** @var $urls string */
/** @var $delUrls string */
use common\widgets\assets\MyDataTableAsset;
MyDataTableAsset::register($this);
?>

<div class="table-div table-responsive table-bordered">
    <div class="table-head-div">
        <? foreach ($columns as $key=>$column) { ?>
            <div class="table-cell-div"><?=$column?></div>
        <? } ?>
        <? if (!empty($delUrls)) { ?>
            <div class="table-cell-div"></div>
        <? } ?>
    </div>
    <?foreach ($data as $dkey=>$datum) { ?>
        <div class="table-body-div">
            <? foreach ($columns as $key=>$column) { ?>
                <?= \yii\helpers\Html::a($datum[$key], [$urls[$dkey]], ['class' => 'table-cell-div table-link'])?>
            <? } ?>
            <? if (!empty($delUrls)) { ?>
                <div class="table-cell-div">
                    <button type="button" data-href="<?=$delUrls[$dkey]?>" class="btn btn-default glyphicon glyphicon-remove js-delete-button"></button>
                </div>
            <? } ?>
        </div>
    <? } ?>
</div>
