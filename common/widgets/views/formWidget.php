<?php
/** @var $this \yii\web\View */
/** @var $form \yii\bootstrap\ActiveForm */
/** @var $id string */
/** @var $inputs array */
/** @var $formOptions array */
\common\widgets\assets\FormWidgetAsset::register($this);
?>
<?$form = \yii\bootstrap\ActiveForm::begin([
    'id' => $id,
    'options' => $formOptions
]); ?>
<? foreach ($inputs as $input) {
    echo $input;
}?>
<? \yii\bootstrap\ActiveForm::end(); ?>
