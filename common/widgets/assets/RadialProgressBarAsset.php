<?php

namespace common\widgets\assets;

use yii\web\AssetBundle;

class RadialProgressBarAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets';
    public $js = [
        'https://www.gstatic.com/charts/loader.js',
        'js/radialProgressBar.js',
        'js/lib/radial-progressbar/d3pie.min.js'
    ];
    public $css = [
        'css/radialProgressBar.css'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'backend\assets\AppAsset',
        'frontend\assets\D3Asset'
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_BEGIN];
}