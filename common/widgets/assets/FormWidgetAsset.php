<?php

namespace common\widgets\assets;

use yii\web\AssetBundle;

class FormWidgetAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets';
    public $js = [
        'js/formWidget.js',
        'js/lib/maskedinput/jquery.maskedinput.js'
    ];
    public $css = [
        'css/formWidget.css'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'backend\assets\AppAsset'
    ];
}