<?php

namespace common\widgets\assets;

use yii\web\AssetBundle;

class FrontNavBarAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets';
    public $js = [
        'js/frontNavBar.js'
    ];
    public $css = [
        'css/frontNavBar.css'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'backend\assets\AppAsset'
    ];
}