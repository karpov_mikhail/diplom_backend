<?php

namespace common\widgets\assets;

use yii\web\AssetBundle;

class TripleProgressBarAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets';
    public $js = [
        'js/tripleProgressBar.js'
    ];
    public $css = [
        'css/tripleProgressBar.css'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'backend\assets\AppAsset'
    ];
    public $jsOptions = ['position' => \yii\web\View::POS_BEGIN];
}