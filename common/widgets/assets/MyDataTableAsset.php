<?php

namespace common\widgets\assets;

use yii\web\AssetBundle;

class MyDataTableAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets';
    public $css = [
        'css/myDataTable.css'
    ];
    public $js = [
        'js/myDataTable.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
}