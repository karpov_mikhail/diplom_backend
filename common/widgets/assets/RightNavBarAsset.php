<?php

namespace common\widgets\assets;

use yii\web\AssetBundle;

class RightNavBarAsset extends AssetBundle
{
    public $sourcePath = '@common/widgets';
    public $js = [
        'js/rightNavBar.js'
    ];
    public $css = [
        'css/rightNavBar.css'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'backend\assets\AppAsset'
    ];
}