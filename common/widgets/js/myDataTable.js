$(document).ready( function() {
    var isAjax = false;
    $('.js-delete-button').on('click', function () {
        var href = $(this).data('href');
        if(isAjax === false) {
            $.ajax({
                url: href,
                data: {
                    action: 'delete'
                },
                dataType: "json",
                type: "post",
                beforeSend: function() {
                    isAjax = true;
                },
                success: function(data) {
                    if (data.success == true) {
                        location.reload();
                    } else {
                        alert(data.message);
                    }
                },
                complete: function() {
                    isAjax = false;
                }
            });
        }
    });
});