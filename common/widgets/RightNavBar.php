<?php

namespace common\widgets;

use yii\base\Widget;

class RightNavBar extends Widget
{
    public $menuItems;

    public function init()
    {
        parent::init();

        return true;
    }

    public function run()
    {
        if(!empty($this->menuItems)) {
            return $this->render('rightNavBar', [
                'menuItems' => $this->menuItems
            ]);
        }
    }
}