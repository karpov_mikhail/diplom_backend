<?php

namespace common\widgets;

use yii\base\Widget;

class RadialProgressBar extends Widget
{
    public $id;
    public $progressData;

    public function init()
    {
        parent::init();

        if(empty($this->id) || empty($this->progressData) || !is_array($this->progressData)) {
            return false;
        }

        return true;
    }

    public function run()
    {
        return $this->render('radialProgressBar', ['id' => $this->id, 'progressData' => $this->progressData]);
    }
}