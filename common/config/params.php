<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'rightAnswerCountToPassedCard' => 18,
    'cardQuestionCount' => 20,
    'maxFailureCount' => 3
];
