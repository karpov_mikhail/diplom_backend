<?php

namespace frontend\models;

use common\models\User;
use yii\base\Model;

/**
 * Class EditUserForm
 * @package frontend\models
 *
 * @property $userId integer
 * @property $firstName string
 * @property $lastName string
 * @property $secondName string
 * @property $email string
 * @property $phoneNumber string
 * @property $password string
 * @property $repeatPassword string
 */
class EditUserForm extends Model
{
    public $userId;
    public $firstName;
    public $lastName;
    public $secondName;
    public $email;
    public $phoneNumber;
    public $password;
    public $repeatPassword;

    public function rules()
    {
        return [
            [['email'], 'required'],
            [['lastName', 'firstName', 'secondName', 'password', 'repeatPassword'], 'string', 'max' => 255],
            [['password', 'repeatPassword', 'phoneNumber'], 'string', 'max' => 20],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password'],

            ['password', 'checkPassword'],
            ['email', 'email']
        ];
    }

    public function checkUserId($attr)
    {
        if(!empty($this->userId)) {
            $this->user = User::findOne($this->userId);
            if($this->user == null) {
                $this->addError($attr, 'Данного пользователя не существует');
            }
        }
    }

    public function checkPassword($attr)
    {
        if(!empty($this->password)) {
            if(strlen($this->password) < 6) {
                $this->addError($attr, 'Длина пароля минимум 6 символов');
            }
        }
    }

    public function loadModel()
    {
        if(!\Yii::$app->user->isGuest) {
            $user = User::findOne(\Yii::$app->user->getId());
            $this->userId = $user->id;
            $this->firstName = $user->first_name;
            $this->lastName = $user->last_name;
            $this->secondName = $user->second_name;
            $this->email = $user->email;
            $this->phoneNumber = $user->phone_number;
        }
    }

    public function saveModel($data)
    {
        if($this->load($data)) {
            if ($this->validate()) {
                $user = User::findOne($this->userId);
                if(!empty($this->password)) {
                    $user->setPassword($this->password);
                }
                $user->first_name = $this->firstName;
                $user->last_name = $this->lastName;
                $user->second_name = $this->secondName;
                $user->email = $this->email;
                $user->phone_number = $this->phoneNumber;
                return $user->save();
            }
        }
        return false;
    }

    public function getWidgetInputs()
    {
        $inputs = [
            ['type' => 'hidden', 'attr' => 'userId', 'label' => 'ID пользователя'],
            ['type' => 'text', 'attr' => 'lastName', 'label' => 'Фамилия'],
            ['type' => 'text', 'attr' => 'firstName', 'label' => 'Имя'],
            ['type' => 'text', 'attr' => 'secondName', 'label' => 'Отчество'],
            ['type' => 'email', 'attr' => 'email', 'label' => 'E-mail'],
            ['type' => 'phone', 'attr' => 'phoneNumber', 'label' => 'Номер телефона'],
            ['type' => 'password', 'attr' => 'password', 'label' => 'Пароль'],
            ['type' => 'password', 'attr' => 'repeatPassword', 'label' => 'Повторите пароль'],
            ['type' => 'submit', 'label' => 'Сохранить', 'class' => 'btn-success'],
            ['type' => 'reset', 'label' => 'Отмена', 'class' => 'btn-danger']
        ];
        return $inputs;
    }
}