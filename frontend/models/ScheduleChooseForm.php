<?php

namespace frontend\models;

use common\models\PracticeSchedule;
use yii\base\Model;

class ScheduleChooseForm extends Model
{
    public $learnerId;
    public $practiceCarId;
    public $date;
    public $arDate;
    public $time;
    public $arTime;

    public function rules()
    {
        return [
            [['date'], 'date', 'format' => 'php:d.m.Y'],
            [['time'], 'time', 'format' => 'php:H:i'],

            ['learnerId', 'exist', 'targetClass' => 'common\models\Staff', 'targetAttribute' => 'id'],
            ['practiceCarId', 'exist', 'targetClass' => 'common\models\PracticeCar', 'targetAttribute' => 'id'],
        ];
    }

    public function loadModel($learnerId, $carId)
    {
        if(!empty($learnerId) && !empty($carId)) {
            $this->learnerId = $learnerId;
            $this->practiceCarId = $carId;
            $startDate = new \DateTime();
            $interval = new \DateInterval('P1D');
            $startDate->add($interval);
            for($i = 0; $i < 7; $i++) {
                $this->arDate[$startDate->format('d.m.Y')] = $startDate->format('d.m.Y');
                $startDate->add($interval);
            }
            $this->arTime = ['08:00' => '08:00',
                '10:00' => '10:00',
                '13:00' => '13:00',
                '15:00' => '15:00',
                '17:00' => '17:00'
            ];
        }
    }

    public function saveModel($data)
    {
        if($this->load($data)) {
            if($this->validate()) {
                $schedule = PracticeSchedule::find()->where([
                    'learner_id' => $this->learnerId,
                    'practice_car_id' => $this->practiceCarId,
                    'datetime' => \DateTime::createFromFormat('d.m.Y H:i', $this->date . " " . $this->time)->format('Y-m-d H:i:s')
                ])->count();
                                if($schedule != 0) {
                    $this->addError('time', 'Время занято');
                    return false;
                }
                $newSchedule = new PracticeSchedule();
                $newSchedule->datetime = \DateTime::createFromFormat('d.m.Y H:i', $this->date . " " . $this->time)->format('Y-m-d H:i:s');
                $newSchedule->learner_id = $this->learnerId;
                $newSchedule->practice_car_id = $this->practiceCarId;
                return $newSchedule->save();
            }
        }
    }

    public function getWidgetInputs()
    {
        $inputs = [
            ['type' => 'select', 'attr' => 'date', 'label' => 'Дата занятия', 'items' => $this->arDate],
            ['type' => 'select', 'attr' => 'time', 'label' => 'Время занятия', 'items' => $this->arTime],
            ['type' => 'submit', 'label' => 'Выбрать время', 'class' => 'btn-success'],
            ['type' => 'reset', 'label' => 'Отмена', 'class' => 'btn-danger']
        ];
        return $inputs;
    }
}