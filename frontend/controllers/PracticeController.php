<?php

namespace frontend\controllers;

use common\models\LearningGroup;
use common\models\PracticeCar;
use common\models\PracticeSchedule;
use common\models\Staff;
use common\models\User;
use common\models\UserLearningGroup;
use common\models\UserPracticeCar;
use frontend\models\ScheduleChooseForm;
use yii\web\Controller;

class PracticeController extends Controller
{
    public $layout = 'main-right-nav';

    public function beforeAction($action)
    {
        $this->view->params['menuItems'] = [
            ['label' => 'Основная информация', 'url' => ['personal/index'], 'options' => []],
            ['label' => 'Редактирование информации', 'url' => ['personal/edit-user'], 'options' => []],
            ['label' => 'Практические занятия', 'url' => ['practice/index'], 'options' => []],
            ['label' => 'Статистика обучения', 'url' => [], 'options' => []]
        ];

        if(!in_array($action->id, ['login'])) {
            if(\Yii::$app->user->isGuest) {
                $this->redirect(['site/login']);
            }

            $user = User::findIdentity(\Yii::$app->user->id);
            if($user == null) {
                $this->redirect(['site/login']);
            }
        }
        $this->view->title = 'Практические занятия';

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $user = User::findOne(\Yii::$app->user->getId());
        $renderData = [];
        $now = new \DateTime();
        $nowString = $now->format('Y-m-d H:i:s');
        foreach ($user->staff as $staff) {
            switch ($staff->group->code) {
                case "instructor":
                    $scheduleData = [];
                    $salary = 0;
                    $car = PracticeCar::findOne(['teacher_id' => $staff->id]);
                    if(!empty($car->id)) {

                        $firstDayOfMonth = new \DateTime();
                        $firstDayOfMonthString = $firstDayOfMonth->modify('first day of this month')->format("Y-m-d 00:00:00");
                        $interval = new \DateInterval('P1D');
                        $yesterdayString = $now->sub($interval)->format('Y-m-d H:i:s');
                        $now->add($interval);

                        $scheduleCount = PracticeSchedule::find()
                            ->where(['practice_car_id' => $car->id])
                            ->andWhere(['<', 'datetime', $nowString])
                            ->andWhere(['>', 'datetime', $firstDayOfMonthString])
                            ->count();

                        $scheduleData = PracticeSchedule::find()
                            ->where(['practice_car_id' => $car->id])
                            ->andWhere(['>', 'datetime', $yesterdayString])
                            ->all();
                        $salary = $staff->salary_per_hour * $scheduleCount * 2;
                    }
                    $renderData[] = $this->renderPartial('instructor', ['schedule' => $scheduleData, 'car' => $car, 'salary' => $salary]);
                    break;
                case "learner":
                    $practiceData = [];
                    $scheduleData = [];
                    foreach ($staff->learningGroups as $learningGroup) {
                        $groupAutoClassId = $learningGroup->auto_class_id;
                        $carIds = PracticeCar::getCarIdsByAutoClassId($groupAutoClassId);
                        $userPracticeCar = UserPracticeCar::findOne(['learner_id' => $staff->id, 'practice_car_id' => $carIds]);
                        if(empty($userPracticeCar)) {
                            $practiceData[$learningGroup->id] = false;
                        } else {
                            $practiceData[$learningGroup->id] = $userPracticeCar->practiceCar;
                        }
                        $schedule = PracticeSchedule::find()->where([
                            'learner_id' => $userPracticeCar->learner_id,
                            'practice_car_id' => $userPracticeCar->practice_car_id,

                        ])->andWhere(['>', 'datetime', $nowString])->all();
                        $scheduleData[$learningGroup->id] = $schedule;
                    }
                    $renderData[] = $this->renderPartial('learner', ['staff' => $staff, 'practiceData' => $practiceData, 'scheduleData' => $scheduleData]);
                    break;
            }
        }
        return $this->render('index', ['renderData' => $renderData]);
    }

    public function actionChooseCar($staff_id = null, $group_id = null)
    {
        $staff_id = intval($staff_id);
        $group_id = intval($group_id);
        if(empty($group_id) || empty($staff_id) || !is_int($group_id) || !is_int($staff_id)) {
            $this->redirect(['practice/index']);
        }
        $isInStaff = Staff::find()->where(['id' => $staff_id, 'user_id' => \Yii::$app->user->id])->count();
        $isInGroup = UserLearningGroup::find()->where(['learning_group_id' => $group_id, 'staff_id' => $staff_id])->count();
        if($isInGroup == 0 || $isInStaff == 0) {
            $this->redirect(['practice/index']);
        }
        $learningGroup = LearningGroup::findOne($group_id);
        $practiceCars = PracticeCar::find()->where(['auto_class_id' => $learningGroup->auto_class_id])->all();
        return $this->render('choose-car', ['practiceCars' => $practiceCars, 'staffId' => $staff_id]);
    }

    public function actionSchedule($staff_id = null, $car_id = null)
    {
        $staff_id = intval($staff_id);
        $car_id = intval($car_id);
        if(empty($car_id) || empty($staff_id) || !is_int($car_id) || !is_int($staff_id)) {
            $this->redirect(['practice/index']);
        }
        $isInStaff = Staff::find()->where(['id' => $staff_id, 'user_id' => \Yii::$app->user->id])->count();
        $isCar = PracticeCar::find()->where(['id' => $car_id])->count();
        $isInCar = UserPracticeCar::find()->where(['practice_car_id' => $car_id, 'learner_id' => $staff_id])->count();
        if($isCar == 0 || $isInStaff == 0 || $isInCar == 0) {
            $this->redirect(['practice/index']);
        }

        $model = new ScheduleChooseForm();
        $model->loadModel($staff_id, $car_id);
        if(\Yii::$app->request->isPost) {
            $result = $model->saveModel(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['practice/index']);
            }
        }

        return $this->render('schedule', ['model' => $model]);
    }

    public function actionAjaxChooseCar()
    {
        $post = \Yii::$app->request->post();
        if(empty($post['car_id']) || empty($post['staff_id'])) {
            return json_encode(['success' => false]);
        }
        $staffCount = Staff::find()->where(['id' => $post['staff_id']])->count();
        $carCount = PracticeCar::find()->where(['id' => $post['car_id']])->count();
        if($staffCount == 0 || $carCount == 0) {
            return json_encode(['success' => false]);
        }
        $newUserPracticeCar = new UserPracticeCar();
        $newUserPracticeCar->learner_id = $post['staff_id'];
        $newUserPracticeCar->practice_car_id = $post['car_id'];
        $newUserPracticeCar->save();
        return json_encode(['success' => true]);
    }
}