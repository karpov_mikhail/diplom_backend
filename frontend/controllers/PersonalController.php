<?php

namespace frontend\controllers;

use common\models\Group;
use common\models\GroupOneTimeCode;
use common\models\LearningGroup;
use common\models\Staff;
use common\models\TheoryAnswerLog;
use common\models\User;
use common\models\UserGroup;
use common\models\UserLearningGroup;
use frontend\assets\PersonalAsset;
use frontend\models\EditUserForm;
use frontend\models\PersonalCodeForm;
use yii\web\Controller;

class PersonalController extends Controller
{
    public $layout = 'main-right-nav';

    public function beforeAction($action)
    {
        $this->view->params['menuItems'] = [
            ['label' => 'Основная информация', 'url' => ['personal/index'], 'options' => []],
            ['label' => 'Редактирование информации', 'url' => ['personal/edit-user'], 'options' => []],
            ['label' => 'Практические занятия', 'url' => ['practice/index'], 'options' => []],
            ['label' => 'Статистика обучения', 'url' => [], 'options' => []]
        ];

        if(!in_array($action->id, ['login'])) {
            if(\Yii::$app->user->isGuest) {
                $this->redirect(['site/login']);
            }

            $user = User::findIdentity(\Yii::$app->user->id);
            if($user == null) {
                $this->redirect(['site/login']);
            }
        }
        $this->view->title = 'Личный кабинет';
        PersonalAsset::register($this->view);

        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $user = User::findIdentity(\Yii::$app->user->getId());
        $renderData = [];
        foreach ($user->staff as $staff) {
            /** @var $staff Staff */
            switch ($staff->group_id) {
                case Group::findOne(['code' => 'teacher'])->id:
                    /** @var LearningGroup[] $learnGroup */
                    $learnGroup = LearningGroup::find()->where(['teacher_id' => $staff->id])->all();
                    $learnGroupLearners = [];
                    $learnersSuccessCardStatistic = [];
                    $learnersTopId = [];
                    $learnersBotId = [];
                    foreach ($learnGroup as $group) {
                        /** @var UserLearningGroup[] $result */
                        $result = UserLearningGroup::find()->where(['learning_group_id' => $group->id])->all();
                        foreach ($result as $learner) {
                            $learnGroupLearners[$learner->learning_group_id][$learner->staff_id] = $learner->staff;
                            $learnersSuccessCardStatistic[$learner->learning_group_id][$learner->staff_id] = $learner->staff->getCardStatistic()['success'];
                        }
                        foreach ($learnersSuccessCardStatistic as $key=>$stat) {
                            $learnersSortSuccessStatistic = $stat;
                            arsort($learnersSortSuccessStatistic);
                            $learnersSortIds[$key] = array_keys($learnersSortSuccessStatistic);
                            $learnersTopId[$key] = [$learnersSortIds[$key][0], $learnersSortIds[$key][1]];
                            $learnersBotId[$key] = [$learnersSortIds[$key][count($learnersSortIds[$key])-1], $learnersSortIds[$key][count($learnersSortIds[$key])-2]];
                        }
                    }
                    $renderData[] = $this->renderPartial('teacher', [
                        'learnGroup' => $learnGroup,
                        'learnGroupLearners' => $learnGroupLearners,
                        'learnersTopId' => $learnersTopId,
                        'learnersBotId' => $learnersBotId
                    ]);
                    break;
                case Group::findOne(['code' => 'learner'])->id:
                    $cardStatistic = $staff->getCardStatistic();
                    $renderData[] = $this->renderPartial('learner', ['cardStatistic' => $cardStatistic]);
                    break;
                case Group::findOne(['code' => 'instructor'])->id: break;
            }
        }

        return $this->render('index', ['user' => $user, 'renderData' => $renderData]);
    }

    public function actionEditUser()
    {
        $model = new EditUserForm();
        $model->loadModel();
        if(\Yii::$app->request->isPost) {
            $result = $model->saveModel(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['personal/index']);
            }
        }
        $this->view->title = "Редактирование данных";
        return $this->render('edit-user', [ 'model' => $model ]);
    }

    public function actionCheckCode()
    {
        try {
            if(\Yii::$app->request->isPost) {
                $code = \Yii::$app->request->post()['code'];
                $codeResult = GroupOneTimeCode::findOne(['unique_code' => $code]);
                if(!empty($codeResult) && $codeResult->activated) {
                    throw new \Exception('Код уже активирован');
                }
                if(!empty($codeResult) && !\Yii::$app->user->isGuest) {
                    $user = User::findIdentity(\Yii::$app->user->getId());
                    $user->first_name = $codeResult->staff->first_name;
                    $user->last_name = $codeResult->staff->last_name;
                    $user->second_name = $codeResult->staff->second_name;
                    $result = $user->save();
                    $userGroup = UserGroup::findOne(['user_id' => $user->id, 'group_id' => $codeResult->staff->group_id]);
                    if(empty($userGroup)) {
                        $userGroup = new UserGroup();
                        $userGroup->user_id = $user->id;
                        $userGroup->group_id = $codeResult->staff->group_id;
                        $result = $result && $userGroup->save();
                    }
                    $codeResult->activated = true;
                    $result = $result && $codeResult->save();
                    $codeResult->staff->user_id = $user->id;
                    $result = $result && $codeResult->staff->save();
                    if($result) {
                        return json_encode(['success' => true, 'message' => "Код успешно активирован"]);
                    } else {
                        throw new \Exception('Ошибка активирования кода');
                    }
                } else {
                    throw new \Exception('Код не найден');
                }
            }
        }
        catch (\Exception $e) {
            return json_encode(['success' => false, 'message' => $e->getMessage()]);
        }
    }
}