<?php

namespace frontend\controllers;

use common\models\AutoClass;
use common\models\TheoryAnswer;
use common\models\TheoryAnswerLog;
use common\models\TheoryCard;
use common\models\TheoryCardLog;
use common\models\TheoryQuestion;
use frontend\assets\TheoryAsset;
use yii\web\Controller;

class TheoryController extends Controller
{
    public $layout = 'main-right-nav';

    public function beforeAction($action)
    {
        $this->view->params['menuItems'] = [
            ['label' => 'Билеты', 'url' => ['theory/index'], 'options' => []],
            ['label' => 'Темы билеты', 'url' => [], 'options' => []],
            ['label' => 'Экзамен', 'url' => [], 'options' => []]
        ];
        TheoryAsset::register($this->view);
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $needAutoClass = AutoClass::findOne(['name' => 'B']);
        $cards = TheoryCard::find()->select('theory_card.*')
            ->leftJoin('theory_card_auto_class', '`theory_card_auto_class`.`card_id`=`theory_card`.`id`')
            ->where(['theory_card_auto_class.auto_class_id' => $needAutoClass->id])->all();
        $this->view->title = 'ПДД Билеты';
        $this->view->params['breadcrumbs'][] = $this->view->title;

        if(!\Yii::$app->user->isGuest) {
            /** @var TheoryCardLog[] $result */
            $result = TheoryCardLog::find()->where(['user_id' => \Yii::$app->user->getId()])->all();
            $cardsStatuses = [];
            foreach ($result as $log) {
                $cardsStatuses[$log->card->card_number] = boolval($log->passed) ? 'success' : 'failure';
            }
            \Yii::$app->session->set('cardStats', $cardsStatuses);
        }

        $cardsStatuses = \Yii::$app->session->get('cardStats');
        return $this->render('index', ['cards' => $cards, 'cardsStatuses' => $cardsStatuses]);
    }

    public function actionCard($cardId = null)
    {
        if(empty($cardId)) {
            $this->redirect(['theory/index']);
        }

        $card = TheoryCard::findOne($cardId);
        $this->view->title = 'Билет ' . $card->card_number;
        $this->view->params['breadcrumbs'][] = ['label' => 'ПДД Билеты', 'url' => ['theory/index']];
        $this->view->params['breadcrumbs'][] = $this->view->title;
        return $this->render('card', ['cardId' => $card->id]);
    }

    public function actionGetQuestion($questionNumber, $cardId)
    {
        if(count(\Yii::$app->session->get('questStats')) == \Yii::$app->params['cardQuestionCount']) {
            return $this->getEndPage($cardId);
        }

        if($questionNumber >= 1 && $questionNumber <= \Yii::$app->params['cardQuestionCount']) {
            $question = TheoryQuestion::findOne(['card_id' => $cardId, 'question_number' => $questionNumber]);
            $answers = TheoryAnswer::find()->where(['question_id' => $question->id])->all();
        }

        return $this->renderPartial('get-question', [
            'questionNumber' => $question->question_number,
            'cardId' => $cardId,
            'question' => $question,
            'answers' => $answers,
            'questionStatuses' => \Yii::$app->session->get('questStats')
        ]);
    }

    public function actionGetNextQuestion($questionNumber, $cardId)
    {
        if($questionNumber == 0) {
            \Yii::$app->session->remove('questStats');
        }

        if(count(\Yii::$app->session->get('questStats')) == \Yii::$app->params['cardQuestionCount']) {
            return $this->getEndPage($cardId);
        }

        if($questionNumber >= 0 && $questionNumber < \Yii::$app->params['cardQuestionCount']) {
            $question = TheoryQuestion::findOne(['card_id' => $cardId, 'question_number' => $questionNumber + 1]);
            $answers = TheoryAnswer::find()->where(['question_id' => $question->id])->all();
        }

        return $this->renderPartial('get-question', [
            'questionNumber' => $question->question_number,
            'cardId' => $cardId,
            'question' => $question,
            'answers' => $answers,
            'questionStatuses' => \Yii::$app->session->get('questStats')
        ]);
    }

    public function actionCheckAnswer($answerId, $questionNumber, $cardId)
    {
        try {
            $result = TheoryAnswer::findOne($answerId)->right_answer;

            $questStats = \Yii::$app->session->get('questStats');
            /** @var TheoryQuestion $question */
            $question = TheoryQuestion::findOne(['card_id' => $cardId, 'question_number' => $questionNumber]);

            if($result) {
                $data = ['success' => true, 'rightAnswer' => true];
                $questStats[$questionNumber]['status'] = 'success';
                $questStats[$questionNumber]['rightAnswerId'] = $answerId;
            } else {
                $data = ['success' => true, 'rightAnswer' => false];
                $rightAnswer = TheoryAnswer::findOne(['right_answer' => true, 'question_id' => $question->id]);
                $data = array_merge($data, [
                    'comment' => $question->note,
                    'rightAnswerId' => $rightAnswer->id
                ]);
                $questStats[$questionNumber]['status'] = 'failure';
                $questStats[$questionNumber]['rightAnswerId'] = $rightAnswer->id;
            }
            $questStats[$questionNumber]['answerId'] = $answerId;

            \Yii::$app->session->set('questStats', $questStats);
            TheoryAnswerLog::addToLog($question->id, $answerId, $result);
            return json_encode($data);
        } catch (\Exception $e) {
            return json_encode(['success' => false, 'message' => $e->getMessage()]);
        }
    }

    /**
     * Метод возвращает последнюю страницу тестирования
     *
     * @param $cardId integer
     * @return string
     */
    private function getEndPage($cardId) {
        $card = TheoryCard::findOne($cardId);
        $questStats = \Yii::$app->session->get('questStats');
        $successCount = 0;
        foreach ($questStats as $stat) {
            if($stat['status'] == 'success' && $stat['answerId'] == $stat['rightAnswerId']) {
                $successCount++;
            }
        }
        $cardStats = \Yii::$app->session->get('cardStats');
        $cardStats[$card->card_number] = $successCount < \Yii::$app->params['rightAnswerCountToPassedCard'] ? 'failure' : 'success';
        \Yii::$app->session->set('cardStats', $cardStats);
        TheoryCardLog::addToLog($cardId, $successCount);
        return $this->renderPartial('end-page', ['card' => $card, 'successCount' => $successCount]);
    }
}