<?php
use common\models\TheoryCard;
use yii\helpers\Html;
/** @var $this \yii\web\View */
/** @var $cards TheoryCard[] */
/** @var $cardsStatuses array */
$i=0;
?>

<div class="cards">
    <div class="grid-div">
        <? foreach ($cards as $card) {
            $class = 'grid-cell-div';
            switch ($cardsStatuses[$card->card_number]) {
                case 'success': $class .= ' alert-success'; break;
                case 'failure': $class .= ' alert-danger'; break;
            }
            echo Html::a($card->card_number, ['theory/card/'.$card->id], ['class' => $class]);
            $i++;
        } ?>
    </div>
</div>
