<?php
use common\models\File;
/** @var $this \yii\web\View */
/** @var $cardId integer */
/** @var $questionNumber integer */
/** @var $question \common\models\TheoryQuestion */
/** @var $answers \common\models\TheoryAnswer[] */
/** @var $questionStatuses array */
$questStat = $questionStatuses[$question->question_number];
?>

<div class="js-question" data-card-id="<?= $cardId ?>" data-question-number="<?= $questionNumber ?>">
    <div class="question-status-container">
        <? for ($i = 1; $i <= 20; $i++) { ?>
            <a data-question-number="<?= $i ?>" href="javascript:void(0)" class="js-question-status
            <? if($question->question_number != $i) {
                switch ($questionStatuses[$i]['status']) {
                    case 'success': echo $questionStatuses[$i]['status']; break;
                    case 'failure': echo $questionStatuses[$i]['status']; break;
                }
            } else { echo 'active'; }?>
            "><?= $i ?></a>
        <? } ?>
    </div>
    <img src="<?= File::getPath($question->image_id) ?>" alt="" class="question-img">
    <p class="question-text"><?= $question->question_text ?></p>
    <hr class="question-horiz-line">
    <div class="answers">
        <? foreach ($answers as $answer) { ?>
            <? if (!empty($questionStatuses[$question->question_number]['status'])) {
                $classes = "answer-text js-answer disabled";
                if($questStat['rightAnswerId'] == $answer->id) {
                    $classes .= ' alert-success';
                } else {
                    $classes .= ' alert-danger';
                }
                ?>
                <a class="<?=$classes?>" data-answer-id="<?= $answer->id ?>" href="javascript:void(0)">
                    <?= $answer->answer_text ?>
                    <?if($questStat['answerId'] == $answer->id) { ?>
                        <span style="color: red">Ваш ответ</span>
                    <? } ?>
                </a>
            <? } else { ?>
                <a class="answer-text js-answer" data-answer-id="<?= $answer->id ?>" href="javascript:void(0)"><?= $answer->answer_text ?></a>
            <?  }?>
        <? } ?>
    </div>
    <button class="next-question-btn btn btn-primary js-next-question"<? if(!empty($questStat)) { ?> style="display: block"<? } ?>>Дальше</button>
    <div class="comment-text alert-info"<? if(!empty($questStat)) { ?> style="display: block"<? } ?>>
        <? if (!empty($questStat)) {
            echo $question->note;
        }?>
    </div>
</div>