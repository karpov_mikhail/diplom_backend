<?php
use yii\helpers\Html;
/** @var $this \yii\web\View */
/** @var $card \common\models\TheoryCard */
/** @var $successCount integer */
?>
<div class="end-page-container">
    <? if($successCount < Yii::$app->params['rightAnswerCountToPassedCard']) {
        echo \yii\helpers\Html::img(['images/failure.png'], ['class' => 'end-page-image']);?>
        <p>Билет <?= $card->card_number ?> не решен</p>
    <? } else {
        echo \yii\helpers\Html::img(['images/success.png'], ['class' => 'end-page-image']);?>
        <p>Билет <?= $card->card_number ?> решен</p>
    <? } ?>
    <p><?= $successCount ?> правильных ответов из <?= Yii::$app->params['cardQuestionCount'] ?></p>
    <p><?= Html::a('К билетам', ['theory/index'])?></p>
</div>
