<?php

/* @var $this yii\web\View */
?>
<div class="site-index">

    <div class="offers-div">
        <div class="offers-block">
            <img src="/frontend/web/images/motocycle.jpg" alt="" class="offers-img">
            <h3>Обучение на права категории "A"</h3>
            <p>Стоимость: 10000 руб.</p>
        </div>
        <div class="offers-block">
            <img src="/frontend/web/images/auto.png" alt="" class="offers-img">
            <h3>Обучение на права категории "B"</h3>
            <p>Стоимость: 15000 руб.</p>
        </div>
        <div class="offers-block">
            <img src="/frontend/web/images/kamaz-5490.jpg" alt="" class="offers-img">
            <h3>Обучение на права категории "C"</h3>
            <p>Стоимость: 20000 руб.</p>
        </div>
    </div>

    <div class="phone-block">
        <h1>Если есть вопросы звоните по телефону<h1><p class="phone-number-big"><?= Yii::$app->params['phone']?></p><h1>и мы ответим на них</h1>
    </div>
</div>
