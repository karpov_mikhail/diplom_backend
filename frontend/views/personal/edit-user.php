<?php
/** @var $this \yii\web\View */
/** @var $model \frontend\models\EditUserForm */
?>
<?= \common\widgets\FormWidget::widget(['model' => $model, 'input' => $model->getWidgetInputs(), 'id'=>'edit-user-form'])?>