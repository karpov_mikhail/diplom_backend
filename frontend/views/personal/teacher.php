<?php
/** @var $this \yii\web\View */
/** @var $learnGroup \common\models\LearningGroup[] */
/** @var $learnGroupLearners array */
/** @var $learnersTopId array */
/** @var $learnersBotId array */
?>
<h1>Преподаватель</h1>
<? foreach($learnGroup as $group) { ?>
    <h2>Группа <?= $group->name ?></h2>

    <div class="row">
        <div class="col-sm-6"><h3>Лучшие курсанты</h3></div>
        <div class="col-sm-6"><h3>Худшие курсанты</h3></div>
        <? foreach ($learnersTopId[$group->id] as $learner) {
            /** @var $learner integer */?>
            <div class="col-sm-3">
                <p><?= $learnGroupLearners[$group->id][$learner]->last_name ?> <?= $learnGroupLearners[$group->id][$learner]->first_name ?></p>
                <?= \common\widgets\RadialProgressBar::widget(['id' => 'topGroup'.$group->id.'Learner'.$learner, 'progressData' => $learnGroupLearners[$group->id][$learner]->getCardStatistic()]); ?>
            </div>
        <? } ?>
        <? foreach ($learnersBotId[$group->id] as $learner) {
            /** @var $learner integer */?>
            <div class="col-sm-3">
                <p><?= $learnGroupLearners[$group->id][$learner]->last_name ?> <?= $learnGroupLearners[$group->id][$learner]->first_name ?></p>
                <?= \common\widgets\RadialProgressBar::widget(['id' => 'botGroup'.$group->id.'Learner'.$learner, 'progressData' => $learnGroupLearners[$group->id][$learner]->getCardStatistic()]); ?>
            </div>
        <? } ?>
    </div>


<? } ?>
