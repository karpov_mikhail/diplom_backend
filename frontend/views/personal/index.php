<?php
/** @var $this \yii\web\View */
/** @var $user \common\models\User */
/** @var $renderData string[] */
?>
<div class="personal">
    <h2>Общая информация</h2>
    <p>Фамилия: <?= $user->last_name ?></p>
    <p>Имя: <?= $user->first_name ?></p>
    <? if(!empty($user->second_name)) { ?>
        <p>Отчество: <?= $user->second_name ?></p>
    <? } ?>
    <h2>Персональный код</h2>
    <div class="div-form">
        <?= \yii\helpers\Html::beginForm('', 'post', ['id' => 'personal-code-form']);?>
        <?= \yii\helpers\Html::label('Персональный код', 'code')?>
        <?= \yii\helpers\Html::input('text', 'code', '', ['id' => 'code', 'class' => 'personal-code-input form-control']);?>
        <?= \yii\helpers\Html::submitButton('Проверить', ['class' => 'btn btn-success js-personal-code-btn']);?>
        <?= \yii\helpers\Html::endForm();?>
    </div>
    <? foreach ($renderData as $render) {
        echo $render;
    } ?>
</div>
