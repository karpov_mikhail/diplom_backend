<?php
/** @var $this \yii\web\View */
/** @var $staff \common\models\Staff */
/** @var $practiceData array */
/** @var $scheduleData array */
?>
<h1>Курсант <?=$staff->last_name . " " . $staff->first_name?></h1>
<? foreach ($staff->learningGroups as $learningGroup) { ?>
    <h2>Группа <?= $learningGroup->name ?></h2>
    <div>
        <? if ($practiceData[$learningGroup->id] === false) { ?>
            <?= \yii\helpers\Html::a('<span class="glyphicon glyphicon-plus"></span> Выбрать машину',
                ['practice/choose-car?staff_id=' . $staff->id . '&group_id=' . $learningGroup->id], ['class' => 'btn btn-success'])?>
        <? } else {
            /** @var \common\models\PracticeCar $car */
            $car = $practiceData[$learningGroup->id]
            ?>
            <div class="practice-car-div">
                <img src="<?= \common\models\File::getPath($car->car_photo_id)?>" alt="">
                <p>Марка: <?= $car->mark ?></p>
                <p>Модель: <?= $car->model ?></p>
                <p>Мощность: <?= $car->car_power ?> л/с</p>
                <?= \yii\helpers\Html::a('Выбрать время для занятия',
                    ['practice/schedule?staff_id=' . $staff->id . '&car_id=' . $car->id], ['class' => 'btn btn-success'])?>
                <? if(!empty($scheduleData[$learningGroup->id])) { ?>
                    <h3>Занятия</h3>
                    <? foreach ($scheduleData[$learningGroup->id] as $schedule) { /** @var $schedule \common\models\PracticeSchedule */?>
                        <p>Дата: <?= DateTime::createFromFormat("Y-m-d H:i:s", $schedule->datetime)->format("d.m.Y") ?>,
                            Время <?= DateTime::createFromFormat("Y-m-d H:i:s", $schedule->datetime)->format("H:i") ?></p>
                    <? } ?>
                <? } ?>
            </div>
        <? } ?>
    </div>
<? } ?>

