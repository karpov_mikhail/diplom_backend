<?php
/** @var $this \yii\web\View */
/** @var $practiceCars \common\models\PracticeCar[] */
/** @var $staffId integer */
?>
<h1>Выбор автомобиля</h1>
<div class="choose-car">
    <? foreach ($practiceCars as $car) { ?>
        <a href="<?= \yii\helpers\Url::to(['practice/ajax-choose-car'])?>" class="choose-practice-car-link js-choose-car"
           data-car-id="<?= $car->id ?>" data-staff-id="<?= $staffId ?>" data-success="<?= \yii\helpers\Url::to(['practice/index'])?>">
            <div class="choose-practice-car-div">
                <img src="<?= \common\models\File::getPath($car->car_photo_id)?>" alt="">
                <p>Марка: <?= $car->mark ?></p>
                <p>Модель: <?= $car->model ?></p>
                <p>Мощность: <?= $car->car_power ?> л/с</p>
            </div>
        </a>
    <? } ?>
</div>
