<?php
/** @var $this \yii\web\View */
/** @var $schedule \common\models\PracticeSchedule[] */
/** @var $car \common\models\PracticeCar */
/** @var $salary int */
?>
<div class="instructor-practice">
    <h1>Автомобиль</h1>
    <? if(empty($car->id)) {?>
        <h3>Вы не привязаны к автомобилю. Обратитесь к администратору</h3>
    <? } else { ?>
        <div class="practice-car-div">
            <img src="<?= \common\models\File::getPath($car->car_photo_id)?>" alt="">
            <p>Марка: <?= $car->mark ?></p>
            <p>Модель: <?= $car->model ?></p>
            <p>Мощность: <?= $car->car_power ?> л/с</p>
            <? if(!empty($schedule)) { ?>
                <h3>Занятия</h3>
                <? foreach ($schedule as $scheduleData) { /** @var $schedule \common\models\PracticeSchedule */?>
                    <p>Дата: <?= DateTime::createFromFormat("Y-m-d H:i:s", $scheduleData->datetime)->format("d.m.Y") ?>,
                        Время <?= DateTime::createFromFormat("Y-m-d H:i:s", $scheduleData->datetime)->format("H:i") ?>,
                        Курсант: <?= $scheduleData->learner->last_name?> <?= $scheduleData->learner->first_name ?> <?= $scheduleData->learner->second_name ?></p>
                <? } ?>
            <? } ?>
        </div>
        <h3>Вы за этот месяц заработали <?= $salary?> руб.</h3>
    <? } ?>
</div>
