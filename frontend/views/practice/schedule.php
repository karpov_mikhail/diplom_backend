<?php
/** @var $this \yii\web\View */
/** @var $model \frontend\models\ScheduleChooseForm */
?>
<?= \common\widgets\FormWidget::widget(['model' => $model, 'input' => $model->getWidgetInputs(), 'id'=>'schedule-choose-form'])?>