<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use common\widgets\FrontNavBar;
use common\widgets\RightNavBar;

AppAsset::register($this);
$menuItems = [
    ['label' => 'Главная', 'url' => ['site/index'], 'options' => []],
    ['label' => 'Новости', 'url' => [], 'options' => []],
    ['label' => 'Билеты', 'url' => ['theory/index'], 'options' => []],
    ['label' => 'Контакты', 'url' => ['site/contact'], 'options' => []]
];
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?=\Yii::getAlias('@web')?>/images/favicon-32x32.png">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> | <?=Yii::$app->name?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="parent">
    <header id="header">
        <div id="top">
            <div class="top-container">
                <div class="top-left">
                    <img src="<?=\Yii::getAlias('@web')?>/images/logo5+.png" alt="Логотип" class="top-logo">
                    <h2 class="top-title"><a href="<?=Yii::$app->homeUrl?>"><?=Yii::$app->name?></a></h2>
                </div>
                <div class="top-right">
                    <? if (Yii::$app->user->isGuest) { ?>
                        <p><?=Html::a('Вход', ['/site/login'])?> | <?=Html::a('Регистрация', ['/site/signup'])?></p>
                    <? } else { ?>
                        <p><?=Html::a('Личный кабинет', ['/personal/index'])?> | <?=Html::a('Выход', ['/site/logout'], ['class' => 'js-logout'])?></p>
                    <? } ?>
                    <p>Телефон: <?= Yii::$app->params['phone']?></p>
                    <p>Адрес: <?= Yii::$app->params['address']?></p>
                </div>
            </div>
        </div>
        <?= FrontNavBar::widget(['menuItems' => $menuItems]); ?>
    </header>

    <div class="container main-container">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-8">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <?= Alert::widget() ?>
                    <?= $content ?>
                </div>
                <div class="col-sm-4">
                    <?= RightNavBar::widget(['menuItems' => $this->params['menuItems']])?>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
