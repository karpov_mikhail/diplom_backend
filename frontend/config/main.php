<?php
$settingsPath = __DIR__ . '/settings.json';
$isExist = file_exists($settingsPath);
if($isExist){
    $settings = (array)json_decode(file_get_contents($settingsPath));
}

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php',
    ['address' => $settings['address'], 'phone' => $settings['phone']]
);

return [
    'id' => 'app-frontend',
    'name' => empty($settings['name']) ? 'Автошкола' : $settings['name'],
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                "/" => "site/index",
                "theory/card/<cardId:\d+>" => "theory/card",
                "theory/get-next-question/" => "theory/get-next-question",
                "theory/get-question/" => "theory/get-question",
                "theory/check-answer/" => "theory/check-answer"
            ],
        ],

    ],
    'params' => $params,
];
