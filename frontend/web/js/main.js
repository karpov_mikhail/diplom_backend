$(document).ready(function() {
    $('body').on('click', '.js-logout', function (e) {
        e.preventDefault();
        var url = $(this).attr('href');
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            success: function(data) {
                window.location.reload();
            }
        });
    });

    $('body').on('click', '.js-choose-car', function (e) {
        e.preventDefault();
        var url = $(this).attr('href'),
            carId = $(this).data('car-id'),
            staffId = $(this).data('staff-id'),
            successUrl = $(this).data('success');
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: {
                car_id : carId,
                staff_id : staffId
            },
            success: function(data) {
                console.log('Car choosed');
                window.location.href = successUrl;
            }
        });
    });
});