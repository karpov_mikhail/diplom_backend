function getQuestion(next, questNum) {
    var question = $('.js-question'),
        questionNumber = question.data('question-number'),
        cardId = question.data('card-id');
    if(questNum != undefined) {
        questionNumber = questNum;
    }
    if(questionNumber >= 0 && questionNumber <= 20) {
        var url = '';
        if(next == true) {
            url = '/frontend/web/theory/get-next-question/';
        } else {
            url = '/frontend/web/theory/get-question/';
        }

        $.ajax({
            type: 'GET',
            url: url,
            data: {
                questionNumber: questionNumber,
                cardId: cardId
            },
            dataType: 'html',
            success: function(data) {
                $('.question-container').html(data);
            }
        });
    }
}

function checkAnswer(answerId, questionNumber, cardId) {
    var url = '/frontend/web/theory/check-answer/',
        commentText = $('body').find('.comment-text'),
        nextQuestionButton = $('body').find('.js-next-question');
    $.ajax({
        type: 'GET',
        url: url,
        data: {
            answerId: answerId,
            questionNumber: questionNumber,
            cardId: cardId
        },
        dataType: 'json',
        success: function(data) {
            if(data.success) {
                if(data.rightAnswer == true) {
                    getQuestion(true);
                } else {
                    commentText.html(data.comment);
                    commentText.css('display', 'block');
                    nextQuestionButton.css('display', 'block');
                    $('.js-answer').each(function (i, elem) {
                        if($(this).data('answer-id') == data.rightAnswerId) {
                            $(this).addClass('alert-success disabled');
                        } else {
                            $(this).addClass('alert-danger disabled');
                        }
                    });
                }
            }
        }
    });
}

$(document).ready(function() {
    getQuestion(true);

    $('body').on('click', '.js-answer', function (e) {
        var answerId = $(this).data('answer-id'),
            questionNumber = $('.js-question').data('question-number'),
            cardId = $('.js-question').data('card-id');
        checkAnswer(answerId, questionNumber, cardId);
    })
    
    $('body').on('click', '.js-next-question', function (e) {
        getQuestion(true);
    });

    $('body').on('click', '.js-question-status', function (e) {
        var questNum = $(this).data('question-number');
        getQuestion(false, questNum);
    });
});