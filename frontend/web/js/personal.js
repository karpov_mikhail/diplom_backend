$(document).ready(function() {
    $('body').on('click', '.js-personal-code-btn', function (event) {
        event.preventDefault();
        var url = '/frontend/web/personal/check-code/',
            $form = $('#personal-code-form');
        $.ajax({
            type: 'POST',
            url: url,
            data: $form.serializeArray(),
            dataType: 'json',
            success: function(data) {
                if(data.success) {
                    $form.parent().html(data.message);
                } else {
                    $form.parent().html(data.message);
                }
            }
        });
    });
});