-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 25 2018 г., 20:14
-- Версия сервера: 10.1.19-MariaDB
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `yii2diplom`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auto_class`
--

CREATE TABLE `auto_class` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `auto_class`
--

INSERT INTO `auto_class` (`id`, `name`, `description`, `active`) VALUES
(1, 'M', 'Мопеды', 1),
(2, 'A', 'Мотоциклы', 0),
(3, 'A1', 'Легкие мотоциклы с рабочим объемом двигателя внутреннего сгорания, не превышающим 125 кубических сантиметров, и максимальной мощностью, не превышающей 11 киловатт', 0),
(4, 'B', 'Автомобили (за исключением транспортных средств категории A), разрешенная максимальная масса которых не превышает 3,5 тонн и число сидячих мест которых, помимо сиденья водителя, не превышает восьми; автомобили категории B, сцепленные с прицепом, разрешенная максимальная масса которого не превышает 750 килограммов; автомобили категории B, сцепленные с прицепом, разрешенная максимальная масса которого превышает 750 килограммов, но не превышает массы автомобиля без нагрузки, при условии, что общая разрешенная максимальная масса такого состава транспортных средств не превышает 3,5 тонн', 1),
(5, 'B1', 'Трициклы и квадрициклы (не путайте с квадроциклами). Речь идет о тех из них, которые имеют право выезжать на дороги общего пользования (объем двигателя более 50 кубических cантиметров, и скорость более 50 км/час). На управление теми, которые не могут выезжать на дороги — объемом более 50 куб. сантиметров, но конструктивной скоростью менее 50 км/час — потребуется получать тракторные права', 0),
(6, 'C', 'Автомобили, за исключением автомобилей категории D, разрешенная максимальная масса которых превышает 3500 килограммов; автомобили категории C, сцепленные с прицепом, разрешенная максимальная масса которого не превышает 750 килограммов', 0),
(7, 'C1', 'Автомобили, за исключением автомобилей категории D, разрешенная максимальная масса которых превышает 3500 килограммов, но не превышает 7500 килограммов; автомобили подкатегории C1, сцепленные с прицепом, разрешенная максимальная масса которого не превышает 750 килограммов', 0),
(8, 'D', 'Автомобили, предназначенные для перевозки пассажиров и имеющие более восьми сидячих мест, помимо сиденья водителя; автомобили категории D, сцепленные с прицепом, разрешенная максимальная масса которого не превышает 750 килограммов', 0),
(9, 'D1', 'Автомобили, предназначенные для перевозки пассажиров и имеющие более восьми, но не более шестнадцати сидячих мест, помимо сиденья водителя; автомобили подкатегории D1, сцепленные с прицепом, разрешенная максимальная масса которого не превышает 750 килограммов', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `file`
--

CREATE TABLE `file` (
  `id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mime` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `group`
--

CREATE TABLE `group` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `group`
--

INSERT INTO `group` (`id`, `name`, `code`) VALUES
(1, 'Администраторы', 'admin'),
(2, 'Модераторы', 'moderator'),
(3, 'Преподаватели', 'teacher'),
(4, 'Инструкторы', 'instructor'),
(5, 'Курсанты', 'learner'),
(6, 'Зарегистрированные пользователи', 'registered'),
(7, 'Незарегистрированные пользователи', 'unregistered');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1520056870),
('m130524_201442_init', 1520056874),
('m180212_132926_user', 1520056887),
('m180213_154441_group', 1520056889),
('m180213_161600_user_group', 1520056893),
('m180214_134018_file', 1520056894),
('m180319_154645_create_question_tables', 1521481124),
('m180320_143426_create_auto_class_table', 1521558213),
('m180320_150643_create_theory_question_auto_class_table', 1521558755);

-- --------------------------------------------------------

--
-- Структура таблицы `theory_answer`
--

CREATE TABLE `theory_answer` (
  `id` int(11) NOT NULL,
  `answer_text` text COLLATE utf8_unicode_ci NOT NULL,
  `question_id` int(10) NOT NULL,
  `right_answer` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `theory_answer_log`
--

CREATE TABLE `theory_answer_log` (
  `id` int(11) NOT NULL,
  `user_id` int(10) NOT NULL,
  `question_id` int(10) NOT NULL,
  `answer_id` int(10) NOT NULL,
  `answer_time` datetime NOT NULL,
  `correctly` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `theory_question`
--

CREATE TABLE `theory_question` (
  `id` int(11) NOT NULL,
  `card_number` int(10) DEFAULT NULL,
  `question_text` text COLLATE utf8_unicode_ci NOT NULL,
  `topic_id` int(10) NOT NULL,
  `image_id` int(10) DEFAULT NULL,
  `update_status` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Триггеры `theory_question`
--
DELIMITER $$
CREATE TRIGGER `before_insert_theory_question` BEFORE INSERT ON `theory_question` FOR EACH ROW BEGIN
            
                SET NEW.update_status = "Y";
                SET NEW.last_update = NOW();
            END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_update_theory_question` BEFORE UPDATE ON `theory_question` FOR EACH ROW BEGIN
            
                SET NEW.update_status = "Y";
                SET NEW.last_update = NOW();
            END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `theory_question_auto_class`
--

CREATE TABLE `theory_question_auto_class` (
  `question_id` int(11) NOT NULL,
  `auto_class_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `theory_topic`
--

CREATE TABLE `theory_topic` (
  `id` int(11) NOT NULL,
  `topic_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `topic_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `theory_topic`
--

INSERT INTO `theory_topic` (`id`, `topic_number`, `topic_text`) VALUES
(1, '1.', 'Общие положения');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `second_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `first_name`, `last_name`, `second_name`, `phone_number`, `avatar_id`) VALUES
(2, 'mixanrulezzz', 'd_gAgeg12IbX0fJuqb4bn_t51Bw2YyJH', '$2y$13$K21N.GAnS1pwStcY7xPjleWymr9TFxJDZZtget98DfkNoZgTdXPdu', NULL, 'mikhail4782010@yandex.ru', 10, 1520359777, 1520924110, 'Иван', 'Иванов', 'Иванович', '+7 (987) 654-3210', NULL),
(5, 'admin', '-yFgxZTAVrzVCDwZZ45H979t6CNzX0hy', '$2y$13$m5mSz4oGgWMexRteaAJYL...qK4Rsm9d6weiXM/UNEoBLclZchvb.', NULL, 'admin@example.ru', 10, 1521311473, 1521311473, 'Михаил', 'Карпов', 'Александрович', '+7 (967) 774-2701', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user_group`
--

CREATE TABLE `user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user_group`
--

INSERT INTO `user_group` (`user_id`, `group_id`) VALUES
(2, 1),
(2, 6),
(5, 1),
(5, 6);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auto_class`
--
ALTER TABLE `auto_class`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `path` (`path`);

--
-- Индексы таблицы `group`
--
ALTER TABLE `group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `theory_answer`
--
ALTER TABLE `theory_answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_theory_answer_question_id` (`question_id`);

--
-- Индексы таблицы `theory_answer_log`
--
ALTER TABLE `theory_answer_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_theory_answer_log_user_id` (`user_id`),
  ADD KEY `fk_theory_answer_log_question_id` (`question_id`),
  ADD KEY `fk_theory_answer_log_answer_id` (`answer_id`);

--
-- Индексы таблицы `theory_question`
--
ALTER TABLE `theory_question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_theory_question_topic_id` (`topic_id`),
  ADD KEY `fk_theory_question_image_id` (`image_id`);

--
-- Индексы таблицы `theory_question_auto_class`
--
ALTER TABLE `theory_question_auto_class`
  ADD PRIMARY KEY (`question_id`,`auto_class_id`),
  ADD KEY `fk_theory_question_auto_class_auto_class_id` (`auto_class_id`);

--
-- Индексы таблицы `theory_topic`
--
ALTER TABLE `theory_topic`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `topic_number` (`topic_number`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`),
  ADD KEY `fk_user_avatar_id` (`avatar_id`);

--
-- Индексы таблицы `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`user_id`,`group_id`),
  ADD KEY `fk_user_group_group_id` (`group_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `auto_class`
--
ALTER TABLE `auto_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `file`
--
ALTER TABLE `file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `group`
--
ALTER TABLE `group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `theory_answer`
--
ALTER TABLE `theory_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `theory_answer_log`
--
ALTER TABLE `theory_answer_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `theory_question`
--
ALTER TABLE `theory_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `theory_topic`
--
ALTER TABLE `theory_topic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `theory_answer`
--
ALTER TABLE `theory_answer`
  ADD CONSTRAINT `fk_theory_answer_question_id` FOREIGN KEY (`question_id`) REFERENCES `theory_question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `theory_answer_log`
--
ALTER TABLE `theory_answer_log`
  ADD CONSTRAINT `fk_theory_answer_log_answer_id` FOREIGN KEY (`answer_id`) REFERENCES `theory_answer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_theory_answer_log_question_id` FOREIGN KEY (`question_id`) REFERENCES `theory_question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_theory_answer_log_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `theory_question`
--
ALTER TABLE `theory_question`
  ADD CONSTRAINT `fk_theory_question_image_id` FOREIGN KEY (`image_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_theory_question_topic_id` FOREIGN KEY (`topic_id`) REFERENCES `theory_topic` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `theory_question_auto_class`
--
ALTER TABLE `theory_question_auto_class`
  ADD CONSTRAINT `fk_theory_question_auto_class_auto_class_id` FOREIGN KEY (`auto_class_id`) REFERENCES `auto_class` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_theory_question_auto_class_question_id` FOREIGN KEY (`question_id`) REFERENCES `theory_question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_avatar_id` FOREIGN KEY (`avatar_id`) REFERENCES `file` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `user_group`
--
ALTER TABLE `user_group`
  ADD CONSTRAINT `fk_user_group_group_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_user_group_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
