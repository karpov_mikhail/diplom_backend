<?php

use yii\db\Migration;

/**
 * Class m180605_060914_add_teacher_salary
 */
class m180605_060914_add_teacher_salary extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('staff', 'salary_per_hour', 'integer default 0');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('staff', 'salary_per_hour');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180605_060914_add_teacher_salary cannot be reverted.\n";

        return false;
    }
    */
}
