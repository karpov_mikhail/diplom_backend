<?php

use yii\db\Migration;

/**
 * Class m180214_134018_file
 */
class m180214_134018_file extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('file', [
            'id' => $this->primaryKey(),
            'path' => $this->string(255)->notNull()->unique(),
            'name' => $this->string(255)->notNull(),
            'extension' => $this->string(10)->notNull(),
            'size' => $this->string(255)->notNull(),
            'mime' => $this->string(255)->notNull(),
        ], $tableOptions);

        $this->addColumn('{{%user}}', 'avatar_id', 'integer(11)');
        $this->addForeignKey('fk_user_avatar_id', '{{%user}}', 'avatar_id', 'file', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_user_avatar_id', '{{%user}}');
        $this->dropColumn('{{%user}}', 'avatar_id');
        $this->dropTable('file');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180214_134018_file cannot be reverted.\n";

        return false;
    }
    */
}
