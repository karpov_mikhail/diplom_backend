<?php

use yii\db\Migration;

/**
 * Class m180425_084224_add_column_question_number
 */
class m180425_084224_add_column_question_number extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('theory_question', 'question_number', $this->integer()->notNull()->after('id'));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('theory_question', 'question_number');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180425_084224_add_column_question_number cannot be reverted.\n";

        return false;
    }
    */
}
