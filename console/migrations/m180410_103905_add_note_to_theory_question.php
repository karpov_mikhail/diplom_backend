<?php

use yii\db\Migration;

/**
 * Class m180410_103905_add_note_to_theory_question
 */
class m180410_103905_add_note_to_theory_question extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('theory_question', 'note', 'text not null');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('theory_question', 'note');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180410_103905_add_note_to_theory_question cannot be reverted.\n";

        return false;
    }
    */
}
