<?php

use yii\db\Migration;

/**
 * Class m180319_154645_create_question_tables
 */
class m180319_154645_create_question_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('theory_topic', [
            'id' => $this->primaryKey(),
            'topic_number' => $this->string(255)->notNull()->unique(),
            'topic_text' => $this->string(255)->notNull()
        ], $tableOptions);

        $this->createTable('theory_question', [
            'id' => $this->primaryKey(),
            'card_number' => $this->integer(10),
            'question_text' => $this->text()->notNull(),
            'topic_id' => $this->integer(10)->notNull(),
            'image_id' => $this->integer(10),
            'update_status' => $this->string(1),
            'last_update' => $this->dateTime()
        ], $tableOptions);

        $this->addForeignKey(
            'fk_theory_question_topic_id',
            'theory_question',
            'topic_id',
            'theory_topic',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_theory_question_image_id',
            'theory_question',
            'image_id',
            'file',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // Триггер, срабатывающий при добавлении билета
        $sql = <<< SQL
            CREATE
                TRIGGER `before_insert_theory_question` BEFORE INSERT ON `theory_question`
                FOR EACH ROW BEGIN
            
                SET NEW.update_status = "Y";
                SET NEW.last_update = NOW();
            END;
SQL;
        $this->execute($sql);

        // Триггер, срабатывающий при обновлении билета
        $sql = <<< SQL
            CREATE 
                TRIGGER `before_update_theory_question` BEFORE UPDATE ON `theory_question`
                FOR EACH ROW BEGIN
            
                SET NEW.update_status = "Y";
                SET NEW.last_update = NOW();
            END;
SQL;
        $this->execute($sql);

        $this->createTable('theory_answer', [
            'id' => $this->primaryKey(),
            'answer_text' => $this->text()->notNull(),
            'question_id' => $this->integer(10)->notNull(),
            'right_answer' => $this->boolean()
        ], $tableOptions);

        $this->addForeignKey(
            'fk_theory_answer_question_id',
            'theory_answer',
            'question_id',
            'theory_question',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable('theory_answer_log', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(10)->notNull(),
            'question_id' => $this->integer(10)->notNull(),
            'answer_id' => $this->integer(10)->notNull(),
            'answer_time' => $this->dateTime()->notNull(),
            'correctly' => $this->boolean()->notNull()
        ], $tableOptions);

        $this->addForeignKey(
            'fk_theory_answer_log_user_id',
            'theory_answer_log',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_theory_answer_log_question_id',
            'theory_answer_log',
            'question_id',
            'theory_question',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_theory_answer_log_answer_id',
            'theory_answer_log',
            'answer_id',
            'theory_answer',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('theory_answer_log');
        $this->dropTable('theory_answer');
        $sql = 'DROP TRIGGER IF EXISTS before_insert_theory_question;';
        $this->execute($sql);
        $sql = 'DROP TRIGGER IF EXISTS before_update_theory_question;';
        $this->execute($sql);
        $this->dropTable('theory_question');
        $this->dropTable('theory_topic');
//        echo "m180319_154645_create_question_tables cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180319_154645_create_question_tables cannot be reverted.\n";

        return false;
    }
    */
}
