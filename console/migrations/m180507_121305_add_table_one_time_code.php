<?php

use yii\db\Migration;

/**
 * Class m180507_121305_add_table_one_time_code
 */
class m180507_121305_add_table_one_time_code extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('group_one_time_code', [
            'id' => $this->primaryKey(),
            'unique_code' => $this->string(10)->unique()->notNull(),
            'create_date' => $this->dateTime()->notNull(),
            'activated' => $this->boolean()->notNull(),
            'staff_id' => $this->integer()->notNull()->unique()
        ]);
        $this->addForeignKey(
            'fk_group_one_time_code_staff_id',
            'group_one_time_code',
            'staff_id',
            'staff',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('group_one_time_code');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180507_121305_add_table_one_time_code cannot be reverted.\n";

        return false;
    }
    */
}
