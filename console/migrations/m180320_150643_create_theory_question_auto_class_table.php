<?php

use yii\db\Migration;

/**
 * Handles the creation of table `theory_question_auto_class`.
 */
class m180320_150643_create_theory_question_auto_class_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('theory_question_auto_class', [
            'question_id' => $this->integer()->notNull(),
            'auto_class_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('pk_theory_question_auto_class', 'theory_question_auto_class', ['question_id', 'auto_class_id']);
        $this->addForeignKey(
            'fk_theory_question_auto_class_question_id',
            'theory_question_auto_class',
            'question_id',
            'theory_question',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_theory_question_auto_class_auto_class_id',
            'theory_question_auto_class',
            'auto_class_id',
            'auto_class',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('theory_question_auto_class');
    }
}
