<?php

use yii\db\Migration;

/**
 * Class m180511_113110_add_table_learning_group
 */
class m180511_113110_add_table_learning_group extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('learning_group', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'teacher_id' => $this->integer(),
            'auto_class_id' => $this->integer()->notNull()
        ]);
        $this->addForeignKey(
            'fk_learning_group_teacher_id',
            'learning_group',
            'teacher_id',
            'staff',
            'id',
            'SET NULL',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_learning_group_auto_class_id',
            'learning_group',
            'auto_class_id',
            'auto_class',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable('user_learning_group', [
            'learning_group_id' => $this->integer()->notNull(),
            'staff_id' => $this->integer()->notNull()
        ]);

        $this->addPrimaryKey('pk_user_learning_group', 'user_learning_group', ['learning_group_id', 'staff_id']);
        $this->addForeignKey(
            'fk_user_learning_group_learning_group_id',
            'user_learning_group',
            'learning_group_id',
            'learning_group',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_user_learning_group_staff_id',
            'user_learning_group',
            'staff_id',
            'staff',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user_learning_group');
        $this->dropTable('learning_group');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180511_113110_add_table_learning_group cannot be reverted.\n";

        return false;
    }
    */
}
