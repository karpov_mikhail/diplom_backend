<?php

use yii\db\Migration;

/**
 * Class m180507_113526_add_staff_table
 */
class m180507_113526_add_staff_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('staff', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(255)->notNull(),
            'last_name' => $this->string(255)->notNull(),
            'second_name' => $this->string(255),
            'group_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()
        ]);
        $this->addForeignKey(
            'fk_staff_group_id',
            'staff',
            'group_id',
            'group',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_staff_user_id',
            'staff',
            'user_id',
            'user',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('staff');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180507_113526_add_staff_table cannot be reverted.\n";

        return false;
    }
    */
}
