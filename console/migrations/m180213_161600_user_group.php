<?php

use yii\db\Migration;

/**
 * Class m180213_161600_user_group
 */
class m180213_161600_user_group extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('user_group', [
            'user_id' => $this->integer(11)->notNull(),
            'group_id' => $this->integer(11)->notNull()
        ], $tableOptions);
        $this->addPrimaryKey('pk_user_group', 'user_group', ['user_id', 'group_id']);
        $this->addForeignKey('fk_user_group_user_id', 'user_group', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_user_group_group_id', 'user_group', 'group_id', 'group', 'id', 'CASCADE', 'CASCADE');

        $this->insert('user_group', ['user_id' => 1, 'group_id' => 1]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user_group');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180213_161600_user_group cannot be reverted.\n";

        return false;
    }
    */
}
