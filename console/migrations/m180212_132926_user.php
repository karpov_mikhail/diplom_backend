<?php

use yii\db\Migration;

/**
 * Class m180214_132926_user
 */
class m180212_132926_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'first_name', 'string(255)');
        $this->addColumn('{{%user}}', 'last_name', 'string(255)');
        $this->addColumn('{{%user}}', 'second_name', 'string(255)');
        $this->addColumn('{{%user}}', 'phone_number', 'string(20)');

        $this->insert('{{%user}}', [
            'id' => 1,
            'username' => 'admin',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'email' => 'admin@example.ru',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('{{%user}}', ["id" => 1]);
        $this->dropColumn('{{%user}}', 'first_name');
        $this->dropColumn('{{%user}}', 'last_name');
        $this->dropColumn('{{%user}}', 'second_name');
        $this->dropColumn('{{%user}}', 'phone_number');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180214_132926_user cannot be reverted.\n";

        return false;
    }
    */
}
