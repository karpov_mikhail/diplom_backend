<?php

use yii\db\Migration;

/**
 * Class m180213_154441_group
 */
class m180213_154441_group extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('group', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'code' => $this->string(255)->notNull()->unique()
        ], $tableOptions);

        $insertData = [
            ['id' => 1, 'name' => 'Администраторы', 'code' => 'admin'],
            ['id' => 2, 'name' => 'Модераторы', 'code' => 'moderator'],
            ['id' => 3, 'name' => 'Преподаватели', 'code' => 'teacher'],
            ['id' => 4, 'name' => 'Инструкторы', 'code' => 'instructor'],
            ['id' => 5, 'name' => 'Курсанты', 'code' => 'learner'],
            ['id' => 6, 'name' => 'Зарегистрированные пользователи', 'code' => 'registered'],
            ['id' => 7, 'name' => 'Незарегистрированные пользователи', 'code' => 'unregistered']
        ];

        foreach ($insertData as $data) {
            $this->insert('group', $data);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('group');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180213_154441_group cannot be reverted.\n";

        return false;
    }
    */
}
