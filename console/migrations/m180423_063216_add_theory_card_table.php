<?php

use yii\db\Migration;

/**
 * Class m180423_063216_add_theory_card_table
 */
class m180423_063216_add_theory_card_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        // Удаление старых таблиц и столбцов
        $this->dropTable('theory_question_auto_class');
        $this->dropColumn('theory_question', 'card_number');

        // Создание таблицы с билетами
        $this->createTable('theory_card', [
            'id' => $this->primaryKey(),
            'card_number' => $this->integer()->notNull()
        ]);

        // Создание таблицы связи билетов с категориями авто
        $this->createTable('theory_card_auto_class', [
            'auto_class_id' => $this->integer()->notNull(),
            'card_id' => $this->integer()->notNull()
        ]);
        $this->addPrimaryKey('pk_theory_card_auto_class', 'theory_card_auto_class', ['card_id', 'auto_class_id']);
        $this->addForeignKey(
            'fk_theory_card_auto_class_card_id',
            'theory_card_auto_class',
            'card_id',
            'theory_card',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_theory_card_auto_class_auto_class_id',
            'theory_card_auto_class',
            'auto_class_id',
            'auto_class',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // Добавление столбца с привязкой к id билета
        $this->addColumn('theory_question', 'card_id', $this->integer()->notNull()->after('question_text'));
        $this->addForeignKey(
            'fk_theory_question_card_id',
            'theory_question',
            'card_id',
            'theory_card',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // Создание таблицы с логами ответов на билет
        $this->createTable('theory_card_log', [
            'id' => $this->primaryKey(),
            'card_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'create_time' => $this->dateTime()->notNull(),
            'passed' => $this->boolean()->notNull(),
            'right_answer_count' => $this->smallInteger()->notNull()
        ]);
        $this->addForeignKey(
            'fk_theory_card_log_card_id',
            'theory_card_log',
            'card_id',
            'theory_card',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_theory_card_log_user_id',
            'theory_card_log',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->createTable('theory_question_auto_class', [
            'question_id' => $this->integer()->notNull(),
            'auto_class_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('pk_theory_question_auto_class', 'theory_question_auto_class', ['question_id', 'auto_class_id']);
        $this->addForeignKey(
            'fk_theory_question_auto_class_question_id',
            'theory_question_auto_class',
            'question_id',
            'theory_question',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_theory_question_auto_class_auto_class_id',
            'theory_question_auto_class',
            'auto_class_id',
            'auto_class',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->dropForeignKey('fk_theory_question_card_id', 'theory_question');
        $this->dropColumn('theory_question', 'card_id');

        $this->addColumn('theory_question', 'card_number', $this->integer()->after('question_text'));

        $this->dropTable('theory_card_auto_class');

        $this->dropTable('theory_card_log');

        $this->dropTable('theory_card');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180423_063216_add_theory_card_table cannot be reverted.\n";

        return false;
    }
    */
}
