<?php

use yii\db\Migration;

/**
 * Class m180522_084515_add_practice_tables
 */
class m180522_084515_add_practice_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('practice_car', [
            'id' => $this->primaryKey(),
            'mark' => $this->string(255)->notNull(),
            'model' => $this->string(255)->notNull(),
            'car_power' => $this->float()->notNull(),
            'max_learners' => $this->integer()->defaultValue(10),
            'teacher_id' => $this->integer()->unique(),
            'auto_class_id' => $this->integer()->notNull(),
            'car_photo_id' => $this->integer()
        ]);
        $this->addForeignKey(
            'fk_practice_car_teacher_id',
            'practice_car',
            'teacher_id',
            'staff',
            'id',
            'SET NULL',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_practice_car_auto_class_id',
            'practice_car',
            'auto_class_id',
            'auto_class',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_practice_car_car_photo_id',
            'practice_car',
            'car_photo_id',
            'file',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable('practice_schedule', [
            'id' => $this->primaryKey(),
            'datetime' => $this->dateTime()->notNull(),
            'learner_id' => $this->integer()->notNull(),
            'practice_car_id' => $this->integer()
        ]);
        $this->addForeignKey(
            'fk_practice_schedule_learner_id',
            'practice_schedule',
            'learner_id',
            'staff',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_practice_schedule_practice_car_id',
            'practice_schedule',
            'practice_car_id',
            'practice_car',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable('user_practice_car', [
            'learner_id' => $this->integer(),
            'practice_car_id' => $this->integer()
        ]);
        $this->addPrimaryKey('pk_user_practice_car', 'user_practice_car', ['learner_id', 'practice_car_id']);
        $this->addForeignKey(
            'fk_user_practice_car_learner_id',
            'user_practice_car',
            'learner_id',
            'staff',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_user_practice_car_practice_car_id',
            'user_practice_car',
            'practice_car_id',
            'practice_car',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user_practice_car');
        $this->dropTable('practice_schedule');
        $this->dropTable('practice_car');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180522_084515_add_practice_tables cannot be reverted.\n";

        return false;
    }
    */
}
