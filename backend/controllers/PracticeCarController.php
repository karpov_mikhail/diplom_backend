<?php

namespace backend\controllers;

use backend\models\PracticeCarForm;
use common\models\PracticeCar;
use yii\data\Pagination;

class PracticeCarController extends MainController
{
    public function actionIndex()
    {
        $query = PracticeCar::find()->orderBy("id");
        $pages = new Pagination(["pageSize" => 10, "totalCount" => $query->count(), "pageSizeParam" => false, "forcePageParam" => false]);
        $group = $query->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        $columns = [
            'id' => 'ID',
            'mark' => 'Марка авто',
            'model' => 'Модель авто',
            'max_learners' => 'Макс. кол-во курсантов',
        ];

        return $this->render('index', ['data' => $group, 'columns' => $columns, 'pages' => $pages]);
    }

    public function actionAdd()
    {
        $model = new PracticeCarForm();
        $model->scenario = PracticeCarForm::ADD_SCENARIO;
        $model->loadModel();
        if(\Yii::$app->request->isPost) {
            $result = $model->saveModel(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['practice-car/index']);
            }
        }
        $this->view->title = "Добавление автомобилей";
        return $this->render('add', [ 'model' => $model ]);
    }

    public function actionDetail($id = null)
    {
        if($id == null) {
            return $this->render("error");
        }

        $model = new PracticeCarForm();
        $model->scenario = PracticeCarForm::UPDATE_SCENARIO;
        $model->loadModel($id);

        if(\Yii::$app->request->isPost) {
            $result = $model->saveModel(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['practice-car/index']);
            }
        }
        $this->view->title = "Обновление автомобилей";

        return $this->render('detail', [ 'model' => $model ]);
    }

    public function actionDelete($id = null)
    {
        try {
            if(empty($id)) {
                throw new \Exception("Не хватает идентификатора автомобиля");
            }
            $car = PracticeCar::findOne($id);
            if($car == null) {
                throw new \Exception("Автомобиля с таким ID не существует");
            }
            $result = $car->delete();
            if($result === false) {
                throw new \Exception("Не удалось удалить автомобиль");
            }
            return json_encode(['success' => true, 'messages' => ['Удаление прошло успешно']]);
        } catch (\Exception $e) {
            return json_encode(['success' => false, 'messages' => [$e->getMessage()]]);
        }
    }
}