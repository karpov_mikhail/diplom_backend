<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 15.02.2018
 * Time: 19:03
 */

namespace backend\controllers;


use backend\models\UserInfForm;
use common\models\User;
use yii\data\Pagination;

class UserController extends MainController
{
    public function actionIndex()
    {
        $query = User::find()->orderBy("id");
        $pages = new Pagination(["pageSize" => 10, "totalCount" => $query->count(), "pageSizeParam" => false, "forcePageParam" => false]);
        $user = $query->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        $columns = ['id' => 'ID', 'username' => 'Имя пользователя', 'email' => 'E-mail'];

        return $this->render('index', ['data' => $user, 'columns' => $columns, 'pages' => $pages]);
    }

    public function actionAdd()
    {
        $model = new UserInfForm();
        $model->loadModel();
        if(\Yii::$app->request->isPost) {
            $result = $model->saveUserInfo(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['user/index']);
            }
        }
        return $this->render('add', [ 'model' => $model ]);
    }

    public function actionDetail($id = null)
    {
        if($id == null) {
            return $this->render("error");
        }

        $model = new UserInfForm();
        $model->loadModel($id);

        if(\Yii::$app->request->isPost) {
            $result = $model->saveUserInfo(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['user/index']);
            }
        }
//        $user = User::findOne($id);
//
//        if(empty($user)) {
//            return $this->render("error");
//        }
        $this->view->title = "Пользователь";

        return $this->render('detail', [ 'model' => $model ]);
    }

    public function actionDelete($id = null)
    {
        try {
            if(empty($id)) {
                throw new \Exception("Не хватает идентификатора пользователя");
            }
            $user = User::findOne($id);
            if($user == null) {
                throw new \Exception("Пользователя с таким ID не существует");
            }
            $result = $user->delete();
            if($result === false) {
                throw new \Exception("Не удалось удалить пользователя");
            }
            return json_encode(['success' => true, 'messages' => ['Удаление прошло успешно']]);
        } catch (\Exception $e) {
            return json_encode(['success' => false, 'messages' => [$e->getMessage()]]);
        }
    }
}