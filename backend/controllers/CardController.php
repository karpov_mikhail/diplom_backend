<?php

namespace backend\controllers;

use backend\models\CardForm;
use common\models\AutoClass;
use common\models\TheoryCard;
use yii\data\Pagination;

class CardController extends TheoryController
{
    public function actionIndex()
    {
        $query = TheoryCard::find()->orderBy("id");
        $pages = new Pagination(["pageSize" => 10, "totalCount" => $query->count(), "pageSizeParam" => false, "forcePageParam" => false]);
        $cards = $query->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        $cardAutoClassString = TheoryCard::getCardsAutoClassString(array_column($cards, 'id'));
        foreach ($cards as $key=>$card) {
            $cards[$key]['auto_class'] = $cardAutoClassString[$card['id']];
        }
        $columns = ['id' => 'ID', 'card_number' => 'Номер билета', 'auto_class' => 'Категории авто'];

        return $this->render('index', ['data' => $cards, 'columns' => $columns, 'pages' => $pages]);
    }

    public function actionAdd()
    {
        $model = new CardForm();
        $model->scenario = CardForm::ADD_SCENARIO;
        $model->loadModel();
        if(\Yii::$app->request->isPost) {
            $result = $model->saveTopic(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['card/index']);
            }
        }
        $this->view->title = "Добавление темы билета";
        return $this->render('add', [ 'model' => $model ]);
    }

    public function actionDetail($id = null)
    {
        if($id == null) {
            return $this->render("error");
        }

        $model = new CardForm();
        $model->scenario = CardForm::UPDATE_SCENARIO;
        $model->loadModel($id);

        if(\Yii::$app->request->isPost) {
            $result = $model->saveTopic(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['card/index']);
            }
        }
        $this->view->title = "Обновление темы билета";

        return $this->render('detail', [ 'model' => $model ]);
    }

    public function actionDelete($id = null)
    {
        try {
            if(empty($id)) {
                throw new \Exception("Не хватает идентификатора билета");
            }
            $card = TheoryCard::findOne($id);
            if($card == null) {
                throw new \Exception("Билета с таким ID не существует");
            }
            $result = $card->delete();
            if($result === false) {
                throw new \Exception("Не удалось удалить билет");
            }
            return json_encode(['success' => true, 'messages' => ['Удаление прошло успешно']]);
        } catch (\Exception $e) {
            return json_encode(['success' => false, 'messages' => [$e->getMessage()]]);
        }
    }
}