<?php

namespace backend\controllers;

use backend\models\TheoryTopicForm;
use common\models\TheoryTopic;
use yii\data\Pagination;

class TopicController extends TheoryController
{
    public function actionIndex()
    {
        $query = TheoryTopic::find()->orderBy("id");
        $pages = new Pagination(["pageSize" => 10, "totalCount" => $query->count(), "pageSizeParam" => false, "forcePageParam" => false]);
        $topic = $query->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        $columns = ['id' => 'ID', 'topic_number' => 'Номер темы', 'topic_text' => 'Название темы'];

        return $this->render('index', ['data' => $topic, 'columns' => $columns, 'pages' => $pages]);
    }

    public function actionAdd()
    {
        $model = new TheoryTopicForm();
        $model->loadModel();
        if(\Yii::$app->request->isPost) {
            $result = $model->saveTopic(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['topic/index']);
            }
        }
        $this->view->title = "Добавление темы билета";
        return $this->render('add', [ 'model' => $model, 'inputs' => $this->getFormInputs(true)]);
    }

    public function actionDetail($id = null)
    {
        if($id == null) {
            return $this->render("error");
        }

        $model = new TheoryTopicForm();
        $model->loadModel($id);

        if(\Yii::$app->request->isPost) {
            $result = $model->saveTopic(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['topic/index']);
            }
        }
        $this->view->title = "Обновление темы билета";

        return $this->render('detail', [ 'model' => $model, 'inputs' => $this->getFormInputs(false) ]);
    }

    public function actionDelete($id = null)
    {
        try {
            if(empty($id)) {
                throw new \Exception("Не хватает идентификатора билета");
            }
            $topic = TheoryTopic::findOne($id);
            if($topic == null) {
                throw new \Exception("Билета с таким ID не существует");
            }
            $result = $topic->delete();
            if($result === false) {
                throw new \Exception("Не удалось удалить билет");
            }
            return json_encode(['success' => true, 'messages' => ['Удаление прошло успешно']]);
        } catch (\Exception $e) {
            return json_encode(['success' => false, 'messages' => [$e->getMessage()]]);
        }
    }

    private function getFormInputs($isAdd = false)
    {
        $inputs = [
            ['type' => 'text', 'attr' => 'number', 'label' => 'Номер темы'],
            ['type' => 'text', 'attr' => 'text', 'label' => 'Название темы'],
            ['type' => 'submit', 'label' => 'Сохранить', 'class' => 'btn-success'],
            ['type' => 'reset', 'label' => 'Отмена', 'class' => 'btn-danger']
        ];
        if($isAdd) {
            $inputs[] = ['type' => 'hidden', 'attr' => 'topicId', 'label' => 'ID темы'];
        }
        return $inputs;
    }
}