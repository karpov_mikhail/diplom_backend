<?php

namespace backend\controllers;

use backend\models\AutoClassForm;
use common\models\AutoClass;
use yii\data\Pagination;

class AutoClassController extends PracticeController
{
    public function actionIndex()
    {
        $query = AutoClass::find()->orderBy("id");
        $pages = new Pagination(["pageSize" => 10, "totalCount" => $query->count(), "pageSizeParam" => false, "forcePageParam" => false]);
        $topic = $query->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        $columns = ['id' => 'ID', 'name' => 'Категория', 'description' => 'Описание'];

        return $this->render('index', ['data' => $topic, 'columns' => $columns, 'pages' => $pages]);
    }

    public function actionAdd()
    {
        $model = new AutoClassForm();
        $model->loadModel();
        if(\Yii::$app->request->isPost) {
            $result = $model->saveAutoClass(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['auto-class/index']);
            }
        }
        $this->view->title = "Добавление категории авто";
        return $this->render('add', [ 'model' => $model, 'inputs' => $this->getFormInputs(true)]);
    }

    public function actionDetail($id = null)
    {
        if($id == null) {
            return $this->render("error");
        }

        $model = new AutoClassForm();
        $model->loadModel($id);

        if(\Yii::$app->request->isPost) {
            $result = $model->saveAutoClass(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['auto-class/index']);
            }
        }
        $this->view->title = "Обновление темы билета";

        return $this->render('detail', [ 'model' => $model, 'inputs' => $this->getFormInputs(false) ]);
    }

    public function actionDelete($id = null)
    {
        try {
            if(empty($id)) {
                throw new \Exception("Не хватает идентификатора категории");
            }
            $class = AutoClass::findOne($id);
            if($class == null) {
                throw new \Exception("Категории с таким ID не существует");
            }
            $result = $class->delete();
            if($result === false) {
                throw new \Exception("Не удалось удалить категорию");
            }
            return json_encode(['success' => true, 'messages' => ['Удаление прошло успешно']]);
        } catch (\Exception $e) {
            return json_encode(['success' => false, 'messages' => [$e->getMessage()]]);
        }
    }

    private function getFormInputs($isAdd = false)
    {
        $inputs = [
            ['type' => 'text', 'attr' => 'name', 'label' => 'Название категории'],
            ['type' => 'textarea', 'attr' => 'description', 'label' => 'Описание категории'],
            ['type' => 'checkbox', 'attr' => 'active', 'label' => 'Активность'],
            ['type' => 'submit', 'label' => 'Сохранить', 'class' => 'btn-success'],
            ['type' => 'reset', 'label' => 'Отмена', 'class' => 'btn-danger']
        ];
        if(!$isAdd) {
            $inputs[] = ['type' => 'hidden', 'attr' => 'autoClassId', 'label' => 'ID категории'];
        }
        return $inputs;
    }
}