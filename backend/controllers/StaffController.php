<?php

namespace backend\controllers;

use backend\models\StaffForm;
use common\models\Staff;
use yii\data\Pagination;

class StaffController extends MainController
{
    public function actionIndex()
    {
        $query = Staff::find()->orderBy("id");
        $pages = new Pagination(["pageSize" => 10, "totalCount" => $query->count(), "pageSizeParam" => false, "forcePageParam" => false]);
        $staff = $query->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        $staffJob = Staff::getStaffGroup(array_column($staff, 'id'));
        foreach ($staff as $key=>$item) {
            $staff[$key]['job'] = $staffJob[$item['id']]['job'];
            $staff[$key]['code'] = $staffJob[$item['id']]['code'];
        }
        $columns = [
            'id' => 'ID',
            'last_name' => 'Фамилия',
            'first_name' => 'Имя',
            'job' => 'Должность',
            'code' => 'Уникальный код'
        ];

        return $this->render('index', ['data' => $staff, 'columns' => $columns, 'pages' => $pages]);
    }

    public function actionAdd()
    {
        $model = new StaffForm();
        $model->scenario = StaffForm::ADD_SCENARIO;
        $model->loadModel();
        if(\Yii::$app->request->isPost) {
            $result = $model->saveModel(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['staff/index']);
            }
        }
        $this->view->title = "Добавление персонала";
        return $this->render('add', [ 'model' => $model ]);
    }

    public function actionDetail($id = null)
    {
        if($id == null) {
            return $this->render("error");
        }

        $model = new StaffForm();
        $model->scenario = StaffForm::UPDATE_SCENARIO;
        $model->loadModel($id);

        if(\Yii::$app->request->isPost) {
            $result = $model->saveModel(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['staff/index']);
            }
        }
        $this->view->title = "Обновление персонала";

        return $this->render('detail', [ 'model' => $model ]);
    }

    public function actionDelete($id = null)
    {
        try {
            if(empty($id)) {
                throw new \Exception("Не хватает идентификатора персонала");
            }
            $staff = Staff::findOne($id);
            if($staff == null) {
                throw new \Exception("Билета с таким ID не существует");
            }
            $result = $staff->delete();
            if($result === false) {
                throw new \Exception("Не удалось удалить билет");
            }
            return json_encode(['success' => true, 'messages' => ['Удаление прошло успешно']]);
        } catch (\Exception $e) {
            return json_encode(['success' => false, 'messages' => [$e->getMessage()]]);
        }
    }
}