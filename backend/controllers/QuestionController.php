<?php

namespace backend\controllers;


use backend\models\AnswerForm;
use backend\models\QuestionForm;
use common\models\TheoryQuestion;
use common\models\TheoryTopic;
use yii\data\Pagination;
use yii\web\UploadedFile;

class QuestionController extends TheoryController
{
    public function actionIndex()
    {
        $query = TheoryQuestion::find()->orderBy("id");
        $pages = new Pagination(["pageSize" => 10, "totalCount" => $query->count(), "pageSizeParam" => false, "forcePageParam" => false]);
        $question = $query->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        $quesIds = array_column($question, 'id');
        $questionCardNumbers = TheoryQuestion::getCardNumbers($quesIds);
        foreach ($question as $key=>$item) {
            $question[$key]['card_number'] = $questionCardNumbers[$item['id']];
        }
        $columns = [
            'id' => 'ID',
            'card_number' => 'Номер билета',
            'question_number' => 'Номер вопроса',
            'question_text' => 'Текст вопроса',
            'last_update' => 'Последнее обновление'
        ];

        return $this->render('index', ['data' => $question, 'columns' => $columns, 'pages' => $pages]);
    }

    public function actionAdd()
    {
        $model = new QuestionForm();
        $model->scenario = QuestionForm::ADD_SCENARIO;
        $model->loadModel();
        if(\Yii::$app->request->isPost) {
            $result = $model->saveModel(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['question/add-answer', 'questionId' => $model->questionId]);
            }
        }
        $this->view->title = "Добавление вопроса";
        return $this->render('add', [ 'model' => $model]);
    }

    public function actionAddAnswer($questionId)
    {
        /** @var AnswerForm[] $answerModels */
        $model = new AnswerForm();
        $model->scenario = AnswerForm::ADD_SCENARIO;
        $model->questionId = $questionId;
        $model->loadModel($questionId);
        if(\Yii::$app->request->isPost) {
            $result = $model->saveModel(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['question/index']);
            }
        }
        $this->view->title = "Добавление ответов для вопроса";
        return $this->render('add-answer', ['model' => $model]);
    }

    public function actionDetail($id)
    {
        $model = new QuestionForm();
        $model->scenario = QuestionForm::UPDATE_SCENARIO;
        $model->loadModel($id);
        if(\Yii::$app->request->isPost) {
            $result = $model->saveModel(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['question/detail-answer', 'questionId' => $model->questionId]);
            }
        }
        $this->view->title = "Добавление вопроса";
        return $this->render('detail', [ 'model' => $model]);
    }

    public function actionDetailAnswer($questionId)
    {
        /** @var AnswerForm[] $answerModels */
        $model = new AnswerForm();
        $model->scenario = AnswerForm::UPDATE_SCENARIO;
        $model->questionId = $questionId;
        $model->loadModel($questionId);
        if(\Yii::$app->request->isPost) {
            $result = $model->saveModel(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['question/index']);
            }
        }
        $this->view->title = "Изменение ответов для вопроса";
        return $this->render('detail-answer', ['model' => $model]);
    }

    public function actionDelete($id)
    {
        try {
            if(empty($id)) {
                throw new \Exception("Не хватает идентификатора вопроса");
            }
            $class = TheoryQuestion::findOne($id);
            if($class == null) {
                throw new \Exception("Вопроса с таким ID не существует");
            }
            $result = $class->delete();
            if($result === false) {
                throw new \Exception("Не удалось удалить вопрос");
            }
            return json_encode(['success' => true, 'messages' => ['Удаление прошло успешно']]);
        } catch (\Exception $e) {
            return json_encode(['success' => false, 'messages' => [$e->getMessage()]]);
        }
    }
}