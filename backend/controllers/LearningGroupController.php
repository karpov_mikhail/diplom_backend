<?php

namespace backend\controllers;

use backend\models\LearningGroupForm;
use common\models\LearningGroup;
use yii\data\Pagination;

class LearningGroupController extends MainController
{
    public function actionIndex()
    {
        $query = LearningGroup::find()->orderBy("id");
        $pages = new Pagination(["pageSize" => 10, "totalCount" => $query->count(), "pageSizeParam" => false, "forcePageParam" => false]);
        $group = $query->offset($pages->offset)->limit($pages->limit)->asArray()->all();
        $columns = [
            'id' => 'ID',
            'name' => 'Название группы'
        ];

        $this->view->title = "Группы курсантов";
        return $this->render('index', ['data' => $group, 'columns' => $columns, 'pages' => $pages]);
    }

    public function actionAdd()
    {
        $model = new LearningGroupForm();
        $model->scenario = LearningGroupForm::ADD_SCENARIO;
        $model->loadModel();
        if(\Yii::$app->request->isPost) {
            $result = $model->saveModel(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['learning-group/index']);
            }
        }
        $this->view->title = "Добавление групп курсантов";
        return $this->render('add', [ 'model' => $model ]);
    }

    public function actionDetail($id = null)
    {
        if($id == null) {
            return $this->render("error");
        }

        $model = new LearningGroupForm();
        $model->scenario = LearningGroupForm::UPDATE_SCENARIO;
        $model->loadModel($id);

        if(\Yii::$app->request->isPost) {
            $result = $model->saveModel(\Yii::$app->request->post());
            if($result) {
                $this->redirect(['learning-group/index']);
            }
        }
        $this->view->title = "Обновление групп курсантов";

        return $this->render('detail', [ 'model' => $model ]);
    }

    public function actionDelete($id = null)
    {
        try {
            if(empty($id)) {
                throw new \Exception("Не хватает идентификатора группы обучающихся");
            }
            $group = LearningGroup::findOne($id);
            if($group == null) {
                throw new \Exception("Билета с таким ID не существует");
            }
            $result = $group->delete();
            if($result === false) {
                throw new \Exception("Не удалось удалить группу обучающихся");
            }
            return json_encode(['success' => true, 'messages' => ['Удаление прошло успешно']]);
        } catch (\Exception $e) {
            return json_encode(['success' => false, 'messages' => [$e->getMessage()]]);
        }
    }
}