function checkStaffGroup()
{
    var staffGroupValue = $('.js-staff-group').val();
    if(staffGroupValue == 3 || staffGroupValue == 5) {
        $('.js-learning-group').parent().css('display', 'block');
    }
    else {
        $('.js-learning-group').parent().css('display', 'none');
    }

    if(staffGroupValue == 4) {
        $('.js-staff-salary').parent().css('display', 'block');
    } else {
        $('.js-staff-salary').parent().css('display', 'none');
    }
}

$(document).ready(function() {
    checkStaffGroup();

    $('body').on('change', '.js-staff-group', function (e) {
        checkStaffGroup();
    });
});