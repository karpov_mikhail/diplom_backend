<?php

namespace backend\models;

use common\models\TheoryTopic;
use yii\base\Model;

/**
 * Class TheoryTopicForm
 * @package backend\models
 *
 * @property integer $topicId
 * @property string $number
 * @property string $text
 * @property TheoryTopic $topic
 */
class TheoryTopicForm extends Model
{
    public $topicId;
    public $number;
    public $text;
    private $topic;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['topicId', 'checkTopicId'],

            ['number', 'trim'],
            ['number', 'required'],
            ['number', 'uniqueNumber'],
            ['number', 'string', 'max' => 255],

            ['text', 'trim'],
            ['text', 'string', 'max' => 255]
        ];
    }

    // Функции для проверки полей

    /**
     * Проверка на существование темы
     * @param $attribute
     */
    public function checkTopicId($attribute)
    {
        if(!empty($this->topicId)) {
            $this->topic = TheoryTopic::findOne($this->topicId);
            if($this->topic == null) {
                $this->addError($attribute, 'Данной темы не существует');
            }
        }
    }

    /**
     * Проверка на уникальность номера
     * @param $attribute
     */
    public function uniqueNumber($attribute)
    {
        if(!empty($this->number)) {
            if($this->number != $this->topic->topic_number) {
                $topic = TheoryTopic::findOne(['topic_number' => $this->number]);
                if($topic != null) {
                    $this->addError($attribute, 'Тема с данным номером уже существует');
                }
            }
        }
    }

    // Функции для загрузки данных в модель

    /**
     * Загрузка данных для модели по id темы
     * @param integer|null $topicId
     */
    public function loadModel($topicId = null)
    {
        if($topicId != null) {
            $this->topicId = $topicId;
            $this->topic = TheoryTopic::findOne($topicId);
            if($this->topic != null) {
                $this->number = $this->topic->topic_number;
                $this->text = $this->topic->topic_text;
            }
        }
    }

    // Функции для добавления и обновления данных о теме билета

    /**
     * Функция добавления или обновления темы билета
     * @param $data
     * @return bool
     */
    public function saveTopic($data)
    {
        if($this->load($data)) {
            if($this->validate()) {
                if($this->topic == null && !empty($this->topicId)) {
                    $this->topic = TheoryTopic::findOne($this->topicId);
                }
                if($this->topic == null && empty($this->topicId)) {
                    return $this->addTopic();
                } else {
                    return $this->updateTopic();
                }
            }
        }
        return false;
    }

    /**
     * Функция добавления темы билета
     * @return bool
     */
    public function addTopic()
    {
        $topic = new TheoryTopic();
        $topic->topic_number = $this->number;
        $topic->topic_text = $this->text;
        return $topic->save();
    }

    /**
     * Функция обновления темы билета
     * @return bool
     */
    public function updateTopic()
    {
        $this->topic->topic_number = $this->number;
        $this->topic->topic_text = $this->text;
        return $this->topic->save();
    }

}