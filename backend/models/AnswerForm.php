<?php

namespace backend\models;

use common\models\TheoryAnswer;
use yii\base\Model;

/**
 * Class AnswerForm
 * @package backend\models
 *
 * @property integer[] $answerIds
 * @property integer $questionId
 * @property string[] $answerText
 * @property boolean $rightAnswer
 * @property array $rightAnswerNumbers
 * @property TheoryAnswer[] $answers
 */
class AnswerForm extends Model
{
    const ADD_SCENARIO = 'add';
    const UPDATE_SCENARIO = 'update';

    public $answerIds;
    public $questionId;
    public $answerText;
    public $rightAnswer;
    public $rightAnswerNumbers;

    private $answers;

    public function scenarios()
    {
        $scenarious = parent::scenarios();
        $scenarious[self::ADD_SCENARIO] = ['answerText', 'questionId', 'rightAnswer'];
        $scenarious[self::UPDATE_SCENARIO] = ['answerIds', 'questionId', 'answerText', 'rightAnswer'];
        return $scenarious;
    }

    public function rules()
    {
        return [
            [['questionId', 'answerIds', 'rightAnswer'], 'required'],
            [['questionId', 'rightAnswer'], 'integer'],
            [['answerText[0]', 'answerText[1]', 'answerText[2]', 'answerText[3]', 'answerText[4]'], 'string', 'max' => 1000],
            ['rightAnswer', 'isRightAnswerText'],

            ['answerId', 'answersExist'],
            ['answerText', 'consistentlyAnswerText'],
            ['questionId', 'exist', 'targetClass' => 'common\models\TheoryQuestion', 'targetAttribute' => 'id'],
        ];
    }

    /**
     * Метод проверяет есть ли вопросы с данными ID
     * @param $attr
     */
    public function answersExist($attr)
    {
        if(!empty($this->answerIds)) {
            foreach ($this->answerIds as $answerId) {
                $result = TheoryAnswer::findOne($answerId);
                if($result == null) {
                    $this->addError($attr, 'Ответа не существует');
                }
            }
        }
    }

    /**
     * Метод проверяет есть ли текст у правильного ответа
     * @param $attr
     */
    public function isRightAnswerText($attr)
    {
        if(!empty($this->rightAnswer)) {
            if(empty($this->answerText[$this->rightAnswer])) {
                $this->addError($attr, 'Нет текста правильного ответа');
            }
        }
    }

    /**
     * Метод проверяет все ли ответы идут друг за другом
     * @param $attr
     */
    public function consistentlyAnswerText($attr)
    {
        if(!empty($this->answerText)) {
            $isEmpty = false;
            foreach ($this->answerText as $answerText) {
                if(!empty($answerText) && $isEmpty) {
                    $this->addError($attr, 'Ответы должны идти подряд');
                }
                if(empty($answerText) && !$isEmpty) {
                    $isEmpty = true;
                }
            }
        }
    }

    // Методы для загрузки модели

    public function loadModel($questionId)
    {
        $this->questionId = $questionId;
        $this->rightAnswerNumbers = [
            0 => 1,
            1 => 2,
            2 => 3,
            3 => 4,
            4 => 5
        ];
        if($this->scenario == self::UPDATE_SCENARIO) {
            /** @var TheoryAnswer[] $result */
            $result = TheoryAnswer::find()->where(['question_id' => $questionId])->all();
            foreach ($result as $answer) {
                $this->answerText[] = $answer->answer_text;
                $this->answers[] = $answer;
                $this->answerIds[] = $answer->id;
                if(boolval($answer->right_answer)) {
                    $this->rightAnswer = count($this->answerIds) - 1;
                }
            }
        }
    }

    // Методы для сохранения модели

    public function saveModel($data)
    {
        if ($this->load($data)) {
            if ($this->validate()) {
                switch ($this->scenario) {
                    case self::ADD_SCENARIO: return $this->addAnswer();
                    case self::UPDATE_SCENARIO: return $this->updateAnswer();
                }
            }
        }
        return false;
    }

    public function addAnswer()
    {
        if(!empty($this->answerText)) {
            $res = true;
            for ($i = 0; $i < 5; $i++) {
                if(empty($this->answerText[$i]) && $this->rightAnswer == $i) {
                    return false;
                }
                if(!empty($this->answerText[$i])) {
                    $answer = new TheoryAnswer();
                    $answer->question_id = $this->questionId;
                    $answer->answer_text = $this->answerText[$i];
                    $answer->right_answer = $this->rightAnswer == $i ? true : false;
                    $res = $res && $answer->save();
                }
            }
            return $res;
        }
        return true;
    }

    public function updateAnswer()
    {
        $res = true;
        for ($i = 0; $i < count($this->answerText); $i++) {
            if(!empty($this->answers[$i])) {
                if(!empty($this->answerText[$i])) {
                    $this->answers[$i]->answer_text = $this->answerText[$i];
                    $this->answers[$i]->right_answer = $this->rightAnswer == $i ? true : false;
                    $res = $res && $this->answers[$i]->save();
                } else {
                    $this->answers[$i]->delete();
                }
            } else {
                if(!empty($this->answerText[$i])) {
                    $answer = new TheoryAnswer();
                    $answer->answer_text = $this->answerText[$i];
                    $answer->right_answer = $this->rightAnswer == $i ? true : false;
                    $res = $res && $answer->save();
                }
            }
        }
        return $res;
    }

    public function getWidgetInputs()
    {
        $inputs = [
            ['type' => 'hidden', 'attr' => 'questionId', 'label' => 'ID вопроса'],
            ['type' => 'hidden', 'attr' => 'answerText', 'label' => 'Ответы'],
            ['type' => 'textarea', 'attr' => 'answerText[0]', 'label' => 'Текст первого ответа'],
            ['type' => 'textarea', 'attr' => 'answerText[1]', 'label' => 'Текст второго ответа'],
            ['type' => 'textarea', 'attr' => 'answerText[2]', 'label' => 'Текст третьего ответа'],
            ['type' => 'textarea', 'attr' => 'answerText[3]', 'label' => 'Текст четвертого ответа'],
            ['type' => 'textarea', 'attr' => 'answerText[4]', 'label' => 'Текст пятого ответа'],
            ['type' => 'radioList', 'attr' => 'rightAnswer', 'label' => 'Правильный ответ', 'items' => $this->rightAnswerNumbers],
            ['type' => 'submit', 'label' => 'Сохранить', 'class' => 'btn-success'],
            ['type' => 'reset', 'label' => 'Отмена', 'class' => 'btn-danger']
        ];
        return $inputs;
    }
}