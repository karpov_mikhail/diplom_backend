<?php

namespace backend\models;

use common\models\AutoClass;
use common\models\File;
use common\models\PracticeCar;
use common\models\Staff;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class PracticeCarForm
 * @package backend\models
 *
 * @property int $carId
 * @property string $mark
 * @property string $model
 * @property double $carPower
 * @property int $maxLearners
 * @property int $instructorId
 * @property string[] $instructorNames
 * @property int $autoClassId
 * @property string[] $autoClassNames
 * @property UploadedFile $carPhoto
 * @property string $carPhotoUrl
 *
 * @property PracticeCar $practiceCar
 */
class PracticeCarForm extends Model
{
    const ADD_SCENARIO = 'add';
    const UPDATE_SCENARIO = 'update';

    public $carId;
    public $mark;
    public $model;
    public $carPower;
    public $maxLearners;
    public $instructorId;
    public $instructorNames;
    public $autoClassId;
    public $autoClassNames;
    public $carPhoto;
    public $carPhotoUrl;
    public $isDelPhoto;

    private $practiceCar;

    public function scenarios()
    {
        $scenarious = parent::scenarios();
        $scenarious[self::ADD_SCENARIO] = ['mark', 'model', 'carPower', 'maxLearners', 'instructorId', 'autoClassId', 'carPhoto'];
        $scenarious[self::UPDATE_SCENARIO] = ['carId', 'mark', 'model', 'carPower', 'maxLearners', 'instructorId', 'autoClassId', 'carPhoto'];
        return $scenarious;
    }

    public function rules()
    {
        return [
            [['carId', 'mark', 'model', 'carPower', 'maxLearners', 'teacherId', 'autoClassId'], 'required'],
            [['carId', 'instructorId', 'autoClassId', 'maxLearners'], 'integer'],
            [['mark', 'model'], 'string', 'max' => 255],
            ['carPower', 'double'],
            ['maxLearners', 'default', 'value' => 10],
            [['carPhoto'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],

            ['carId', 'exist', 'targetClass' => 'common\models\PracticeCar', 'targetAttribute' => 'id'],
            ['instructorId', 'exist', 'targetClass' => 'common\models\Staff', 'targetAttribute' => 'id'],
            ['autoClassId', 'exist', 'targetClass' => 'common\models\AutoClass', 'targetAttribute' => 'id'],
            ['instructorId', 'checkInstructor'],
        ];
    }

    public function checkInstructor($attr) {
        if($this->scenario == self::ADD_SCENARIO || $this->practiceCar->teacher_id != $this->instructorId) {
            $car = PracticeCar::findOne(['teacher_id' => $this->instructorId]);
            if(!empty($car->id)) {
                $this->addError($attr, 'Данный инструктор уже назначен на автомобиль');
            }
        }
    }

    // Методы для загрузки модели

    public function loadModel($carId = null)
    {
        if($this->scenario == self::UPDATE_SCENARIO && !empty($carId)) {
            $this->carId = $carId;
            $result = PracticeCar::findOne($carId);
            if(!empty($result)) {
                $this->practiceCar = $result;
                $this->mark = $result->mark;
                $this->model = $result->model;
                $this->carPower = $result->car_power;
                $this->maxLearners = $result->max_learners;
                $this->instructorId = $result->teacher_id;
                $this->autoClassId = $result->auto_class_id;
                $this->carPhotoUrl = File::getPath($result->car_photo_id);
            }
        }
        $this->loadInstructorNames();
        $this->loadAutoClassNames();
    }

    public function loadInstructorNames()
    {
        $this->instructorNames = Staff::getInstructorNames();
    }

    public function loadAutoClassNames()
    {
        $this->autoClassNames = AutoClass::getAutoClassNames();
    }

    // Методы для сохранения модели

    public function saveModel($data)
    {
        if ($this->load($data)) {
            $this->carPhoto = UploadedFile::getInstance($this, 'carPhoto');
            $this->isDelPhoto = $data['PracticeCarForm']['isDelPhoto'];
            if ($this->validate()) {
                switch ($this->scenario) {
                    case self::ADD_SCENARIO: return $this->addPracticeCar();
                    case self::UPDATE_SCENARIO: return $this->updatePracticeCar();
                }
            }
        }
        return false;
    }

    private function addPracticeCar()
    {
        $imageId = $this->addImage();
        $newCar = new PracticeCar();
        $newCar->mark = $this->mark;
        $newCar->model = $this->model;
        $newCar->car_power = $this->carPower;
        $newCar->max_learners = $this->maxLearners;
        $newCar->teacher_id = $this->instructorId;
        $newCar->auto_class_id = $this->autoClassId;
        if(!empty($this->carPhoto) && !empty($imageId) && is_numeric($imageId)) {
            $newCar->car_photo_id = $imageId;
        }

        return $newCar->save();
    }

    private function updatePracticeCar()
    {
        $imageId = $this->addImage();
        $this->practiceCar->mark = $this->mark;
        $this->practiceCar->model = $this->model;
        $this->practiceCar->car_power = $this->carPower;
        $this->practiceCar->max_learners = $this->maxLearners;
        $this->practiceCar->teacher_id = $this->instructorId;
        $this->practiceCar->auto_class_id = $this->autoClassId;
        if(!empty($this->carPhoto) && !empty($imageId) && is_numeric($imageId)) {
            $this->practiceCar->car_photo_id = $imageId;
        }
        return $this->practiceCar->save();
    }

    /**
     * Метод добавления изображения
     *
     * @return integer|bool|null
     */
    public function addImage()
    {
        if (!empty($this->carPhoto)) {
            $fileId = File::addFile($this->carPhoto, "car");
            if(!is_int($fileId)){
                $this->addError("carPhoto", "Не удалось загрузить файл");
                return false;
            }
            return $fileId;
        }
        return null;
    }

    public function getWidgetInputs()
    {
        $inputs = [];
        if(!empty($this->carPhotoUrl) && $this->scenario == self::UPDATE_SCENARIO) {
            $inputs = array_merge($inputs, [
                ['type' => 'image', 'attr' => 'carPhotoUrl', 'label' => 'Изображение автомобиля', 'src' => $this->carPhotoUrl]
            ]);
        }
        $inputs = array_merge($inputs, [
            ['type' => 'hidden', 'attr' => 'carId', 'label' => 'ID автомобиля'],
            ['type' => 'file', 'attr' => 'carPhoto', 'label' => 'Изображение для автомобиля']
        ]);
        if(!empty($this->carPhotoUrl) && $this->scenario == self::UPDATE_SCENARIO) {
            $inputs = array_merge($inputs, [
                ['type' => 'checkbox', 'attr' => 'isDelPhoto', 'label' => 'Удалить изображение?'],
            ]);
        }
        $inputs = array_merge($inputs, [
            ['type' => 'text', 'attr' => 'mark', 'label' => 'Марка авто'],
            ['type' => 'text', 'attr' => 'model', 'label' => 'Модель авто'],
            ['type' => 'text', 'attr' => 'carPower', 'label' => 'Мощность авто (в лошадиных силах)'],
            ['type' => 'text', 'attr' => 'maxLearners', 'label' => 'Максимальное кол-во обучающихся'],
            ['type' => 'select', 'attr' => 'instructorId', 'label' => 'Инструктор', 'items' => $this->instructorNames],
            ['type' => 'select', 'attr' => 'autoClassId', 'label' => 'Категория авто', 'items' => $this->autoClassNames],
            ['type' => 'submit', 'label' => 'Сохранить', 'class' => 'btn-success'],
            ['type' => 'reset', 'label' => 'Отмена', 'class' => 'btn-danger']
        ]);
        return $inputs;
    }
}