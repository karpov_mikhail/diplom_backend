<?php

namespace backend\models;

use common\models\Group;
use common\models\GroupOneTimeCode;
use common\models\LearningGroup;
use common\models\Staff;
use common\models\UserLearningGroup;
use yii\base\Model;
use yii\web\User;

/**
 * Class StaffForm
 * @package backend\models
 *
 * @property integer $staffId
 * @property string $firstName
 * @property string $lastName
 * @property string $secondName
 * @property integer $groupId
 * @property string[] $groupNames
 * @property int[] $learningGroupIds
 * @property string[] $learningGroupNames
 * @property int $salaryPerHour
 *
 * @property Staff $staff
 */
class StaffForm extends Model
{
    const ADD_SCENARIO = 'add';
    const UPDATE_SCENARIO = 'update';

    public $staffId;
    public $firstName;
    public $lastName;
    public $secondName;
    public $groupId;
    public $groupNames;
    public $learningGroupIds;
    public $learningGroupNames;
    public $salaryPerHour;

    private $staff;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarious = parent::scenarios();
        $scenarious[self::ADD_SCENARIO] = ['firstName', 'lastName', 'secondName', 'groupId', 'learningGroupIds', 'salaryPerHour'];
        $scenarious[self::UPDATE_SCENARIO] = ['staffId', 'firstName', 'lastName', 'secondName', 'groupId', 'learningGroupIds', 'salaryPerHour'];
        return $scenarious;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['staffId', 'exist', 'targetClass' => 'common\models\Staff', 'targetAttribute' => 'id'],
            ['groupId', 'exist', 'targetClass' => 'common\models\Group', 'targetAttribute' => 'id'],

            [['firstName', 'lastName', 'secondName'], 'trim'],
            [['firstName', 'lastName', 'groupId'], 'required'],
            [['groupId', 'salaryPerHour'], 'integer'],
            ['learningGroupIds', 'trim'],
            ['salaryPerHour', 'checkSalary']
        ];
    }

    public function checkSalary($attr)
    {
        if($this->groupId == Group::findOne(['code' => 'instructor'])->id && empty($this->salaryPerHour)) {
            $this->addError($attr, "Зарплата не указана!");
        }
    }

    // Функции для загрузки данных в модель

    /**
     * Загрузка данных для модели по id персонала
     * @param integer|null $staffId
     */
    public function loadModel($staffId = null)
    {
        if($staffId != null && $this->scenario == self::UPDATE_SCENARIO) {
            $this->staffId = $staffId;
            $this->staff = Staff::findOne($staffId);
            if($this->staff != null) {
                $this->firstName = $this->staff->first_name;
                $this->lastName = $this->staff->last_name;
                $this->secondName = $this->staff->second_name;
                $this->groupId = $this->staff->group->id;
                $this->salaryPerHour = $this->staff->salary_per_hour;
            }
        }
        $this->loadGroupNames();
        $this->loadLearningGroups();
    }

    /**
     * Метод загрузки имен групп
     */
    private function loadGroupNames()
    {
        /** @var Group[] $groups */
        $groups = Group::find()->where(['code' => [
            'admin', 'teacher', 'instructor', 'learner'
        ]])->all();
        foreach ($groups as $group) {
            $this->groupNames[$group->id] = $group->name;
        }
    }

    private function loadLearningGroups()
    {
        $this->learningGroupNames = LearningGroup::getLearningGroupNames();
        if($this->scenario == self::UPDATE_SCENARIO) {
            switch ($this->groupId) {
                case Group::findOne(['code' => 'teacher'])->id:
                    /** @var LearningGroup[] $result */
                    $result = LearningGroup::find()->where(['teacher_id' => $this->staffId])->all();
                    foreach ($result as $item) {
                        $this->learningGroupIds[] = $item->id;
                    }
                    break;
                case Group::findOne(['code' => 'learner'])->id:
                    /** @var UserLearningGroup[] $result */
                    $result = UserLearningGroup::find()->where(['staff_id' => $this->staffId])->all();
                    foreach ($result as $item) {
                        $this->learningGroupIds[] = $item->learning_group_id;
                    }
                    break;
            }
        }
    }

    // Функции для сохранения модели

    public function saveModel($data)
    {
        if($this->load($data)) {
            if($this->validate()) {
                switch ($this->scenario) {
                    case self::ADD_SCENARIO: return $this->addStaff(); break;
                    case self::UPDATE_SCENARIO: return $this->updateStaff(); break;
                }
            }
        }
        return false;
    }

    /**
     * Функция добавления персонала
     * @return bool
     */
    private function addStaff()
    {
        $newStaff = new Staff();
        $newStaff->first_name = $this->firstName;
        $newStaff->last_name = $this->lastName;
        $newStaff->second_name = $this->secondName;
        $newStaff->group_id = $this->groupId;
        if($this->groupId == Group::findOne(['code' => 'instructor'])->id) {
            $newStaff->salary_per_hour = $this->salaryPerHour;
        }
        $saveResult = $newStaff->save();
        if($saveResult) {
            $this->staffId = $newStaff->id;
            $this->staff = $newStaff;
            $this->generateCode();
            $this->saveLearningGroups();
        }
        return $saveResult;
    }

    /**
     * Функция изменения данных персонала
     * @return bool
     */
    private function updateStaff()
    {
        $this->staff->first_name = $this->firstName;
        $this->staff->last_name = $this->lastName;
        $this->staff->second_name = $this->secondName;
        $this->staff->group_id = $this->groupId;
        if($this->groupId == Group::findOne(['code' => 'instructor'])->id) {
            $this->staff->salary_per_hour = $this->salaryPerHour;
        }
        $saveResult = $this->staff->save();
        if($saveResult) {
            $this->generateCode();
            $this->saveLearningGroups();
        }
        return $saveResult;
    }

    private function saveLearningGroups()
    {
        switch ($this->groupId) {
            case Group::findOne(['code' => 'teacher'])->id:
                $this->saveTeacherLearningGroups();
                break;
            case Group::findOne(['code' => 'learner'])->id:
                $this->saveLearnerLearningGroups();
                break;
        }
    }

    /**
     * Функция сохранения привязки учителей к группам обучающихся
     */
    private function saveTeacherLearningGroups()
    {
        if($this->scenario == self::ADD_SCENARIO) {
            foreach ($this->learningGroupIds as $id) {
                $group = LearningGroup::findOne($id);
                $group->teacher_id = $this->staffId;
                $group->save();
            }
        }
        if($this->scenario == self::UPDATE_SCENARIO) {
            $oldLearningGroupIds = LearningGroup::getTeacherLearningGroupIds($this->staffId);
            if($this->learningGroupIds != $oldLearningGroupIds) {
                // Добавление учителя для групп обучающихся
                foreach ($this->learningGroupIds as $groupId) {
                    if(!in_array($groupId, $oldLearningGroupIds)) {
                        $group = LearningGroup::findOne($groupId);
                        $group->teacher_id = $this->staffId;
                        $group->save();
                    }
                }
                // Удаление учителя у групп обучающихся
                foreach ($oldLearningGroupIds as $groupId) {
                    if(!in_array($groupId, $this->learningGroupIds)) {
                        $group = LearningGroup::findOne($groupId);
                        $group->teacher_id = null;
                        $group->save();
                    }
                }
            }
        }
    }

    private function saveLearnerLearningGroups()
    {
        if($this->scenario == self::ADD_SCENARIO) {
            foreach ($this->learningGroupIds as $id) {
                $newGroup = new UserLearningGroup();
                $newGroup->staff_id = $this->staffId;
                $newGroup->learning_group_id = $id;
                $newGroup->save();
            }
        }
        if($this->scenario == self::UPDATE_SCENARIO) {
            $oldLearningGroupIds = UserLearningGroup::getUserLearningGroupIds($this->staffId);
            if($this->learningGroupIds != $oldLearningGroupIds) {
                // Добавление курсанта в группу обучающихся
                foreach ($this->learningGroupIds as $groupId) {
                    if(!in_array($groupId, $oldLearningGroupIds)) {
                        $newGroup = new UserLearningGroup();
                        $newGroup->staff_id = $this->staffId;
                        $newGroup->learning_group_id = $groupId;
                        $newGroup->save();
                    }
                }
                // Удаление курсанта из группы обучающихся
                foreach ($oldLearningGroupIds as $groupId) {
                    if(!in_array($groupId, $this->learningGroupIds)) {
                        $group = UserLearningGroup::findOne(['staff_id' => $this->staffId, 'learning_group_id' => $groupId]);
                        $group->delete();
                    }
                }
            }
        }
    }

    /**
     * Функция генерации уникального кода для привязки персонала
     */
    private function generateCode()
    {
        $code = GroupOneTimeCode::findOne(['staff_id' => $this->staffId]);

        if(empty($code)) {
            $newCode = new GroupOneTimeCode();
            $newCode->unique_code = GroupOneTimeCode::generateUniqueCode();
            $newCode->create_date = date('Y-m-d H:i:s');
            $newCode->activated = false;
            $newCode->staff_id = $this->staffId;
            $newCode->save();
        }
    }

    public function getWidgetInputs()
    {
        $groupInput = ['type' => 'select', 'attr' => 'groupId', 'label' => 'Должность', 'class' => 'js-staff-group', 'items' => $this->groupNames];
        if($this->scenario == self::UPDATE_SCENARIO) {
            $groupInput['disabled'] = true;
        }
        $inputs = [
            ['type' => 'text', 'attr' => 'lastName', 'label' => 'Фамилия'],
            ['type' => 'text', 'attr' => 'firstName', 'label' => 'Имя'],
            ['type' => 'text', 'attr' => 'secondName', 'label' => 'Отчество'],
            $groupInput,
            ['type' => 'text', 'attr' => 'salaryPerHour', 'label' => 'Зарплата (руб./час)', 'class' => 'js-staff-salary'],
            ['type' => 'checkboxList', 'attr' => 'learningGroupIds', 'label' => 'Группы курсантов',
                'class' => 'js-learning-group', 'items' => $this->learningGroupNames]
        ];
        $inputs = array_merge($inputs, [
            ['type' => 'submit', 'label' => 'Сохранить', 'class' => 'btn-success'],
            ['type' => 'reset', 'label' => 'Отмена', 'class' => 'btn-danger']
        ]);
        if($this->scenario == self::UPDATE_SCENARIO) {
            $inputs[] = ['type' => 'hidden', 'attr' => 'staffId', 'label' => 'ID персонала'];
        }
        return $inputs;
    }
}