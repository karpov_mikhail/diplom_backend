<?php

namespace backend\models;

use common\models\AutoClass;
use common\models\LearningGroup;
use yii\base\Model;

/**
 * Class LearningGroupForm
 * @package backend\models
 *
 * @property int $learningGroupId
 * @property string $name
 * @property int $autoClassId
 * @property string[] $autoClassNames
 *
 * @property LearningGroup $learningGroup
 */
class LearningGroupForm extends Model
{
    const ADD_SCENARIO = 'add';
    const UPDATE_SCENARIO = 'update';

    public $learningGroupId;
    public $name;
    public $autoClassId;
    public $autoClassNames;

    private $learningGroup;

    public function scenarios()
    {
        $scenarious = parent::scenarios();
        $scenarious[self::ADD_SCENARIO] = ['name', 'autoClassId'];
        $scenarious[self::UPDATE_SCENARIO] = ['learningGroupId', 'name', 'autoClassId'];
        return $scenarious;
    }

    public function rules()
    {
        return [
            [['learningGroupId', 'name', 'autoClassId'], 'required'],
            [['learningGroupId', 'autoClassId'], 'integer'],
            [['name'], 'string', 'max' => 255],

            ['autoClassId', 'exist', 'targetClass' => 'common\models\AutoClass', 'targetAttribute' => 'id'],
        ];
    }

    // Методы для загрузки модели

    public function loadModel($learningGroupId = null)
    {
        if($this->scenario == self::UPDATE_SCENARIO && !empty($learningGroupId)) {
            $this->learningGroupId = $learningGroupId;
            $result = LearningGroup::findOne($learningGroupId);
            if(!empty($result)) {
                $this->learningGroup = $result;
                $this->name = $result->name;
                $this->autoClassId = $result->auto_class_id;
            }
        }
        $this->loadAutoClassNames();
    }

    public function loadAutoClassNames()
    {
        $this->autoClassNames = AutoClass::getAutoClassNames();
    }

    // Методы для сохранения модели

    public function saveModel($data)
    {
        if ($this->load($data)) {
            if ($this->validate()) {
                switch ($this->scenario) {
                    case self::ADD_SCENARIO: return $this->addLearningGroup();
                    case self::UPDATE_SCENARIO: return $this->updateLearningGroup();
                }
            }
        }
        return false;
    }

    private function addLearningGroup()
    {
        $newGroup = new LearningGroup();
        $newGroup->name = $this->name;
        $newGroup->auto_class_id = $this->autoClassId;
        return $newGroup->save();
    }

    private function updateLearningGroup()
    {
        $this->learningGroup->name = $this->name;
        $this->learningGroup->auto_class_id = $this->autoClassId;
        return $this->learningGroup->save();
    }

    public function getWidgetInputs()
    {
        $inputs = [
            ['type' => 'hidden', 'attr' => 'learningGroupId', 'label' => 'ID обучающей группы'],
            ['type' => 'text', 'attr' => 'name', 'label' => 'Название группы'],
            ['type' => 'select', 'attr' => 'autoClassId', 'label' => 'Категория авто', 'items' => $this->autoClassNames],
            ['type' => 'submit', 'label' => 'Сохранить', 'class' => 'btn-success'],
            ['type' => 'reset', 'label' => 'Отмена', 'class' => 'btn-danger']
        ];
        return $inputs;
    }
}