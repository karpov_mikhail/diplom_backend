<?php

namespace backend\models;

use yii\base\Model;
use yii\helpers\FileHelper;

class SettingsForm extends Model
{
    public $siteName;
    public $address;
    public $phone;
    public $colorScheme;
    public $colors;
    public $logoBig;
    public $logoSmall;

    private $settingsPath;

    public function rules()
    {
        return [
            [['siteName', 'address', 'phone'], 'required'],
            [['siteName', 'address', 'phone'], 'string', 'max' => 255]
        ];
    }

    public function loadModel()
    {
        $this->settingsPath = FileHelper::normalizePath(realpath(\Yii::getAlias('@app/..')) . '/frontend/config/settings.json');
        $settingsFile = file_exists($this->settingsPath);
        if($settingsFile !== false) {
            $settings = json_decode(file_get_contents($this->settingsPath));
            $this->siteName = $settings->{'name'};
            $this->address = $settings->{'address'};
            $this->phone = $settings->{'phone'};
            $this->colorScheme = $settings->{'color'};
            $this->colors = [
                'blue' => 'Синий',
                'red' => 'Красный',
                'green' => 'Зеленый',
                'yellow' => 'Желтый',
                'black' => 'Черный'
            ];
        }
    }

    public function saveModel($data)
    {
        if($this->load($data)) {
            if($this->validate()) {
                $settings = [];
                $settings['name'] = $this->siteName;
                $settings['address'] = $this->address;
                $settings['phone'] = $this->phone;
                $settings['color'] = $this->colorScheme;
                $result = file_put_contents($this->settingsPath, json_encode($settings));
                return ($result === false) ? false : true;
            }
        }
        return false;
    }

    public function getWidgetInputs()
    {
        $inputs = [
            ['type' => 'file', 'attr' => 'logoSmall', 'label' => 'Логотип маленький'],
            ['type' => 'file', 'attr' => 'logoBig', 'label' => 'Логотип большой'],
            ['type' => 'text', 'attr' => 'siteName', 'label' => 'Название автошколы'],
            ['type' => 'text', 'attr' => 'address', 'label' => 'Адрес автошколы'],
            ['type' => 'phone', 'attr' => 'phone', 'label' => 'Телефон автошколы'],
            ['type' => 'select', 'attr' => 'colorScheme', 'label' => 'Цвета сайта', 'items' => $this->colors],
            ['type' => 'submit', 'label' => 'Сохранить', 'class' => 'btn-success'],
            ['type' => 'reset', 'label' => 'Отмена', 'class' => 'btn-danger']
        ];
        return $inputs;
    }
}