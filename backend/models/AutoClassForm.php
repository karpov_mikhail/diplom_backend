<?php

namespace backend\models;

use common\models\AutoClass;
use yii\base\Model;

/**
 * Class AutoClassForm
 * @package backend\models
 *
 * @property integer $autoClassId
 * @property string $name
 * @property string $description
 * @property boolean $active
 * @property AutoClass $autoClass
 */
class AutoClassForm extends Model
{
    public $autoClassId;
    public $name;
    public $description;
    public $active;
    private $autoClass;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['autoClassId', 'checkAutoClassId'],

            ['name', 'trim'],
            ['name', 'required'],
            ['name', 'uniqueName'],
            ['name', 'string', 'max' => 255],

            ['description', 'trim'],
            ['description', 'string', 'max' => 4098],

            ['active', 'boolean']
        ];
    }

    // Функции для проверки полей

    /**
     * Проверка на существование категории
     * @param $attribute
     */
    public function checkAutoClassId($attribute)
    {
        if(!empty($this->autoClassId)) {
            $this->autoClass = AutoClass::findOne($this->autoClassId);
            if($this->autoClass == null) {
                $this->addError($attribute, 'Данной категории не существует');
            }
        }
    }

    /**
     * Проверка на уникальность названия категории
     * @param $attribute
     */
    public function uniqueName($attribute)
    {
        if(!empty($this->name)) {
            if($this->name != $this->autoClass->name) {
                $autoClass = AutoClass::findOne(['name' => $this->name]);
                if($autoClass != null) {
                    $this->addError($attribute, 'Категория с данным названием уже существует');
                }
            }
        }
    }

    // Функции для загрузки данных в модель

    /**
     * Загрузка данных для модели по id категории
     * @param integer|null $autoClassId
     */
    public function loadModel($autoClassId = null)
    {
        if($autoClassId != null) {
            $this->autoClassId = $autoClassId;
            $this->autoClass = AutoClass::findOne($autoClassId);
            if($this->autoClass != null) {
                $this->name = $this->autoClass->name;
                $this->description = $this->autoClass->description;
                $this->active = $this->autoClass->active;
            }
        }
    }

    // Функции для добавления и обновления данных категории авто

    /**
     * Функция добавления или обновления категории авто
     * @param $data
     * @return bool
     */
    public function saveAutoClass($data)
    {
        if($this->load($data)) {
            if($this->validate()) {
                if($this->autoClass == null && !empty($this->autoClassId)) {
                    $this->autoClass = AutoClass::findOne($this->autoClassId);
                }
                if($this->autoClass == null && empty($this->autoClassId)) {
                    return $this->addAutoClass();
                } else {
                    return $this->updateAutoClass();
                }
            }
        }
        return false;
    }

    /**
     * Функция добавления категории авто
     * @return bool
     */
    public function addAutoClass()
    {
        $autoClass = new AutoClass();
        $autoClass->name = $this->name;
        $autoClass->description = $this->description;
        $autoClass->active = $this->active;
        return $autoClass->save();
    }

    /**
     * Функция обновления категории авто
     * @return bool
     */
    public function updateAutoClass()
    {
        $this->autoClass->name = $this->name;
        $this->autoClass->description = $this->description;
        $this->autoClass->active = $this->active;
        return $this->autoClass->save();
    }

}