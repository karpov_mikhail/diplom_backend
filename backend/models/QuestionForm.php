<?php

namespace backend\models;

use common\models\AutoClass;
use common\models\File;
use common\models\TheoryAnswer;
use common\models\TheoryCard;
use common\models\TheoryQuestion;
use common\models\TheoryTopic;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class QuestionForm
 * @package backend\models
 *
 * @property integer $questionId        ID вопроса
 * @property integer $cardId            ID билета
 * @property string $questionNumber     Номер вопроса
 * @property string $questionText       Текст вопроса
 * @property integer $topicId           ID темы
 * @property UploadedFile $imageFile    Файл с изображением
 * @property AnswerForm[] $answers      Ответы на вопрос
 * @property string[] $topicNames       Названия тем
 * @property string $imageSrc           Ссылка на файл изображения
 * @property string $note               Комментарии к вопросу
 * @property string[] $cardNumbers      Билеты
 * @property boolean $isDelImage        Флаг удаления изображения
 *
 * @property TheoryQuestion $question
 */
class QuestionForm extends Model
{
    const ADD_SCENARIO = 'add';
    const UPDATE_SCENARIO = 'update';

    public $questionId;
    public $cardId;
    public $questionNumber;
    public $questionText;
    public $topicId;
    public $imageFile;
    public $answers;
    public $topicNames;
    public $imageSrc;
    public $note;
    public $cardNumbers;
    public $isDelImage = false;

    private $question;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarious = parent::scenarios();
        $scenarious[self::ADD_SCENARIO] = ['cardNumbers', 'questionText', 'topicId', 'imageFile', 'note', 'questionNumber'];
        $scenarious[self::UPDATE_SCENARIO] = ['questionId', 'cardNumbers', 'questionText', 'topicId', 'imageFile', 'note', 'questionNumber'];
        return $scenarious;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cardId', 'questionText', 'topicId', 'note', 'questionNumber'], 'required'],
            [['imageFile'], 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
            [['questionId', 'cardId', 'topicId', 'questionNumber'], 'integer'],
            [['questionText', 'note'], 'string', 'max' => 1000],

            ['topicId', 'exist', 'targetClass' => 'common\models\TheoryTopic', 'targetAttribute' => 'id'],
            ['questionId', 'exist', 'targetClass' => 'common\models\TheoryQuestion', 'targetAttribute' => 'id'],
            ['cardId', 'exist', 'targetClass' => 'common\models\TheoryCa', 'targetAttribute' => 'id'],
        ];
    }

    // Методы для загрузки модели

    /**
     * @param integer|null $questionId
     */
    public function loadModel($questionId = null)
    {
        if ($questionId != null && is_numeric($questionId) && $this->scenario == self::UPDATE_SCENARIO) {
            $this->question = TheoryQuestion::findOne($questionId);
            if($this->question != null) {
                $this->questionId = $questionId;
                $this->cardId = $this->question->card_id;
                $this->questionText = $this->question->question_text;
                $this->questionNumber = $this->question->question_number;
                $this->topicId = $this->question->topic_id;
                $this->imageSrc = File::getPath($this->question->image_id);
                $this->note = $this->question->note;
            }
        }
        $this->loadTopicNames();
        $this->loadCardNumbers();
    }

    /**
     * Метод загрузки имен тем вопросов
     */
    public function loadTopicNames()
    {
        /** @var TheoryTopic[] $result */
        $result = TheoryTopic::find()->select(["id", "topic_text"])->all();
        foreach ($result as $topic) {
            $this->topicNames[$topic->id] = $topic->topic_text;
        }
    }

    /**
     * Метод загрузки номеров билетов
     */
    public function loadCardNumbers()
    {
        /** @var TheoryCard[] $result */
        $result = TheoryCard::find()->all();
        $cardIds = [];
        foreach ($result as $card) {
            $this->cardNumbers[$card->id] = "Билет " . $card->card_number;
            $cardIds[] = $card->id;
        }
        $cardAutoClassString = TheoryCard::getCardsAutoClassString($cardIds);
        foreach ($cardAutoClassString as $key=>$string) {
            $this->cardNumbers[$key] .= " : Категории " . $string;
        }
    }

    // Методы сохранения вопросов

    /**
     * Метод сохранения модели
     *
     * @param $data array
     * @return bool
     */
    public function saveModel($data)
    {
        if($this->load($data)) {
            $this->imageFile = UploadedFile::getInstance($this, 'imageFile');
            $this->cardId = $data['QuestionForm']['cardId'];
            $this->isDelImage = $data['QuestionForm']['isDelImage'];
            if ($this->validate()) {
                switch ($this->scenario) {
                    case self::ADD_SCENARIO: return $this->addQuestion();
                    case self::UPDATE_SCENARIO: return $this->updateQuestion();
                }
            }
        }
        return false;
    }

    /**
     * Метод для добавления вопроса
     *
     * @return boolean
     */
    public function addQuestion()
    {
        $imageId = $this->addImage();
        $newQuestion = new TheoryQuestion();
        $newQuestion->topic_id = $this->topicId;
        $newQuestion->card_id = $this->cardId;
        $newQuestion->question_text = $this->questionText;
        $newQuestion->question_number = $this->questionNumber;
        $newQuestion->note = $this->note;
        if(!empty($this->imageFile) && !empty($imageId) && is_numeric($imageId)) {
            $newQuestion->image_id = $imageId;
        }
        $result = $newQuestion->save();
        if ($result === true) {
            $this->questionId = $newQuestion->id;
        }
        return $result;
    }

    /**
     * Метод обновления вопроса
     *
     * @return boolean
     */
    public function updateQuestion()
    {
        $imageId = $this->addImage();
        $this->question->card_id = $this->cardId;
        $this->question->question_text = $this->questionText;
        $this->question->question_number = $this->questionNumber;
        $this->question->topic_id = $this->topicId;
        $this->question->note = $this->note;
        if(!empty($this->imageFile) && !empty($imageId) && is_numeric($imageId)) {
            $this->question->image_id = $imageId;
        }
        if($this->isDelImage) {
            $this->question->image_id = null;
        }
        $result = $this->question->save();
        return $result;
    }

    /**
     * Метод добавления изображения
     *
     * @return integer|bool|null
     */
    public function addImage()
    {
        if (!empty($this->imageFile)) {
            $fileId = File::addFile($this->imageFile, "question");
            if(!is_int($fileId)){
                $this->addError("imageFile", "Не удалось загрузить файл");
                return false;
            }
            return $fileId;
        }
        return null;
    }

    public function getWidgetInputs()
    {
        $inputs = [];
        if(!empty($this->imageSrc) && $this->scenario == self::UPDATE_SCENARIO) {
            $inputs = array_merge($inputs,[
                ['type' => 'image', 'attr' => 'imageSrc', 'label' => 'Изображение для вопроса', 'src' => $this->imageSrc]
            ]);
        }
        $inputs = array_merge($inputs, [
            ['type' => 'select', 'attr' => 'cardId', 'label' => 'Номер билета', 'items' => $this->cardNumbers],
            ['type' => 'text', 'attr' => 'questionNumber', 'label' => 'Номер вопроса'],
            ['type' => 'textarea', 'attr' => 'questionText', 'label' => 'Текст вопроса'],
            ['type' => 'select', 'attr' => 'topicId', 'label' => 'Тема вопроса', 'items' => $this->topicNames],
            ['type' => 'file', 'attr' => 'imageFile', 'label' => 'Изображение для вопроса'],
        ]);
        if(!empty($this->imageSrc) && $this->scenario == self::UPDATE_SCENARIO) {
            $inputs = array_merge($inputs, [
                ['type' => 'checkbox', 'attr' => 'isDelImage', 'label' => 'Удалить изображение?'],
            ]);
        }
        $inputs = array_merge($inputs, [
            ['type' => 'textarea', 'attr' => 'note', 'label' => 'Комментарии для вопроса'],
            ['type' => 'submit', 'label' => 'Добавить ответы к вопросу', 'class' => 'btn-success'],
            ['type' => 'reset', 'label' => 'Отмена', 'class' => 'btn-danger']
        ]);
        if($this->scenario == self::UPDATE_SCENARIO) {
            $inputs[] = ['type' => 'hidden', 'attr' => 'questionId', 'label' => 'ID вопроса'];
        }
        return $inputs;
    }
}