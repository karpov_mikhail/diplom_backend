<?php

namespace backend\models;

use common\models\AutoClass;
use common\models\TheoryCard;
use common\models\TheoryCardAutoClass;
use yii\base\Model;

/**
 * Class CardForm
 * @package backend\models
 *
 * @property integer $cardId
 * @property integer $cardNumber
 * @property integer[] $autoClassIds
 * @property string[] $autoClassNames
 *
 * @property TheoryCard $card
 */
class CardForm extends Model
{
    const ADD_SCENARIO = 'add';
    const UPDATE_SCENARIO = 'update';

    public $cardId;
    public $cardNumber;
    public $autoClassIds;
    public $autoClassNames;
    private $card;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarious = parent::scenarios();
        $scenarious[self::ADD_SCENARIO] = ['cardNumber', 'autoClassIds'];
        $scenarious[self::UPDATE_SCENARIO] = ['cardId', 'cardNumber', 'autoClassIds'];
        return $scenarious;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['cardId', 'exist', 'targetClass' => 'common\models\TheoryCard', 'targetAttribute' => 'id'],

            ['cardNumber', 'trim'],
            ['cardNumber', 'required'],
            ['autoClassIds', 'requiredAutoClassIds'],
            ['cardNumber', 'integer'],
        ];
    }

    // Функции для проверки полей

    public function requiredAutoClassIds($attribute)
    {
        if(empty($this->autoClassIds) || count($this->autoClassIds) == 0) {
            $this->addError($attribute, 'Билет должен принадлежать хотя бы одной категории');
        }
    }

    // Функции для загрузки данных в модель

    /**
     * Загрузка данных для модели по id темы
     * @param integer|null $cardId
     */
    public function loadModel($cardId = null)
    {
        if($cardId != null && $this->scenario == self::UPDATE_SCENARIO) {
            $this->cardId = $cardId;
            $this->card = TheoryCard::findOne($cardId);
            if($this->card != null) {
                $this->cardNumber = $this->card->card_number;
            }
        }
        $this->loadAutoClassNames();
    }

    /**
     *
     */
    public function loadAutoClassNames()
    {
        $this->autoClassNames = AutoClass::getAutoClassNames();
        /** @var TheoryCardAutoClass[] $result */
        $result = TheoryCardAutoClass::find()->where(['card_id' => $this->cardId])->all();
        foreach ($result as $questionAutoClass) {
            $this->autoClassIds[] = $questionAutoClass->auto_class_id;
        }
    }

    // Функции для добавления и обновления билета

    /**
     * Функция сохранения темы билета
     * @param $data
     * @return bool
     */
    public function saveTopic($data)
    {
        if($this->load($data)) {
            $this->autoClassIds = $data['CardForm']['autoClassIds'];
            if($this->validate()) {
                switch ($this->scenario) {
                    case self::ADD_SCENARIO: return $this->addCard(); break;
                    case self::UPDATE_SCENARIO: return $this->updateCard(); break;
                }
            }
        }
        return false;
    }

    /**
     * Функция добавления билета
     * @return bool
     */
    public function addCard()
    {
        $topic = new TheoryCard();
        $topic->card_number = $this->cardNumber;
        $result = $topic->save();
        if($result) {
            $this->cardId = $topic->id;
            $this->saveCardAutoClasses();
        }
        return $result;
    }

    /**
     * Функция обновления билета
     * @return bool
     */
    public function updateCard()
    {
        $this->card->card_number = $this->cardNumber;
        $result = $this->card->save();
        if($result) {
            $this->saveCardAutoClasses();
        }
        return $result;
    }

    public function saveCardAutoClasses()
    {
        if($this->scenario == self::UPDATE_SCENARIO && !empty($this->cardId)) {
            $oldQuestionAutoClassIds = $this->card->getCardAutoClassIds();
            if($this->autoClassIds != $oldQuestionAutoClassIds) {
                // Присоединение пользователя к новой группе
                foreach ($this->autoClassIds as $autoGroupId) {
                    if(!in_array($autoGroupId, $oldQuestionAutoClassIds)) {
                        $newQuestionAutoClass = new TheoryCardAutoClass();
                        $newQuestionAutoClass->card_id = $this->cardId;
                        $newQuestionAutoClass->auto_class_id = $autoGroupId;
                        $newQuestionAutoClass->save();
                    }
                }
                // Удаление пользователя из группы
                foreach ($oldQuestionAutoClassIds as $autoGroupId) {
                    if(!in_array($autoGroupId, $this->autoClassIds)){
                        $oldQuestionAutoClass = TheoryCardAutoClass::findOne(['card_id' => $this->cardId, 'auto_class_id' => $autoGroupId]);
                        $oldQuestionAutoClass->delete();
                    }
                }
            }
        } else {
            if(!empty($this->cardId)) {
                foreach ($this->autoClassIds as $autoGroupId) {
                    $newQuestionAutoClass = new TheoryCardAutoClass();
                    $newQuestionAutoClass->card_id = $this->cardId;
                    $newQuestionAutoClass->auto_class_id = $autoGroupId;
                    $newQuestionAutoClass->save();
                }
            }
        }
    }

    public function getWidgetInputs()
    {
        $inputs = [
            ['type' => 'text', 'attr' => 'cardNumber', 'label' => 'Номер билета'],
            ['type' => 'checkboxList', 'attr' => 'autoClassIds', 'label' => 'Категории авто', 'items' => $this->autoClassNames]
        ];
        $inputs = array_merge($inputs, [
            ['type' => 'submit', 'label' => 'Добавить', 'class' => 'btn-success'],
            ['type' => 'reset', 'label' => 'Отмена', 'class' => 'btn-danger']
        ]);
        if($this->scenario == self::UPDATE_SCENARIO) {
            $inputs[] = ['type' => 'hidden', 'attr' => 'cardId', 'label' => 'ID билета'];
        }
        return $inputs;
    }
}