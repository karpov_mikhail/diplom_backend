<?php

namespace backend\models;

use common\models\Group;
use common\models\User;
use common\models\UserGroup;
use yii\base\Model;

/**
 * Class UserInfForm
 * @package backend\models
 *
 * @property integer $userId
 * @property string $username
 * @property string $newPassword
 * @property string $repeatNewPassword
 * @property string $email
 * @property string $firstName
 * @property string $lastName
 * @property string $secondName
 * @property string $phoneNumber
 * @property array $userGroupIds
 * @property array $groupNames
 * @property User $user
 */
class UserInfForm extends Model
{
    const UNREGISTERED_CODE = 'unregistered';
    const REGISTERED_CODE = 'registered';
    public $userId;
    public $username;
    public $newPassword;
    public $repeatNewPassword;
    public $email;
    public $firstName;
    public $lastName;
    public $secondName;
    public $phoneNumber;
    public $userGroupIds;
    public $groupNames;
    private $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['userId', 'checkUserId'],

            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'uniqueUserName'],
            ['username', 'string', 'min' => 3, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'uniqueEmail'],

            ['newPassword', 'string'],
            ['newPassword', 'checkNewPassword'],

            ['repeatNewPassword', 'string'],
            ['repeatNewPassword', 'checkRepeatNewPassword'],

            ['firstName', 'string', 'max' => 255],
            ['lastName', 'string', 'max' => 255],
            ['secondName', 'string', 'max' => 255],

            ['phoneNumber', 'string', 'max' => 20],
            ['phoneNumber', 'checkPhoneNumber'],

            ['userGroupIds', 'checkUserGroupIds']
        ];

    }

    // Функции для проверки полей

    /**
     * Проверка на существование пользователя
     * @param $attribute
     */
    public function checkUserId($attribute)
    {
        if(!empty($this->userId)) {
            $this->user = User::findOne($this->userId);
            if($this->user == null) {
                $this->addError($attribute, 'Данного пользователя не существует');
            }
        }
    }

    /**
     * Проверка на уникальность имени пользователя
     * @param $attribute
     */
    public function uniqueUserName($attribute) {
        if(!empty($this->username)) {
            if($this->username != $this->user->username) {
                $user = User::findOne(['username' => $this->username]);
                if($user != null) {
                    $this->addError($attribute, 'Данное имя пользователя уже используется');
                }
            }
        }
    }

    /**
     * Проверка на уникальность email
     * @param $attribute
     */
    public function uniqueEmail($attribute) {
        if(!empty($this->email)) {
            if($this->email != $this->user->email) {
                $user = User::findOne(['email' => $this->email]);
                if($user != null) {
                    $this->addError($attribute, 'Данный E-mail уже используется');
                }
            }
        }
    }

    /**
     * Проверка нового пароля
     * @param $attribute
     */
    public function checkNewPassword($attribute) {
        if(!empty($this->newPassword)) {
            if(strlen($this->newPassword) < 6 || strlen($this->newPassword) > 20) {
                $this->addError($attribute, 'Пароль должен содержать от 6 до 20 символов');
            }
        }
    }

    /**
     * Проверка повтора нового пароля
     * @param $attribute
     */
    public function checkRepeatNewPassword($attribute) {
        if(!empty($this->repeatNewPassword)) {
            if($this->repeatNewPassword != $this->newPassword) {
                $this->addError($attribute, 'Пароль должен содержать от 6 до 20 символов');
            }
        }
    }

    /**
     * Проверка телефона
     * @param $attribute
     */
    public function checkPhoneNumber($attribute) {
        if(!empty($this->phoneNumber)) {

        }
    }

    /**
     * Проверка Id групп пользователей, к которым принадлежит нужный пользователь
     * @param $attribute
     */
    public function checkUserGroupIds($attribute) {
        if(!empty($this->userGroupIds)) {

        }
    }

    // Функции для загрузки информации в модель

    /**
     * Загрузка данных для модели по id пользователя
     * @param integer|null $userId
     */
    public function loadModel($userId = null) {
        if ($userId != null) {
            $this->userId = $userId;
            $this->user = User::findOne($userId);
            if($this->user != null) {
                $this->username = $this->user->username;
                $this->email = $this->user->email;
                $this->firstName = $this->user->first_name;
                $this->lastName = $this->user->last_name;
                $this->secondName = $this->user->second_name;
                $this->phoneNumber = $this->user->phone_number;
            }
        }
        $this->loadUserGroup();
    }

    /**
     * Загрузка групп пользователей
     */
    public function loadUserGroup() {
        // Ищем все имена групп, кроме группы "Незарегистрированные пользователи"
        $dbResult = Group::find()
            ->where('code != :code', ['code' => self::UNREGISTERED_CODE])
            ->select(['id', 'name'])
            ->all();
        $groups = [];
        /** @var Group $group */
        foreach ($dbResult as $group) {
            $groups[$group->id] = $group->name;
        }
        $this->groupNames = $groups;
        // Если не новый пользователь, то загружаются группы, к которым принадлежит пользователь
        if($this->user != null) {
            $this->userGroupIds = $this->user->getUserGroupIds();
        } else {
            $group = Group::findOne(['code' => self::REGISTERED_CODE]);
            $this->userGroupIds = [ $group->id ];
        }
    }

    // Функции для добавления или обновления данных о пользователе

    /**
     * Сохранение (или обновление) данных о пользователе
     * @param $data
     * @return bool
     */
    public function saveUserInfo($data) {
        if($this->load($data)) {
            if($this->validate()) {
                if($this->user == null && !empty($this->userId)) {
                    $this->user = User::findOne($this->userId);
                }
                if($this->user == null && empty($this->userId)) {
                    return $this->addUserData();
                } else {
                    return $this->updateUserData();
                }
            }
        }
        return false;
    }

    /**
     * Функция сохранения данных о новом пользователе
     * @return bool
     */
    private function addUserData() {
        $user = new User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->first_name = $this->firstName;
        $user->last_name = $this->lastName;
        $user->second_name = $this->secondName;
        $user->phone_number = $this->phoneNumber;
        if(empty($this->newPassword)) {
            return false;
        }
        $user->setPassword($this->newPassword);
        $user->generateAuthKey();
        $result = $user->save();
        if($result) {
            $this->updateUserGroup($user->id);
        }
        return $result;
    }

    /**
     * Функция обновления данных существующего пользователя
     * @return bool
     */
    private function updateUserData() {
        $this->user->username = $this->username;
        $this->user->email = $this->email;
        $this->user->first_name = $this->firstName;
        $this->user->last_name = $this->lastName;
        $this->user->second_name = $this->secondName;
        $this->user->phone_number = $this->phoneNumber;
        if(!empty($this->newPassword)) {
            $this->user->setPassword($this->newPassword);
        }
        $result = $this->user->save();
        if($result) {
            $this->updateUserGroup();
        }
        return $result;
    }

    /**
     * Функция добавления (или обновления) групп, к которым принадлежит пользователь
     * @param null $id
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    private function updateUserGroup($id = null) {
        if($id == null) {
            $oldUserGroupIds = $this->user->getUserGroupIds();
            if($this->userGroupIds != $oldUserGroupIds) {
                // Присоединение пользователя к новой группе
                foreach ($this->userGroupIds as $userGroupId) {
                    if(!in_array($userGroupId, $oldUserGroupIds)){
                        $newUserGroup = new UserGroup();
                        $newUserGroup->user_id = $this->userId;
                        $newUserGroup->group_id = $userGroupId;
                        $newUserGroup->save();
                    }
                }
                // Удаление пользователя из группы
                foreach ($oldUserGroupIds as $userGroupId) {
                    if(!in_array($userGroupId, $this->userGroupIds)){
                        $oldUserGroup = UserGroup::findOne(['user_id' => $this->userId, 'group_id' => $userGroupId]);
                        $oldUserGroup->delete();
                    }
                }
            }
        }
        else {
            foreach ($this->userGroupIds as $userGroupId) {
                $newUserGroup = new UserGroup();
                $newUserGroup->user_id = $id;
                $newUserGroup->group_id = $userGroupId;
                $newUserGroup->save();
            }
        }
    }

}