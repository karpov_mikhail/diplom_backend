<?php
/* @var $this yii\web\View */
/* @var $model \backend\models\TheoryTopicForm */
/* @var $inputs array */
?>
<?= \common\widgets\FormWidget::widget(['model' => $model, 'input' => $inputs, 'id'=>'topic-add-form'])?>
