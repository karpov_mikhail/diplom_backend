<?php
/* @var $this yii\web\View */
/* @var $data array */
/* @var $columns array */

$this->title = 'Темы билетов';
?>
<div class="topic-index">
    <?= \yii\helpers\Html::a('<span class="glyphicon glyphicon-plus"></span> Добавить тему билета', ['topic/add'], ['class' => 'btn btn-success add-button'])?>
    <?= \common\widgets\myDataTable::widget(["data" => $data, "columns" => $columns, "detailUrl" => "topic/detail/#id#", "deleteUrl" => "topic/delete/#id#"])?>
    <?= \yii\widgets\LinkPager::widget(['pagination' => $pages]);?>
</div>
