<?php
/** @var $this \yii\web\View */
/** @var $model \backend\models\UserInfForm */
$inputs = [
    ['type' => 'hidden', 'attr' => 'userId', 'label' => 'ID пользователя'],
    ['type' => 'text', 'attr' => 'username', 'label' => 'Имя пользователя'],
    ['type' => 'email', 'attr' => 'email', 'label' => 'E-mail'],
    ['type' => 'password', 'attr' => 'newPassword', 'label' => 'Новый пароль'],
    ['type' => 'password', 'attr' => 'repeatNewPassword', 'label' => 'Повторите пароль'],
    ['type' => 'text', 'attr' => 'lastName', 'label' => 'Фамилия'],
    ['type' => 'text', 'attr' => 'firstName', 'label' => 'Имя'],
    ['type' => 'text', 'attr' => 'secondName', 'label' => 'Отчество'],
    ['type' => 'phone', 'attr' => 'phoneNumber', 'label' => 'Номер телефона'],
    ['type' => 'checkboxList', 'attr' => 'userGroupIds', 'label' => 'Группы пользователей', 'items' => $model->groupNames],
    ['type' => 'submit', 'label' => 'Сохранить', 'class' => 'btn-success'],
    ['type' => 'reset', 'label' => 'Отмена', 'class' => 'btn-danger']
];
?>
<?= \common\widgets\FormWidget::widget(['model' => $model, 'input' => $inputs, 'id'=>'user-change-form'])?>
