<?php

/* @var $this yii\web\View */
/* @var $data array */
/* @var $columns array */

$this->title = 'Пользователи';
?>
<div class="site-index">
    <?= \yii\helpers\Html::a('<span class="glyphicon glyphicon-plus"></span>Добавить пользователя', ['user/add'], ['class' => 'btn btn-success add-button'])?>
    <?= \common\widgets\myDataTable::widget(["data" => $data, "columns" => $columns, "detailUrl" => "user/detail/#id#", "deleteUrl" => "user/delete/#id#"])?>
    <?= \yii\widgets\LinkPager::widget(['pagination' => $pages]);?>
</div>
