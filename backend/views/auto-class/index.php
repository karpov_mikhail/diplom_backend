<?php
/* @var $this yii\web\View */
/* @var $data array */
/* @var $columns array */

$this->title = 'Категории автомобилей';
?>
<div class="auto-class-index">
    <?= \yii\helpers\Html::a('<span class="glyphicon glyphicon-plus"></span> Добавить категорию авто', ['auto-class/add'], ['class' => 'btn btn-success add-button'])?>
    <?= \common\widgets\myDataTable::widget(["data" => $data, "columns" => $columns, "detailUrl" => "auto-class/detail/#id#", "deleteUrl" => "auto-class/delete/#id#"])?>
    <?= \yii\widgets\LinkPager::widget(['pagination' => $pages]);?>
</div>
