<?php
/* @var $this yii\web\View */
/* @var $model \backend\models\AutoClassForm */
/* @var $inputs array */
?>
<?= \common\widgets\FormWidget::widget(['model' => $model, 'input' => $inputs, 'id'=>'auto-class-edit-form'])?>
