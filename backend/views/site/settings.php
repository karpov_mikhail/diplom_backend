<?php
/** @var $this \yii\web\View */
/** @var $model \backend\models\SettingsForm */
?>
<?= \common\widgets\FormWidget::widget(['model' => $model, 'input' => $model->getWidgetInputs(), 'id'=>'settings-form'])?>
