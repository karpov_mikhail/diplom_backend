<?php
/* @var $this yii\web\View */
/* @var $data array */
/* @var $columns array */

$this->title = 'Персонал';
?>
<div class="topic-index">
    <?= \yii\helpers\Html::a('<span class="glyphicon glyphicon-plus"></span> Добавить персонал', ['staff/add'], ['class' => 'btn btn-success add-button'])?>
    <?= \common\widgets\myDataTable::widget(["data" => $data, "columns" => $columns, "detailUrl" => "staff/detail/#id#", "deleteUrl" => "staff/delete/#id#"])?>
    <?= \yii\widgets\LinkPager::widget(['pagination' => $pages]);?>
</div>
