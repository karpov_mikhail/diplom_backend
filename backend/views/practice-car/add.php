<?php
/* @var $this yii\web\View */
/* @var $model \backend\models\PracticeCarForm */
?>
<?= \common\widgets\FormWidget::widget(['model' => $model, 'input' => $model->getWidgetInputs(), 'id'=>'practice-car-add-form'])?>
