<?php
/* @var $this yii\web\View */
/* @var $model \backend\models\StaffForm */
?>
<?= \common\widgets\FormWidget::widget(['model' => $model, 'input' => $model->getWidgetInputs(), 'id'=>'learning-group-add-form'])?>
