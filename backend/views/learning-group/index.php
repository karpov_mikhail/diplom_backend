<?php
/* @var $this yii\web\View */
/* @var $data array */
/* @var $columns array */

$this->title = 'Группы курсантов';
?>
<div class="topic-index">
    <?= \yii\helpers\Html::a('<span class="glyphicon glyphicon-plus"></span> Добавить группу', ['learning-group/add'], ['class' => 'btn btn-success add-button'])?>
    <?= \common\widgets\myDataTable::widget(["data" => $data, "columns" => $columns, "detailUrl" => "learning-group/detail/#id#", "deleteUrl" => "learning-group/delete/#id#"])?>
    <?= \yii\widgets\LinkPager::widget(['pagination' => $pages]);?>
</div>
