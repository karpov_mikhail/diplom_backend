<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?=$this->params['username']?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    ['label' => 'Мое меню', 'options' => ['class' => 'header']],
                    ['label' => 'Пользователи', 'icon' => 'address-book', 'url' => ['user/index']],
                    ['label' => 'Персонал', 'icon' => 'male', 'url' => ['staff/index']],
                    ['label' => 'Билеты ПДД', 'icon' => 'database', 'items' => [
                        ['label' => 'Темы билетов', 'icon' => 'arrow-right', 'url' => ['topic/index'],],
                        ['label' => 'Билеты', 'icon' => 'arrow-right', 'url' => ['card/index'],],
                        ['label' => 'Вопросы', 'icon' => 'arrow-right', 'url' => ['question/index'],],
                        ['label' => 'Группы обучения', 'icon' => 'arrow-right', 'url' => ['learning-group/index'],]
                    ]],
                    ['label' => 'Практика', 'icon' => 'car', 'items' => [
                        ['label' => 'Категории авто', 'icon' => 'arrow-right', 'url' => ['auto-class/index'],],
                        ['label' => 'Автомобили', 'icon' => 'arrow-right', 'url' => ['practice-car/index'],]
                    ]],
                    ['label' => 'Настройки', 'icon' => 'cogs', 'items' => [
                        ['label' => 'Настройки сайта', 'icon' => 'cog', 'url' => ['site/settings']],
                        ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                        ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ]],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>
