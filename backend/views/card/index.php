<?php
/* @var $this yii\web\View */
/* @var $data array */
/* @var $columns array */

$this->title = 'Билеты';
?>
<div class="topic-index">
    <?= \yii\helpers\Html::a('<span class="glyphicon glyphicon-plus"></span> Добавить билет', ['card/add'], ['class' => 'btn btn-success add-button'])?>
    <?= \common\widgets\myDataTable::widget(["data" => $data, "columns" => $columns, "detailUrl" => "card/detail/#id#", "deleteUrl" => "card/delete/#id#"])?>
    <?= \yii\widgets\LinkPager::widget(['pagination' => $pages]);?>
</div>
