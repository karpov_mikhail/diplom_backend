<?php
/* @var $this yii\web\View */
/* @var $model \backend\models\CardForm */
?>
<?= \common\widgets\FormWidget::widget(['model' => $model, 'input' => $model->getWidgetInputs(), 'id'=>'card-add-form'])?>
