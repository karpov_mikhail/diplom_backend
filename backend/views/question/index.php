<?php

/* @var $this yii\web\View */
/* @var $data array */
/* @var $columns array */
/* @var $pages \yii\data\Pagination */

$this->title = 'Вопросы';
?>
<div class="site-index">
    <?= \yii\helpers\Html::a('<span class="glyphicon glyphicon-plus"></span> Добавить вопрос', ['question/add'], ['class' => 'btn btn-success add-button'])?>
    <?= \common\widgets\myDataTable::widget(["data" => $data, "columns" => $columns, "detailUrl" => "question/detail/#id#", "deleteUrl" => "question/delete/#id#"])?>
    <?= \yii\widgets\LinkPager::widget(['pagination' => $pages]);?>
</div>
