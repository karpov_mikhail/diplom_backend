<?php
/** @var $this \yii\web\View */
/** @var $model \backend\models\QuestionForm */
?>
<?= \common\widgets\FormWidget::widget(['model' => $model, 'input' => $model->getWidgetInputs(), 'id'=>'question-change-form'])?>
