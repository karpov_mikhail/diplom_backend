<?php
/* @var $this yii\web\View */
/* @var $model \backend\models\QuestionForm */
/* @var $inputs array */

?>
<?= \common\widgets\FormWidget::widget(['model' => $model, 'input' => $model->getWidgetInputs(), 'id'=>'question-add-form'])?>
