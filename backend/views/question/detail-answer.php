<?php
use backend\models\AnswerForm;
use common\widgets\FormWidget;
/* @var $model AnswerForm */
?>
<?=FormWidget::widget(['model' => $model, 'input' => $model->getWidgetInputs(), 'id'=>'answer-update-form']);?>
