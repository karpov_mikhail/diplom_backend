<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'name' => 'Админка',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'user/page/<page:\d+>' => 'user/index',
                'user/' => 'user/index',
                'user/detail/<id:\d+>' => 'user/detail',
                'user/delete/<id:\d+>' => 'user/delete',

                'topic/page/<page:\d+>' => 'topic/index',
                'topic/' => 'topic/index',
                'topic/detail/<id:\d+>' => 'topic/detail',
                'topic/delete/<id:\d+>' => 'topic/delete',

                'auto-class/page/<page:\d+>' => 'auto-class/index',
                'auto-class/' => 'auto-class/index',
                'auto-class/detail/<id:\d+>' => 'auto-class/detail',
                'auto-class/delete/<id:\d+>' => 'auto-class/delete',

                'question/page/<page:\d+>' => 'question/index',
                'question/' => 'question/index',
                'question/detail/<id:\d+>' => 'question/detail',
                'question/delete/<id:\d+>' => 'question/delete',

                'card/page/<page:\d+>' => 'card/index',
                'card/' => 'card/index',
                'card/detail/<id:\d+>' => 'card/detail',
                'card/delete/<id:\d+>' => 'card/delete',

                'staff/page/<page:\d+>' => 'staff/index',
                'staff/' => 'staff/index',
                'staff/detail/<id:\d+>' => 'staff/detail',
                'staff/delete/<id:\d+>' => 'staff/delete',

                'learning-group/page/<page:\d+>' => 'learning-group/index',
                'learning-group/' => 'learning-group/index',
                'learning-group/detail/<id:\d+>' => 'learning-group/detail',
                'learning-group/delete/<id:\d+>' => 'learning-group/delete',

                'practice-car/page/<page:\d+>' => 'practice-car/index',
                'practice-car/' => 'practice-car/index',
                'practice-car/detail/<id:\d+>' => 'practice-car/detail',
                'practice-car/delete/<id:\d+>' => 'practice-car/delete'
            ],
        ],

    ],
    'params' => $params,
];
